# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



###########################################
## Need to run the following code before rails db:seed
# 
###########################################

###########################################
## Need to run the following code after rails db:seed
# rails db:seed:townships
# rails db:seed:user_create
# ##### rails db:seed:emails_update_to_org
# rails db:seed:members_yangon
# rails db:seed:members_mandalay
# rails db:seed:members_bago
# rails db:seed:members_magway
# rails db:seed:members_sagaing
# rails db:seed:members_ayeyawaddy
# rails db:seed:ecmembers
# rails db:seed:committee_members
###########################################

###########################################
## Email and password for admin
# contact@zwenex.com:12345678
# office@mcpamyanmar.org:12345678


## Email and password for member
# member@zwenex.com:12345678
###########################################





member_type = ["Student Member", "Associate Member", "Professional Member"]
gender_type = ["Male", "Female"]
education_type = ["Graduated", "Under Graduated", "Post Graduated"]

State.delete_all
Township.delete_all
Organization.delete_all
MembershipApplication.delete_all
Membership.delete_all
Member.delete_all
Ecmember.delete_all
Committee.delete_all
Staff.delete_all
User.delete_all

State.create([
    {name: "Yangon"}, 
    {name: "Mandalay"}, 
    {name: "Rakhine"}, 
    {name: "Magway"}, 
    {name: "Sagaing"}, 
    {name: "Bago"}, 
    {name: "Kayah"}, 
    {name: "Kachin"}, 
    {name: "Mon"}, 
    {name: "Ayeyawaddy"}, 
    {name: "Tanintharyi"}, 
    {name: "Union Territory"}, 
    {name: "Chin"}, 
    {name: "Kayin"}, 
    {name: "Shan"}
])

Organization.create(organization_name: 'Yangon Region Computer Professionals Association', state_id: State.find_by_name("Yangon").id, short_name: 'YRCPA')
Organization.create(organization_name: 'Mandalay Region Computer Professionals Association', state_id: State.find_by_name("Mandalay").id, short_name: 'ManRCPA')
Organization.create(organization_name: 'Sagaing Region Computer Professionals Association', state_id: State.find_by_name("Sagaing").id, short_name: 'SRCPA')
Organization.create(organization_name: 'Magway Region Computer Professionals Association', state_id: State.find_by_name("Magway").id, short_name: 'MagRCPA')
Organization.create(organization_name: 'Bago Region Computer Professionals Association', state_id: State.find_by_name("Bago").id, short_name: 'BRCPA')
Organization.create(organization_name: 'Ayeyarwaddy Region Computer Professionals Association', state_id: State.find_by_name("Ayeyawaddy").id, short_name: 'ARCPA')
Organization.create(organization_name: 'Kayin State Computer Professionals Association', state_id: State.find_by_name("Kayin").id, short_name: 'KSCPA')
Organization.create(organization_name: 'Mon State Computer Professionals Association', state_id: State.find_by_name("Mon").id, short_name: 'MSCPA')
Organization.create(organization_name: 'Myanmar Computer Professionals Association', country: "Myanmar", short_name: 'MyanmarCPA')
puts "Organization count #{Organization.count}"


##################################################################

##################################################################

committee_type = ["Education", "Technology", "Examination", "Competition", "Administration", "Member Affairs", "Information & PR",  "International Relations"]
Organization.all.each do |o|
    committee_type.each do |committee|
        Committee.create(name: committee, organization_id: o.id)
    end
end

##################################################################

# #### Organization name change #####
# Organization.find_by_state_id(State.find_by_name("Yangon").id).update(organization_name: 'Yangon Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Mandalay").id).update(organization_name: 'Mandalay Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Sagaing").id).update(organization_name: 'Sagaing Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Magway").id).update(organization_name: 'Magway Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Bago").id).update(organization_name: 'Bago Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Ayeyawaddy").id).update(organization_name: 'Ayeyarwaddy Region Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Kayin").id).update(organization_name: 'Kayin State Computer Professionals Association')
# Organization.find_by_state_id(State.find_by_name("Mon").id).update(organization_name: 'Mon State Computer Professionals Association')
# Organization.find_by_country('Myanmar').update(organization_name: 'Myanmar Computer Professionals Association')