class ChangeMemberNoToIntegerInMember < ActiveRecord::Migration[5.0]
  def change
    change_column :members, :member_no,'integer USING CAST(member_no AS integer)'
  end
end
