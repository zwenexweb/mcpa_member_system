class AddAttachmentDegreeFileToDegrees < ActiveRecord::Migration
  def self.up
    change_table :degrees do |t|
      t.attachment :degree_file
    end
  end

  def self.down
    remove_attachment :degrees, :degree_file
  end
end
