class AddGenderToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :gender, :string
  end
end
