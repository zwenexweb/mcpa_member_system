class AddColumnsToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :nrc_initial, :integer
    add_column :membership_applications, :nrc_middle, :string
    add_column :membership_applications, :nrc_tsp, :string
    add_column :membership_applications, :nrc_no, :integer
  end
end
