class AddColumnToOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :state_id, :integer
    add_column :organizations, :country, :string
  end
end
