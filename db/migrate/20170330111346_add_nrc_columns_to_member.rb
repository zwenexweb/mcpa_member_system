class AddNrcColumnsToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :nrc_initial, :integer
    add_column :members, :nrc_middle, :string
    add_column :members, :nrc_tsp, :string
    add_column :members, :nrc_no, :integer
  end
end
