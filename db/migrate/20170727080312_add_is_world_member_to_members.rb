class AddIsWorldMemberToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :is_world_member, :boolean
  end
end
