class CreateTownships < ActiveRecord::Migration[5.0]
  def change
    create_table :townships do |t|
      t.string :division
      t.integer :state_id
      t.string :name
      t.string :name_mm

      t.timestamps
    end
  end
end
