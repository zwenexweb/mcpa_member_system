class ChangeYearToStringInEcmember < ActiveRecord::Migration[5.0]
  def change
  	change_column :ecmembers, :year, :string
  	change_column :committee_members, :year, :string
  end
end
