class AddPositionToStaff < ActiveRecord::Migration[5.0]
  def change
    add_column :staffs, :position, :string
  end
end
