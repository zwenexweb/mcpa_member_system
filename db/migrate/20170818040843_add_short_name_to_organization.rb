class AddShortNameToOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :short_name, :string
  end
end
