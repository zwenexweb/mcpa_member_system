class AddMemberTypeColumnToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :member_type, :string
  end
end
