class AddYearToCommitteeMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :committee_members, :year, :integer
  end
end
