class CreateMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :members do |t|
      t.string :name
      t.date :dob
      t.string :nrc
      t.string :father_name
      t.text :address
      t.string :township
      t.string :sate
      t.string :phone
      t.string :email
      t.string :highest_education
      t.string :areas_of_tranining, array: true
      t.string :efficiencies, array: true
      t.string :areas_of_professional_working_experiences, array: true

      t.timestamps
    end
  end
end
