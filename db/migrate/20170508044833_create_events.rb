class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.date :form_date
      t.date :to_date
      t.time :form_time
      t.time :to_time
      t.string :place
      t.string :participant_type

      t.timestamps
    end
  end
end
