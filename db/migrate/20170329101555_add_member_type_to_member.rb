class AddMemberTypeToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :member_type, :string
  end
end
