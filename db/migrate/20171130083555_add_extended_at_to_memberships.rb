class AddExtendedAtToMemberships < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :extended_at, :date
  end
end
