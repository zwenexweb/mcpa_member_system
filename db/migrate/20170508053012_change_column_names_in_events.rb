class ChangeColumnNamesInEvents < ActiveRecord::Migration[5.0]
  def change
  	rename_column :events, :form_date, :from_date
  	rename_column :events, :form_time, :from_time
  end
end
