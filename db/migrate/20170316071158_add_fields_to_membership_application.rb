class AddFieldsToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :certificates, :json
    add_column :membership_applications, :current_employment, :json
    add_column :membership_applications, :employment_history, :json
  end
end
