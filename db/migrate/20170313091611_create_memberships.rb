class CreateMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :memberships do |t|
      t.integer :member_id
      t.integer :organization_id
      t.date :valid_till
      t.string :grade

      t.timestamps
    end
  end
end
