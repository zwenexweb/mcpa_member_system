class ChangeColumnNameInOrganization < ActiveRecord::Migration[5.0]
  def change
  	rename_column :organizations, :name, :organization_name
  end
end
