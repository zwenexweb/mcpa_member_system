class AddOrganizationIdToStaff < ActiveRecord::Migration[5.0]
  def change
    add_column :staffs, :organization_id, :integer
  end
end
