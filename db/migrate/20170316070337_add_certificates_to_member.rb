class AddCertificatesToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :certificates, :json
  end
end
