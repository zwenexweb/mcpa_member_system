class RenameAssessControlIdInPermission < ActiveRecord::Migration[5.0]
  def change
  	rename_column :permissions, :assess_control_id, :access_control_id
  end
end
