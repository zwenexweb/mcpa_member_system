class CreateAccessControls < ActiveRecord::Migration[5.0]
  def change
    create_table :access_controls do |t|
      t.string :key
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
