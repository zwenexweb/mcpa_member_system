class AddAttachmentTypeInAttachments < ActiveRecord::Migration[5.0]
  def change
  	add_column :attachments, :attachment_type, :string
  end
end
