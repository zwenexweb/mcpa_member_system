class CreateCommitteeMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :committee_members do |t|
      t.string :title
      t.integer :organization_id
      t.integer :committee_id
      t.integer :member_id

      t.timestamps
    end
  end
end
