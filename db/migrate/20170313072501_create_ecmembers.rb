class CreateEcmembers < ActiveRecord::Migration[5.0]
  def change
    create_table :ecmembers do |t|
      t.integer :organization_id
      t.string :role
      t.integer :year

      t.timestamps
    end
  end
end
