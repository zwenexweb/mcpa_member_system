class CreateDegrees < ActiveRecord::Migration[5.0]
  def change
    create_table :degrees do |t|
      t.string :degree_title
      t.integer :membership_application_id

      t.timestamps
    end
  end
end
