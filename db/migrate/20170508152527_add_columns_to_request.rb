class AddColumnsToRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :requests, :upgraded_type, :string
    add_column :requests, :extended_month, :integer
  end
end
