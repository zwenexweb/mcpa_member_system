class AddGenderToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :gender, :string
  end
end
