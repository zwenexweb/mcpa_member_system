class AddUpgradedAtToMemberships < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :upgraded_at, :date
  end
end
