class CreateMemberData < ActiveRecord::Migration[5.0]
  def change
    create_table :member_data do |t|
      t.date :date
      t.string :content_catagory, array: true
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
