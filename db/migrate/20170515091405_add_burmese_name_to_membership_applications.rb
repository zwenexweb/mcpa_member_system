class AddBurmeseNameToMembershipApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :burmese_name, :string
    add_column :members, :burmese_name, :string
  end
end
