class ChangeColumnInMember < ActiveRecord::Migration[5.0]
  def change
  	rename_column :members, :areas_of_tranining, :areas_of_training
  end
end
