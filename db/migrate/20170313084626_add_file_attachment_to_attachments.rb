class AddFileAttachmentToAttachments < ActiveRecord::Migration[5.0]
  def change
  	add_attachment :attachments, :file_attachment
  end
end
