class AddColumnToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :membership_applications, :code_no, :string
    add_column :members, :code_no, :string
  end
end
