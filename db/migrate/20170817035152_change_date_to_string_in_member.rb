class ChangeDateToStringInMember < ActiveRecord::Migration[5.0]

  def up
    change_column :members, :acceptance_date, :string
    change_column :members, :application_date, :string
  end

  def down
    change_column :members, :acceptance_date, :datetime
    change_column :members, :application_date, :datetime
  end
end
