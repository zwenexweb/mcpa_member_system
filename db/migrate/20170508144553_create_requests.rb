class CreateRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :requests do |t|
      t.integer :member_id
      t.string :request_type
      t.integer :staff_id

      t.timestamps
    end
  end
end
