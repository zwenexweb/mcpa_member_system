class AddOrganizationIdToRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :requests, :organization_id, :integer
  end
end
