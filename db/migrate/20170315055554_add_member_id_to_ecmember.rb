class AddMemberIdToEcmember < ActiveRecord::Migration[5.0]
  def change
    add_column :ecmembers, :member_id, :integer
  end
end
