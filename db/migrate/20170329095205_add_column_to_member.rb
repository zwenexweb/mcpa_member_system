class AddColumnToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :log, :json
  end
end
