class RemoveMemberTypeOnMember < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :member_type
  end
end
