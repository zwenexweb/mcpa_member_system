class AddAttachmentToMembershipApplication < ActiveRecord::Migration[5.0]
  def change
    add_attachment :membership_applications, :profile
  end
end
