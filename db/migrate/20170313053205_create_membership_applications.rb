class CreateMembershipApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :membership_applications do |t|
      t.string :name
      t.date :dob
      t.string :nrc
      t.string :father_name
      t.text :address
      t.string :township
      t.string :state
      t.string :phone
      t.string :email
      t.string :highest_education
      t.string :areas_of_training, array: true
      t.string :efficiencies, array: true
      t.string :areas_of_professional_working_experiences, array: true
      t.string :application_status
      t.string :process_status
      t.string :ref_no

      t.timestamps
    end
  end
end
