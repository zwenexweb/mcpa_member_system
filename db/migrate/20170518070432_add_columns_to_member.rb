class AddColumnsToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :application_date, :datetime
    add_column :members, :acceptance_date, :datetime
  end
end
