class RenameTitleToRoleInCommitteeMember < ActiveRecord::Migration[5.0]
  def change
    rename_column :committee_members, :title, :role
  end
end
