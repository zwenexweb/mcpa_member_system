class AddCommitteeNameToEcmember < ActiveRecord::Migration[5.0]
  def change
    add_column :ecmembers, :committee_name, :string
  end
end
