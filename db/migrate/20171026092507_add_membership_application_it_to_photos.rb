class AddMembershipApplicationItToPhotos < ActiveRecord::Migration[5.0]
  def change
    add_column :photos, :membership_application_id, :integer
  end
end
