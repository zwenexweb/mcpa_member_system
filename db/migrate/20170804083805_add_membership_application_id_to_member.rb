class AddMembershipApplicationIdToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :membership_application_id, :integer
  end
end
