class AddEmploymentToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :current_employment, :json
    add_column :members, :employment_history, :json
  end
end
