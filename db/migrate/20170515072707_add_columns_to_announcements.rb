class AddColumnsToAnnouncements < ActiveRecord::Migration[5.0]
  def change
    add_column :announcements, :member_type, :string
    add_column :announcements, :organization_id, :integer
    add_column :announcements, :committee_id, :integer
  end
end
