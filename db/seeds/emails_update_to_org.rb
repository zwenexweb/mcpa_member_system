org_email = ["mcpayangonoffice@gmail.com", "mcpamandalayoffice@gmail.com", "mcpamagwayoffice@gmail.com", "mcpasagaingoffice@gmail.com", "mcpaayeyarwaddyoffice@gmail.com", "mcpakayinoffice@gmail.com", "mcpamonoffice@gmail.com", "mcpabagooffice@gmail.com"]

Organization.all.each {|o| o.update(email: "mcpa#{o.state.name.downcase}office@gmail.com") if o.state.present?}

Organization.find_by_organization_name('Myanmar Computer Professionals Association').update(email: 'office@mcpamyanmar.org')