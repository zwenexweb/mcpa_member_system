
member_type = ["Student Member", "Associate Member", "Professional Member"]
gender_type = ["Male", "Female"]
education_type = ["Graduated", "Under Graduated", "Post Graduated"]

############################## Admin Account
admin = Staff.create(name: "MCPA Admin", email: "office@mcpamyanmar.org", role:"Admin", organization_id: Organization.first.id)
user = User.create(role: admin.role, email: admin.email, password: 12345678, staff_id: admin.id)
admin = Staff.create(name: "Zwenex Admin", email: "contact@zwenex.com", role:"Admin", organization_id: Organization.first.id)
user = User.create(role: admin.role, email: admin.email, password: 12345678, staff_id: admin.id)

############################## Staff Account for each organization
############################## Head Account for each organization
Organization.all.each do |o|
    # head = Staff.create(name: "head for " + o.organization_name, email: 'head_'+ o.state.name.downcase + "@zwenex.com", role: 'Head', organization_id: o.id) if o.state.present?
    # user = User.create(role: 'Head', email: head.email, password: 12345678, staff_id: head.id) if o.state.present? && head.present?
    staff = Staff.create(name: "staff for " + o.organization_name, email: "mcpa#{o.state.name.downcase}office@gmail.com", role: 'Staff', organization_id: o.id) if o.state.present?
    user = User.create(role: 'Staff', email: staff.email, password: 12345678, staff_id: staff.id) if o.state.present? && staff.present?
end


########### Staff Account for MCPA
    staff = Staff.create(name: "staff for MCPA", email: "staff_myanmar@zwenex.com", role: 'Staff', organization_id: Organization.find_by_country("Myanmar").id)
    user = User.create(role: 'Staff', email: staff.email, password: 12345678, staff_id: staff.id)


# ########################### Test member account
ma = MembershipApplication.create(name: Faker::Name.name, dob: Date.today()- 25.years, nrc_initial: rand(1..12), nrc_middle: "Naing", nrc_tsp: 'AhLaNa', nrc_no: rand(1..999999), father_name: Faker::Name.name, address: 'member101 address', township: 'Bahan', state: State.first.name, phone: '111111', email: "member@zwenex.com", highest_education: education_type[rand(3)], gender: gender_type[rand(2)], member_type: member_type[rand(3)])
m = Member.create(name: ma.name, state: ma.state, phone: ma.phone, email: ma.email, membership_application_id: ma.id)
Membership.create(member_id: m.id, organization_id: State.find_by_name(m.state).organization.id, valid_till: DateTime.now + 3.months, grade: ma.member_type)
User.create(role: "Member", email: m.email, password: 12345678, member_id: m.id)

