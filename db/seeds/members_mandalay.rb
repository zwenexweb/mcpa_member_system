
##########################################################
# Mandalay member list
##########################################################

count = 0
invalid_members = []
lines = File.open("#{Rails.root}/members_mandalay.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
	if member_no.present?
		member_no = "0900000" + member_no if member_no.length == 1
		member_no = "090000" + member_no if member_no.length == 2
		member_no = "09000" + member_no if member_no.length == 3
		member_no = "0900" + member_no if member_no.length == 4
		member_no = "090" + member_no if member_no.length == 5
		member_no = "09" + member_no if member_no.length == 6

# print member_no
# print " = #{member_no.to_i} " 
# print " = #{member_no.length} " 
# 	print "     / "
		if member_no.length == 8 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?
		
		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"

		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name("Mandalay").present? ? State.find_by_name("Mandalay").organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save
			    User.create(email: email, password: 12345678, role: 'Member', member_id: member.id) if email.present?
			else
				invalid_members << member.errors
			end
			count +=1
			if count%100 == 0
				print "."
			end
		end
		end
	end
end

# puts "Mandalay member added"
# puts Member.count
# puts Membership.count
# puts User.count
# puts invalid_members.inspect




# puts "Mandalay member added"
# puts Member.count
# puts Membership.count
# puts User.count
# puts invalid_members.inspect


# puts "Mandalay member added"
# puts Member.count
# puts Membership.count
# puts User.count
# puts invalid_members.inspect


# puts "Mandalay member added"
# puts Member.count
# puts Membership.count
# puts User.count
# puts invalid_members.inspect






















