# At first, I applied the current state of the member as its current organization's state.
# But the MCPA staff said, if a member no. of member is started with 12 (12XXXXXX), that member is Yangon MCPA's member no matter where he currently is.


org = Organization.find(8)

org.members.all.each do |m|
	# puts m.inspect
	# puts m.memberships.inspect
	# puts m.organizations.inspect
	if m.member_no.present?
		organization_id = Organization.first
		if m.member_no > 12000000
			organization_id = Organization.find_by_short_name("YRCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Yangon"
		elsif m.member_no > 10000000
			organization_id = Organization.find_by_short_name("ARCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Ayeyawaddy"
		elsif m.member_no > 9000000
			organization_id = Organization.find_by_short_name("ManRCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Mandalay"
		elsif m.member_no > 8000000
			organization_id = Organization.find_by_short_name("MagRCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Magway"
		elsif m.member_no > 7000000
			organization_id = Organization.find_by_short_name("BRCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Bago"
		elsif m.member_no > 5000000
			organization_id = Organization.find_by_short_name("SRCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Sagaing"
		elsif m.member_no > 3000000
			organization_id = Organization.find_by_short_name("KSCPA")
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to Kayin"
		else
			# organization_id = Organization.first
			puts m.member_no.to_s + " - " + m.organizations.first.organization_name + " to ------- Yangon"
		end

		if m.memberships.present? && organization_id.present?
			membership = m.memberships.first
			membership.organization_id = organization_id.id
			membership.save
		end
	else
		puts m.inspect
	end
end
