
##########################################################################################
# Member list for Kayin
##########################################################################################

puts "Kayin members"

organization_id = State.find_by_name('Kayin').organization.id

invalid_members = []
new_members = []
count = 0
lines = File.open("#{Rails.root}/members_kayin.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")

    if member_no.present?
		member_no = "0300000" + member_no if member_no.length == 1
		member_no = "030000" + member_no if member_no.length == 2
		member_no = "03000" + member_no if member_no.length == 3
		member_no = "0300" + member_no if member_no.length == 4
		member_no = "030" + member_no if member_no.length == 5
		member_no = "03" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0

		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"
		   	
		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    # organization_id =  State.find_by_name(state).present? && State.find_by_name(state).organization.present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    # membership.save(validate: false)
			    if membership.save(validate: false)
				    user = User.new(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
				    print " #{count} "
				    new_members << member.id
			    else
			    	print "#{count} - #{member_no} / "
			    end
			else
				puts "#{member.inspect} error"
				invalid_members << member.errors
			end

			count +=1
			if count%10 == 0
				puts "-"
			end
		else
			puts "#{member_no} error"
		end
	else
		puts name + " error"
	end
end

puts new_members.inspect
puts new_members.count

puts "count is - #{count}"
puts "kayin member count - #{Member.where(state: 'Kayin').count}"
# puts User.count
puts invalid_members.inspect




invalid_members = []
count = 0
lines = File.open("#{Rails.root}/members_kayin_without_user_account.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "0300000" + member_no if member_no.length == 1
		member_no = "030000" + member_no if member_no.length == 2
		member_no = "03000" + member_no if member_no.length == 3
		member_no = "0300" + member_no if member_no.length == 4
		member_no = "030" + member_no if member_no.length == 5
		member_no = "03" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0
			
		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"
		   	
		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    # organization_id =  State.find_by_name(state).present? && State.find_by_name("Kayin").organization.present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    if membership.save(validate: false)
			    else
			    	print "#{count} - #{member_no} / "
			    end
			else
				puts "#{member.inspect}"
				invalid_members << member.errors
			end

			count +=1
			if count%10 == 0
				print "."
			end
		else
			puts "error - #{member_no}"
		end
	else
		puts name
	end
end

puts "count is - #{count}"
puts "kayin member count - #{Member.where(state: 'Kayin').count}"
puts invalid_members.inspect


