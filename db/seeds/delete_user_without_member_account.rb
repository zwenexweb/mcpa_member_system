will_delete_user = []
User.where.not(member_id: nil).each do |user|
	will_delete_user << user unless user.member.present?
end

puts "will delete user count - #{will_delete_user.count}"

will_delete_user.each do |u|
	if Member.find_by_email(u.email).present?
		User.find(u.id).update(member_id: Member.find_by_email(u.email).id)
		print "."
	else
		u.delete
	end
end