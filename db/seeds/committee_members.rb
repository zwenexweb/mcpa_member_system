
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/committee_members_country.tsv").readlines
lines.each do |l|
    member_no, name, member_type, state, email, phone, committee, role = l.strip.split("\t")
   	
	   	member_type = "Professional Member" if member_type == "PM"
	   	member_type = "Associate Member" if member_type == "AM"
	   	member_type = "Student Member" if member_type == "SM"
	   	member_type = "Fellow Member" if member_type == "FM"
	   	
   	if Member.find_by_member_no(member_no).present?
   		member = Member.find_by_member_no(member_no)
   		member_id = member.id
   		member.update(is_world_member: true)
   		organization = Organization.where.not(country: nil).first
   		committees = organization.committees
   		committee_id = committees.where(name: committee)
   		if committee_id.present?
	    	CommitteeMember.create(organization_id: organization.id, committee_id: committee_id.first.id, member_id: member_id, role: role, year: 2017) 
	    else
	    	c = Committee.create(name: committee, organization_id: organization.id)
	    	CommitteeMember.create(organization_id: organization.id, committee_id: c.id, member_id: member_id, role: role, year: 2017) 
	    end
   	else
	    member = Member.new(member_no: member_no, name: name, phone: phone, email: email, state: state, member_type: member_type)
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: state, member_type: member_type)
	   	
	    if member.save(validate: false) && membership_application.save(validate: false)
	    	member.update(membership_application_id: membership_application.id)
	    	current_state = State.find_by_name(state)
		    organization_id =  current_state.present? && current_state.organization.present? ? current_state.organization.id : Organization.first.id
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
       		if committee_id.present?
		    	CommitteeMember.create(organization_id: organization.id, committee_id: committee_id.first.id, member_id: member_id, role: role, year: 2017) 
		    else
		    	c = Committee.create(name: committee, organization_id: organization.id)
		    	CommitteeMember.create(organization_id: organization.id, committee_id: c.id, member_id: member_id, role: role, year: 2017) 
		    end
		else
			invalid_members << member.errors
		end
	end
	count +=1
	if count%100 == 0
		print "."
	end
end

puts "committee members myanmar added"
puts CommitteeMember.count

