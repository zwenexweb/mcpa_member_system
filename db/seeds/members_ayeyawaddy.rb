
##########################################################
# Bago member list
##########################################################


count = 0
invalid_members = []
valid_members = []
lines = File.open("#{Rails.root}/members_ayeyawaddy.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "1000000" + member_no if member_no.length == 1
		member_no = "100000" + member_no if member_no.length == 2
		member_no = "10000" + member_no if member_no.length == 3
		member_no = "1000" + member_no if member_no.length == 4
		member_no = "100" + member_no if member_no.length == 5
		member_no = "10" + member_no if member_no.length == 6

		if member_no.length == 8 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?

		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"
		   	
		   	member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save
			    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
				valid_members << member
			else
				invalid_members << member.errors
			end
		end
		count +=1
		if count%100 == 0
			print "."
		end
	end
	end
end

puts "ayeyawaddy member added"
puts valid_members.count
puts invalid_members.count
puts invalid_members.inspect


##########################################################################################
# Member without user account for bago
##########################################################################################


invalid_members = []
valid_members = []

count = 0
lines = File.open("#{Rails.root}/members_ayeyawaddy_without_user_account.tsv").readlines
lines.each do |l|
	print '-'
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "1000000" + member_no if member_no.length == 1
		member_no = "100000" + member_no if member_no.length == 2
		member_no = "10000" + member_no if member_no.length == 3
		member_no = "1000" + member_no if member_no.length == 4
		member_no = "100" + member_no if member_no.length == 5
		member_no = "10" + member_no if member_no.length == 6

		if member_no.length == 8 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?
			
		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"

			member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save
			    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
				valid_members << member
			else
				invalid_members << member.errors
			end

			count +=1
			if count%10 == 0
				puts "."
			end
		end
		end
	end
end
# puts Member.count
# puts Membership.count
# puts User.count
puts valid_members.count
puts invalid_members.count
puts "Member without user account for ayeyawaddy"
