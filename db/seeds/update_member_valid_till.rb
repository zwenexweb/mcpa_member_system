lines = File.open("#{Rails.root}/data/member_list_with_valid_till.csv").readlines
puts lines.count

lines[1...-1].each do |line|
    no, member_no, member_type, name, phone, email, upgtaded_at, application_date, acceptance_date, valid_till, date_2017, date_2018, date_2019, date_2020, date_2021, date_2022 = line.split("\t")
    member = Member.find_by_member_no(member_no)
    email = email.gsub(" ", "")
    if member.present?
        application_date == application_date.to_date if application_date.class == "Date"
        acceptance_date == acceptance_date.to_date if acceptance_date.class == "Date"
        if valid_till.present?
            membership = member.memberships.last
            organization_id =  State.find_by_name("Yangon").present? ? State.find_by_name("Yangon").organization.id : Organization.first.id
            unless valid_till == "Fellow Member"
            else
                # ### become fellow member
                # ### update member type
                member_type = 'Fellow Member'
                valid_till = DateTime.now
            end
            unless membership.valid_till == valid_till.to_date
                if membership.update(grade: member_type, valid_till: valid_till.to_date, extended_at: DateTime.now, organization_id: organization_id)
                    puts "update membership for #{member_no} with #{valid_till.to_date}"
                else
                end
            end
        else
            ### valid till is not present
            ### will not update membership
        end

        ### need to update user account
        if email.present? && email != '-'
            member.update(name: name, phone: phone, email: email, acceptance_date: acceptance_date, application_date: application_date)
            user = member.user
            if user.present?
                puts "#{user.email} will update to #{email}" unless user.email == email
                if user.email == email 
                    puts "no update"
                else
                    unless user.update(email: email)
                        puts "update error #{member_no} - #{user.errors.full_messages}"
                    end
                end
            else
                ### no user with email
                puts "#{member_no} #{email} does not have user."
                user = User.new(role: 'Member', member_id: member.id, email: email, password: 12345678, password_confirmation: 12345678)
                unless user.save
                    puts "------------------- #{email} #{member_no} - #{user.errors.full_messages}"
                else
                    puts "new user create #{user.email} is #{email}"
                end
            end
        else
            ### no email
            if member.update(name: name, phone: phone, remark: acceptance_date, application_date: application_date)
                puts "no email, no new user, phone, remark update"
            else
                puts "---------------- member is not updated, no email error."
            end
        end
    else
        ### should not reach here
        ### member with such member_no is not present in the database
    end
    puts "----------------*************************---------------------"
end


