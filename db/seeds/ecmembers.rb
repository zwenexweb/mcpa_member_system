#################################################################
# bago ecmember
#################################################################
puts "Bago"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_bago.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Bago").organization.id
    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Bago", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Bago", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Bago Ecmember added"



#################################################################
# kayin ecmember
#################################################################
puts "Kayin"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_kayin.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Kayin").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Kayin", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Kayin", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Kayin Ecmember added"



#################################################################
# Magway ecmember
#################################################################
puts "Magway"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_magway.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Magway").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Magway", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Magway", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Magway Ecmember added"


#################################################################
# Mandalay ecmember
#################################################################
puts "Mandalay"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_mandalay.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Mandalay").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Mandalay", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Mandalay", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Mandalay Ecmember added"



#################################################################
# Mon ecmember
#################################################################
puts "Mon"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_mon.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Mon").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Mon", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Mon", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Mon Ecmember added"



#################################################################
# Sagaing ecmember
#################################################################
puts "Sagaing"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_sagaing.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Sagaing").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Sagaing", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Sagaing", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Sagaing Ecmember added"



#################################################################
# Yangon ecmember
#################################################################
puts "Yangon"
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_yangon.tsv").readlines
lines.each do |l|
    name, rank, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"

   	organization_id = State.find_by_name("Yangon").organization.id

    member = Member.where("member_no = ?", member_no.to_i).first if member_no.present?
    member = Member.find_by_name(name) unless member.present?
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.inspect unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Yangon", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Yangon", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end

	end
	count +=1
end
puts count
puts "Yangon Ecmember added"






################################################################
# country ecmember
################################################################
count = 0
invalid_members = []
lines = File.open("#{Rails.root}/ecmembers_country.tsv").readlines
lines.each do |l|
    name, rank, member_type, member_no, email, phone, member_type = l.strip.split("\t")

   	member_type = "Student Member" if member_type == "SM"
   	member_type = "Professional Member" if member_type == "PM"
   	member_type = "Associate Member" if member_type == "AM"
   	member_type = "Fellow Member" if member_type == "FM"
   	
   	organization_id = Organization.where.not(country: nil).first.id
    member = Member.where("member_no = ?", member_no.to_i).first
    if member.present?
		ec = Ecmember.new(organization_id: organization_id, role: rank, member_id: member.id, year: "2017-2018")
		puts ec.errors.full_messages unless ec.save
	else
	    membership_application = MembershipApplication.new(name: name, phone: phone, email: email, state: "Yangon", member_type: member_type)
	    member = Member.new(member_no: member_no.to_i, name: name, phone: phone, email: email, state: "Yangon", member_type: member_type, membership_application_id: membership_application.id)
		   	
	    if member.save(validate: false) && membership_application.save(validate: false)
	    	current_state = State.find_by_name("Yangon")
		    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
		    membership.save
		    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
		    ec = Ecmember.new(organization_id: organization_id, role: rank, year: "2017-2018", member_id: member.id)
			puts member_no unless ec.save
		else
			invalid_members << member.errors
		end
   end
	count +=1
end

puts count
puts "MCPA Myanmar Ecmember added"