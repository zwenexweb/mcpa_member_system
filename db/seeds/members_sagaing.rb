
##########################################################################################
# Member list for Sagaing
##########################################################################################


# Member.delete_all
# Membership.delete_all


invalid_members = []
count = 0
lines = File.open("#{Rails.root}/members_sagaing.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "0500000" + member_no if member_no.length == 1
		member_no = "050000" + member_no if member_no.length == 2
		member_no = "05000" + member_no if member_no.length == 3
		member_no = "0500" + member_no if member_no.length == 4
		member_no = "050" + member_no if member_no.length == 5
		member_no = "05" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?

		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"
		   	
		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? ? State.find_by_name("Sagaing").organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save(validate: false)
			    user = User.new(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
			    puts user.errors.full_messages unless user.save
			else
				invalid_members << member.errors
				invalid_members << membership_application.errors
			end

			count +=1
			if count%100 == 0
				print "."
			end
		end
		end
	end
end
puts "count is - #{count}"
puts "sagaing member count - #{Member.where(state: 'Sagaing').count}"
puts "sagaing membership count - #{Membership.where(organization_id: 5).count}"
puts User.count
puts invalid_members.inspect



invalid_members = []
count = 0
lines = File.open("#{Rails.root}/members_sagaing_without_user_account.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "0500000" + member_no if member_no.length == 1
		member_no = "050000" + member_no if member_no.length == 2
		member_no = "05000" + member_no if member_no.length == 3
		member_no = "0500" + member_no if member_no.length == 4
		member_no = "050" + member_no if member_no.length == 5
		member_no = "05" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?

		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	member_type = "Fellow Member" if member_type == "FM"
		   	
		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? && State.find_by_name("Sagaing").organization.present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save(validate: false)
			else
				invalid_members << member.errors
			end

			count +=1
			if count%100 == 0
				print "."
			end
		end
		end
	end
end
puts "count is - #{count}"
puts "sagaing member count - #{Member.where(state: 'Sagaing').count}"
puts "sagaing membership count - #{Membership.where(organization_id: 5).count}"
puts invalid_members.inspect

puts "Sagaing Member Added"

