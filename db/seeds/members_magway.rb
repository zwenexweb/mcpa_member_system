
##########################################################
# Magway member list
##########################################################

count = 0
invalid_members = []
lines = File.open("#{Rails.root}/members_magway.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "0800000" + member_no if member_no.length == 1
		member_no = "080000" + member_no if member_no.length == 2
		member_no = "08000" + member_no if member_no.length == 3
		member_no = "0800" + member_no if member_no.length == 4
		member_no = "080" + member_no if member_no.length == 5
		member_no = "08" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?

		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"

		    member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save
			    unless User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
			    	puts User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id).errors.full_messages
			    	puts member.email
			    end
			else
				invalid_members << member.errors
			end

			count +=1
			if count%100 == 0
				print "."
			end
		end
		end
	end
end
puts "count #{count}"
puts "Magway member added"
puts Member.count
puts Membership.count
puts User.count


##########################################################################################
# Member without user account for magway
##########################################################################################

invalid_members = []

count = 0
lines = File.open("#{Rails.root}/members_magway_without_user_account.tsv").readlines
lines.each do |l|
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,phone,email,application_date,acceptance_date,state,address = l.strip.split("\t")
   	
   	if member_no.present?
		member_no = "0800000" + member_no if member_no.length == 1
		member_no = "080000" + member_no if member_no.length == 2
		member_no = "08000" + member_no if member_no.length == 3
		member_no = "0800" + member_no if member_no.length == 4
		member_no = "080" + member_no if member_no.length == 5
		member_no = "08" + member_no if member_no.length == 6

		if member_no.length == 7 && member_no.to_i != 0
	    unless Member.where(member_no: member_no).present?
			
		   	member_type = "Professional Member" if member_type == "PM"
		   	member_type = "Associate Member" if member_type == "AM"
		   	member_type = "Student Member" if member_type == "SM"
		   	
		   	member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)
		    membership_application = MembershipApplication.new(name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: state, member_type: member_type, nrc: nrc)

		    if member.save(validate: false) && membership_application.save(validate: false)
			    member.update(membership_application_id: membership_application.id)
			    organization_id =  State.find_by_name(state).present? ? State.find_by_name(state).organization.id : Organization.first.id
			    membership = member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 6.months)
			    membership.save
			    User.create(email: member.email, password: 12345678, role: 'Member', member_id: member.id)
			else
				invalid_members << member.errors
			end

			count +=1
			if count%10 == 0
				puts "."
			end
		end
		end
	end
end

puts "count #{count}"
puts Member.count
puts Membership.count
puts User.count
puts invalid_members.inspect
invalid_members = []
puts "Member without user account for Magway"