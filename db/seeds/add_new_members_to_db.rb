# require 'spreadsheet'
# require 'fileutils'

## If member exists, update the fields
## If not, create new member along with membership_application and membership to associated organization
error_folder_name = "#{Rails.root.to_s}/error_#{DateTime.now.month}_#{DateTime.now.day}"
Dir.mkdir "#{error_folder_name}" unless File.exists?("#{error_folder_name}")

# File.open("#{error_folder_name}/name_error_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end
# File.open("#{error_folder_name}/new_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end
# File.open("#{error_folder_name}/no_expiry_date_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end
# File.open("#{error_folder_name}/no_expiry_date_new_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end
# File.open("#{error_folder_name}/no_name_nor_member_type_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end
# File.open("#{error_folder_name}/unknown_error_members.txt", 'w+') do
#     |file| file.write("member_no\tmember_name\tmember_type\terror\n") 
# end

count = 0
invalid_members = []
invalid_members_hash = {}
new_members = []
new_members_hash = {}
valid_members = []
valid_members_hash = {}

error = {}
error_member = []
# book = Spreadsheet.open "#{Rails.root}/data/mcpa_member_update_2018_10_10.xls" ### old one
book = Spreadsheet.open "#{Rails.root}/data/Update_Member_List_2018_11_26.xls"

sheet = book.worksheet 0

sheet.each do |row|
    count +=1
    # break if row[0].to_i > 10
    member_no,member_type,name,father_name,dob,gender,age,nrc,position,organization,highest_education,areas_of_training,address, phone,email,upgrade, application_date, valid_till, year_2017, year_2018, year_2019, year_2020, year_2021, year_2022, year_2023, year_2024, year_2025 = row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14],row[15],row[16],row[17],row[18],row[19],row[20],row[21],row[22],row[23],row[24],row[25],row[26],row[27],row[28]
    
    break unless member_no.present?

    if member_no.present? && member_type.present? && name.present?
        begin
            member_no = member_no.to_s
            date_for_valid_till = false

            puts "\n------------------------"
            ### if member_no present, check the database for member with that member_no exists
            ### to check member with member_no exists, need to check member_no is 8 digit
            unless member_no.length == 8 && member_no.to_i != 0
                ### member_no need to be like this "100000001"
                member_no = "1200000" + member_no if member_no.length == 1
                member_no = "120000" + member_no if member_no.length == 2
                member_no = "12000" + member_no if member_no.length == 3
                member_no = "1200" + member_no if member_no.length == 4
                member_no = "120" + member_no if member_no.length == 5
                member_no = "12" + member_no if member_no.length == 6
            else
                ### no need to manipulate member_no    
            end

            if Member.find_by_member_no(member_no).present?
                print " existing member "
                member = Member.find_by_member_no(member_no)
                if member_no.length == 8 && member_no.to_i != 0
                    # if member.name.gsub(" ", "").downcase == name.gsub(" ", "").downcase
                    #     ### name matches

                    # else
                    #     ### name not match
                    #     invalid_members << member_no
                        
                    #     invalid_members_hash['name is not match'] = [] unless invalid_members_hash['name is not match'].present?
                    #     invalid_members_hash['name is not match'] << member_no

                    #     puts "   put into name error member"

                    #     File.open("#{error_folder_name}/name_error_members.txt", 'a') do
                    #         |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. The name of #{member_no} in database is #{member.name} and the name for that member is #{name} in excel. \n") 
                    #     end
                    #     next
                    # end

                    member_type2 = member_type.gsub(" ", "")
                    member_type = "Professional Member" if member_type2 == "PM"
                    member_type = "Associate Member" if member_type2 == "AM"
                    member_type = "Student Member" if member_type2 == "SM"
                    member_type = "Fellow Member" if member_type2 == "FM"
                        
                    memberships = member.memberships

                    if upgrade == 'Fellow Member'
                        if member.update(name: name) && memberships.last.update(grade: 'Fellow Member')
                            # ### no need to add valid_till
                            # puts " #{member.name} #{member.member_no} is a fellow member"
                            
                            # valid_members << member_no

                            # valid_members_hash["upgrade_to_fellow_member"] = [] unless valid_members_hash["upgrade_to_fellow_member"].present?
                            # valid_members_hash["upgrade_to_fellow_member"] << member_no
                            # # memberships.last.update(grade: member_type, valid_till: nil)
                        else
                            puts 'cant save fellow member'
                        end

                    elsif member_type == "Fellow Member" || memberships.last.grade == "Fellow Member"
                        if member.update(name: name) && memberships.last.update(grade: 'Fellow Member')
                            #     ### this is fellow member
                            #     puts " #{member_no} is already fellow member."
                            #     valid_members << member_no
                                
                            #     valid_members_hash["already_fellow_member"] = [] unless valid_members_hash["already_fellow_member"].present?
                            #     valid_members_hash["already_fellow_member"] << member_no
                            # else
                        else
                            if member.errors.full_messages == ["Member no has already been taken"]
                                Member.where(member_no: member_no).last.delete
                                member = Member.find_by_member_no(member_no)
                            elsif member.errors.full_messages == ["State can't be blank"]
                                member.state = 'Yangon'
                            end
                            unless member.update(name: name) && member.memberships.last.update(grade: 'Fellow Member')
                                puts 'cant save already fellow member'

                                File.open("#{error_folder_name}/cannot_save_existing_fellow_member.txt", 'a') do
                                    |file| file.write("#{member_no}\t#{name}\t#{member_type}\t cannot be updated.  #{member.errors.full_messages} \n") 
                                end
                            end
                        end
                    else
                        ### membership grade is not equal to member_type
                        ### need to update grade
                        ### need to check the date
                        # valid_till, year_2017, year_2018, year_2019, year_2020, year_2021, year_2022, year_2023, year_2024, year_2025
                        date_for_valid_till = false
                        year = false
                        month = false
                        day = false
                        if year_2025.present?
                            year_2025 = year_2025.to_date
                            year = 2025
                            month = year_2025.month
                            day = year_2025.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2024.present?
                            year_2024 = year_2024.to_date
                            year = 2024
                            month = year_2024.month
                            day = year_2024.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2023.present?
                            year_2023 = year_2023.to_date
                            year = 2023
                            month = year_2023.month
                            day = year_2023.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2022.present?
                            year_2022 = year_2022.to_date
                            year = 2022
                            month = year_2022.month
                            day = year_2022.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2021.present?
                            year_2021 = year_2021.to_date
                            year = 2021
                            month = year_2021.month
                            day = year_2021.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2020.present?
                            year_2020 = year_2020.to_date
                            year = 2020
                            month = year_2020.month
                            day = year_2020.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2019.present?
                            year_2019 = year_2019.to_date
                            year = 2019
                            month = year_2019.month
                            day = year_2019.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2018.present?
                            year_2018 = year_2018.to_date
                            year = 2018
                            month = year_2018.month
                            day = year_2018.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif year_2017.present?
                            year_2017 = year_2017.to_date
                            year = 2017
                            month = year_2017.month
                            day = year_2017.day
                            date_for_valid_till = [day, month, year].join("-").to_date
                        elsif valid_till.present?
                            if valid_till.to_date.is_a?(Date)
                                date_for_valid_till = valid_till.to_date
                            else
                                puts "ERROR in valid date"
                            end
                        elsif application_date.present?
                            date_for_valid_till = application_date
                        else
                        end

                        if date_for_valid_till.present?
                            ### membership date will be updated


                            # valid_members << member_no
                            
                            # valid_members_hash["update_valid_till"] = [] unless valid_members_hash["update_valid_till"].present?
                            # valid_members_hash["update_valid_till"] << member_no
                            unless memberships.last.update(grade: member_type, valid_till: date_for_valid_till)
                                File.open("#{error_folder_name}/cannot_save_existing_member.txt", 'a') do
                                    |file| file.write("#{member_no}\t#{name}\t#{member_type}\t cannot be updated. date for valid_till is #{date_for_valid_till} #{memberships.last.errors.full_messages} \n") 
                                end
                            end
                        else
                            puts " puts into no_expiry date member "
                            File.open("#{error_folder_name}/cannot_save_no_expiry_date.txt", 'a') do
                                |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. That member doesn't have expiry date for membership. \n") 
                            end
                        end
                    end
                end
                ### existing member section ends
            else
                ### member number not found in db
                ### need to create new member
                
                new_members_hash['new_member'] = [] unless new_members_hash['new_member'].present?
                new_members_hash['new_member'] << member_no

                member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: 'Yangon', nrc: nrc)
                if  member_type == "Fellow Member"
                    ### this is fellow member
                    unless member.valid?
                        # puts member.errors.full_messages
                        File.open("#{error_folder_name}/new_member_cant_save_fellow_member_errors.txt", 'a') do
                            |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. \t error details - #{member.errors.full_messages} \n") 
                        end
                    else
                        membership = member.memberships.build(organization_id: Organization.first.id, grade: "Fellow Member", valid_till: DateTime.now + 6.months)
                        unless membership.valid?
                            # puts member.errors.full_messages
                            File.open("#{error_folder_name}/new_member_cant_save_fellow_membership_errors.txt", 'a') do
                                |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. \t error details - #{membership.errors.full_messages} \n") 
                            end
                        end
                    end
                    # puts " new member #{member_no} is already fellow member."
                    # new_members << member_no
                    
                    # new_members_hash["already_fellow_member"] = [] unless new_members_hash["already_fellow_member"].present?
                    # new_members_hash["already_fellow_member"] << member_no
                else
                    # valid_till, year_2017, year_2018, year_2019, year_2020, year_2021, year_2022, year_2023, year_2024, year_2025
                    date_for_valid_till = false
                    year = false
                    month = false
                    day = false
                    if year_2025.present?
                        year_2025 = year_2025.to_date
                        year = 2025
                        month = year_2025.month
                        day = year_2025.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2024.present?
                        year_2024 = year_2024.to_date
                        year = 2024
                        month = year_2024.month
                        day = year_2024.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2023.present?
                        year_2023 = year_2023.to_date
                        year = 2023
                        month = year_2023.month
                        day = year_2023.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2022.present?
                        year_2022 = year_2022.to_date
                        year = 2022
                        month = year_2022.month
                        day = year_2022.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2021.present?
                        year_2021 = year_2021.to_date
                        year = 2021
                        month = year_2021.month
                        day = year_2021.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2020.present?
                        year_2020 = year_2020.to_date
                        year = 2020
                        month = year_2020.month
                        day = year_2020.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2019.present?
                        year_2019 = year_2019.to_date
                        year = 2019
                        month = year_2019.month
                        day = year_2019.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2018.present?
                        year_2018 = year_2018.to_date
                        year = 2018
                        month = year_2018.month
                        day = year_2018.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif year_2017.present?
                        year_2017 = year_2017.to_date
                        year = 2017
                        month = year_2017.month
                        day = year_2017.day
                        date_for_valid_till = [day, month, year].join("-").to_date
                    elsif valid_till.present?
                        if valid_till.to_date.is_a?(Date)
                            date_for_valid_till = valid_till.to_date
                        else
                            puts "ERROR in valid date"
                        end
                    elsif application_date.present?
                        date_for_valid_till = application_date
                    else
                        # new_members << member_no
                        # new_members_hash['no_valid_till_date'] = [] unless new_members_hash['no_valid_till_date'].present?
                        # new_members_hash['no_valid_till_date'] << member_no

                        # File.open("#{error_folder_name}/no_expiry_date_new_members.txt", 'a') do
                        #     |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. \temail - #{email}\tphone - #{phone}\t That member doesn't have expiry date for membership. \n") 
                        # end
                        # next
                    end

                    if date_for_valid_till.present?
                        # ### membership will be updated
                        # puts " new member #{member_no} date_for_valid_till is #{date_for_valid_till}"
                        
                        # new_members << member_no
                        
                        # new_members_hash["update_valid_till"] = [] unless new_members_hash["update_valid_till"].present?
                        # new_members_hash["update_valid_till"] << member_no

                        member = Member.new(member_no: member_no.to_i, name: name, father_name: father_name, dob: dob, gender: gender, highest_education: highest_education, areas_of_training: areas_of_training, phone: phone, email: email, address: address, state: 'Yangon', nrc: nrc)

                        unless member.save
                            # puts member.errors.full_messages
                            File.open("#{error_folder_name}/new_member_cant_save_member_errors.txt", 'a') do
                                |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. \t error details - #{member.errors.full_messages} \n") 
                            end
                        else
                            membership = member.memberships.build(organization_id: Organization.first.id, grade: member_type, valid_till: DateTime.now + 6.months)
                            unless membership.save
                                # puts member.errors.full_messages
                                File.open("#{error_folder_name}/new_member_cant_save_membership_errors.txt", 'a') do
                                    |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. \t error details - #{membership.errors.full_messages} \n") 
                                end
                            end
                        end
                    else
                        # puts member.errors.full_messages
                        File.open("#{error_folder_name}/new_member_cant_save_member_no_date_errors.txt", 'a') do
                            |file| file.write("#{member_no}\t#{name}\t#{member_type}\t  is error. valid till is #{valid_till} or #{date_for_valid_till} but no expiry date \n") 
                        end
                    end
                end
                
            end
            if count%100 == 0
                print "."
            end
        rescue => ex
            error_member << member_no

            error[ex] = [] unless error[ex].present?
            error[ex] << member_no
            puts "#{member_no} this is error ----------- #{ex}"

            File.open("#{error_folder_name}/unknown_error_members.txt", 'a') do
                |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error.  #{ex} \n") 
            end
        end
    else
        ### member number nor member type is missing
        puts "  puts into no member no or member type"
        invalid_members_hash['no_member_no_nor_member_type'] = [] unless invalid_members_hash['no_member_no_nor_member_type'].present?
        invalid_members_hash['no_member_no_nor_member_type'] << member_no

        invalid_members << member_no
        # puts "\nERROR = member_no is #{member_no}, member_type is #{member_type} - missing member number nor member type"

        File.open("#{error_folder_name}/no_name_nor_member_type_members.txt", 'a') do
            |file| file.write("#{member_no}\t#{name}\t#{member_type}\t#{member_no} is error. The member name nor member type is not present for member number #{member_no}. \n") 
        end

    end    
end


puts "error_member count - #{error_member.count}"
puts "valid member count - #{valid_members.count}"
puts "new member count - #{new_members.count}"
puts "invalid member count - #{invalid_members.count}"
puts "total member count = #{valid_members.count + invalid_members.count + error_member.count + new_members.count}"

puts "Total count in excel is #{count}"
# puts "valid_members_hash['valid_member'] is #{valid_members_hash['valid_member']}"
# puts "invalid_members_hash is #{invalid_members_hash.keys}"

# invalid_members_hash.each do |key, value|
#     puts "#{key} - #{value.count}"
# end

# puts "valid member -----"
# valid_members_hash.each do |key, value|
#     puts "#{key} - #{value.count}"
# end


# puts "error -----"
# error.each do |key, value|
#     puts "#{key} - #{value}"
# end







__END__
