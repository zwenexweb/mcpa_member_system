require 'fileutils'
dirname = File.dirname("/mnt/mcpa_sg1_volume/backups")
unless File.directory?(dirname)
  FileUtils.mkdir_p(dirname)
end

namespace :db do

    desc "Dumps the database to backups"
    task :dump => :environment do
        cmd = nil
        with_config do |app, host, db, user|
            # cmd = "pg_dump -F c -v -h #{host} -d #{db} -U mcpa_database_admin -f #{Rails.root}/db/backups/#{Time.now.strftime("%Y%m%d%H%M%S")}_#{db}.psql"
            cmd = "PGPASSWORD='mcpa_database_admin' pg_dump -h #{host} -d #{db} -U mcpa_database_admin > #{dirname}/#{Time.now.strftime("%Y%m%d%H%M%S")}_#{db}.bak"
        end
        puts cmd
        exec cmd
    end

    desc "Restores the database from backups"
    task :restore, [:date] => :environment do |task,args|
        if args.present?
            cmd = nil
            with_config do |app, host, db, user|
                cmd = "PGPASSWORD='mcpa_database_admin' psql -U mcpa_database_admin #{db} < #{Rails.root}/db/backups/20181126104610_mcpa_member_application.bak"
            end
            Rake::Task["db:drop"].invoke
            Rake::Task["db:create"].invoke
            puts cmd
            exec cmd
        else
            puts 'Please pass a date to the task'
        end
    end

    private

    def with_config
        yield Rails.application.class.parent_name.underscore,
        ActiveRecord::Base.connection_config[:host],
        ActiveRecord::Base.connection_config[:database],
        ActiveRecord::Base.connection_config[:username]
    end

end
