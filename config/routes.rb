Rails.application.routes.draw do
	
  resources :degrees
  resources :photos
  # dashboard links for member

  # CSV Exports for ecmembers, committee members, members
  get "/export-ecmembers" => 'ecmembers#csv_export', as: 'csv_export_ecmembers'
  get "/export-committee_members" => 'committee_members#csv_export', as: 'csv_export_committee_members'
  get "/export-members" => 'members#csv_export', as: 'csv_export_members'


  get "/d/announcements" => "dashboard#announcements"
  get "/d/announcement/:id" => "dashboard#announcement", as: 'd_announcement'
  get "/d/attachments" => "dashboard#attachments"
  get "/d/attachment/:id" => "dashboard#attachment", as: 'd_attachment'
  get "/d/members" => "dashboard#members"
  get "/d/member_information" => "dashboard#member"
  get "/d/organizations" => "dashboard#organizations"
  get "/d/ecmembers" => "dashboard#ecmembers"
  get "/d/committees" => "dashboard#committees"
  get "/d/committee_members" => "dashboard#committee_members"



  # Edit password only
  get '/users/edit_information' => "users#edit_information", as: 'user_edit_information'
  post '/users/update_information' => "users#update_information", as: 'user_update_information'


  post '/users' => 'users#create'
  devise_for :users, controllers: {
      sessions: 'users/sessions'
  }, path_names: {sign_up: 'creating_an_account_is_not_an_option' }
  
  resources :api_keys
  resources :announcements

  resources :committee_members
  resources :ecmembers
  resources :members
  resources :users
  
  resources :membership_applications
  resources :memberships
  resources :attachments
  resources :requests
  
  resources :committees
  resources :organizations

  # resources :access_controls
  # resources :permissions
  # resources :events
  # resources :member_data
  # resources :comittees
  
  get '/apply' => 'membership_applications#new', as: 'new_application'
	
  resources :states
  resources :townships
  resources :staffs

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # township populated when state is chosen
  get '/state_search' => 'membership_applications#state_search', as: 'state_search'

  # Email and role populated according to staff or member id in user form
  get '/populate_by_staff_id' => 'users#populate_by_staff_id', as: 'populate_by_staff_id'
  get '/populate_by_member_id' => 'users#populate_by_member_id', as: 'populate_by_member_id'

  root to: 'dashboard#index' 
  get 'index.xlsx' => 'members#export_excel'



  # Extend/Upgrade membership
  get '/memberships/:id/upgrade' => 'memberships#upgrade', as: 'upgrade_membership'
  post '/memberships/:id/upgrade' => 'memberships#upgraded'
  get '/memberships/:id/extend' => 'memberships#extend', as: 'extend_membership'
  post '/memberships/:id/extend' => 'memberships#extended'

  get 'requests/:id/accept' => 'requests#accept', as: 'accept_request'
  get 'requests/:id/reject' => 'requests#reject', as: ''

  get '/reports' => "report#index", as: 'report'

  get '/successful' => "membership_applications#successful"
  get '/accept/:id' => "membership_applications#accept", as: 'accept_membership_application_request'
  get '/reject/:id' => "membership_applications#reject", as: 'reject_membership_application_request'
  get '/done' => "membership_applications#done"
 
# CSV import for existing member list for each organization
  get '/csv_import' => 'members#csv_import', as: "csv_import"
  post '/member_list_csv_import' => 'members#member_list_csv_import', as: "member_list_csv_import"


# API key and Api 
  get '/display' => 'api_version1#display'
  get '/access_control/:id' => 'api_version1#access_control'
  get '/announcement/:id' => 'api_version1#announcement'
  get '/attachment/:id' => 'api_version1#attachment'


  get '/ecmember/:id' => 'api_version1#ecmember'
  get '/event/:id' => 'api_version1#event'
  get '/member_datum/:id' => 'api_version1#member_datum'
  get '/member/:id' => 'api_version1#member'
  get '/membership_application/:id' => 'api_version1#membership_application'
  get '/membership/:id' => 'api_version1#membership'
  get '/organization/:id' => 'api_version1#organization'

  get '/staff/:id' => 'api_version1#staff'
  get '/state/:id' => 'api_version1#state'
  get '/township/:id' => 'api_version1#township'
  get '/user/:id' => 'api_version1#user'


  get '/member_info_edit' => "members#member_info_edit"
  post '/member_info_edit' => "members#member_info_update"


  get '/member_search' => "ecmembers#member_search"


  get "/organization_lvl_report" => "report#organization_lvl_report"
end