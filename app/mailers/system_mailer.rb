class SystemMailer < ApplicationMailer

    # When a new application is received
    def application_received
        # mail(to: "mknyein@zwenex.com", subject: "Testing - application_received")
        mail(to: "office@mcpamyanmar.org", subject: "application_received")
    end

    # email to inform admin or staff about the new membership application
    def application_is_ready_for_review(membership_application)
        @membership_application = membership_application
        mail_to_organization = State.find_by_name(@membership_application.state).organization || Organization.first
        
        mail(to: mail_to_organization.email, subject: "application_is_ready_for_review")
        # mail(to: "mknyein@zwenex.com", subject: "Testing - New Membership is reday to review")
        # mail(to: "office@mcpamyanmar.org", subject: "Testing - application_is_ready_for_review")
    end

    def membership_application_rejected
        # mail(to: "mknyein@zwenex.com", subject: "Testing - membership_application_rejected")
        mail(to: "office@mcpamyanmar.org", subject: "Membership Application is rejected")
    end

    # email to inform newly accepted member 
    # email will include password
    def membership_application_accepted(user, password)
        @user = user
        @password = password

        # mail(to: "mknyein@zwenex.com", subject: "Testing - Membership Application Accepted membership_application_accepted")
        mail(to: @user.email, subject: "Membership Application Accepted membership_application_accepted")
    end

    # email to inform the applicant that the membership request is successifully received by MCPA
    def new_membership_welcome_mail(membership_application)
        # puts "working two"
        # puts "new_membersip_welcome_mail"
        @membership_application = membership_application
        mail_to_organization = State.find_by_name(@membership_application.state).organization || Organization.first
        
        mail(to: mail_to_organization.email, subject: "Testing - application_is_ready_for_review")
        # mail(to: "mknyein@zwenex.com", subject: "This mail is for new applicant")
        # mail(to: "office@mcpamyanmar.org", subject: "Testing - application_is_ready_for_review")
    end

    # email to inform admin or staff about new upgrade request is received
    def upgrade_membership_received(member, request)
        @member = member
        @request = request
        mail_to_organization = @member.organizations.last

        # mail(to: "mknyein@zwenex.com", subject: "Testing - upgrade_membership_received")
        mail(to: mail_to_organization.email, subject: "Testing - upgrade_membership_received")
        # mail(to: "office@mcpamyanmar.org", subject: "Testing - upgrade_membership_received")
    end

    # email to inform the upgrading membership request is accepted
    # email will include 
    def upgrade_membership_accept(request)
        @request = request
        @member = request.member

        mail(to: @member.user.email, subject: "Testing - Upgrading Request Accepted")
    end

    # email to inform admin or staff about new extend request is received
    def extend_membership_received(member, request)
        @member = member
        @request = request
        mail_to_organization = @member.organizations.last

        # mail(to: "mknyein@zwenex.com", subject: "Testing - extend_membership_received")
        mail(to: mail_to_organization.email, subject: "Testing - extend_membership_received")
        # mail(to: "office@mcpamyanmar.org", subject: "Testing - extend_membership_received")
    end

    # email to inform the extending membership request is accepted 
    # email will include 
    def extend_membership_accept(member)
        @member = member

        # mail(to: "mknyein@zwenex.com", subject: "Testing - Extending Request Accepted")
        mail(to: @member.user.email, subject: "Testing - Extending Request Accepted")
    end

end

