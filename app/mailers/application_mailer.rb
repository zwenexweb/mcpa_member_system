class ApplicationMailer < ActionMailer::Base
    default from: 'auto@zwenex.com'
    layout 'mailer'

end
