class Request < ApplicationRecord
	validates_presence_of :request_type

	belongs_to :organization, optional: true
	belongs_to :member
end
