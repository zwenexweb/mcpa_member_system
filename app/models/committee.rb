class Committee < ApplicationRecord
	validates_presence_of :name
	has_many :announcements
	# has_many :committee_members
	has_many :members, through: :committee_members

	belongs_to :organization

end
