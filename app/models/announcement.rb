class Announcement < ApplicationRecord
	validates_presence_of :title, :date, :content

	belongs_to :organization, optional: true
	belongs_to :committee, optional: true
end
