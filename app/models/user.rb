class User < ApplicationRecord
	validates_presence_of :role, :email

    has_many :permissions
    has_many :access_controls, through: :permissions

    belongs_to :member, optional: true
    belongs_to :staff, optional: true

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :rememberable, :registerable,# :recoverable,
         :trackable, :validatable

	validates_numericality_of :member_id, allow_nil: true
    validates_numericality_of :staff_id, allow_nil: true
    validate :member_xor_staff

    has_attached_file :profile, styles: { large: "600x800>", medium: "300x300>", thumb: "30x30#" }, default_url: "/images/:style/missing.png"

    validates_attachment :profile, optional: true,
      content_type: { content_type: %w(image/jpeg image/jpg image/png) },
      size: { in: 0..2.megabytes }

    private
	def member_xor_staff
		unless member_id.blank? ^ staff_id.blank?
			errors.add(:base, "Specify member or staff, not both")
		end
        unless (member_id.present? && Member.find(member_id).present?) || (staff_id.present? && Staff.find(staff_id).present?)
            errors.add(:base, "Specify member or staff, not both")
        end
	end

end
