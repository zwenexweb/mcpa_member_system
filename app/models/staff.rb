class Staff < ApplicationRecord
	validates_presence_of :name, :email, :role
	validates_uniqueness_of :email
	
	has_one :user

	belongs_to :organization
end
