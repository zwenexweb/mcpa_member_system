class CommitteeMember < ApplicationRecord
	validates_presence_of :role
	belongs_to :organization
	# belongs_to :committee
	belongs_to :member

	def self.to_csv(options = {}, committee_members)
		# puts "committee_members count to export is #{committee_members.count}"
		# puts "@committee_members count to export is #{self.count}"
		committee_members = committee_members || CommitteeMember.limit(5)
		attributes = %w{role year}
		member_attributes = %w{name member_no phone email state}
		organization_attributes = %w{organization_name}
		committee_attributes = %w{name}

		CSV.generate(headers: true) do |csv|
	      csv << %w{name member_no phone email state role year committee_name organization_name}
	      committee_members.each do |committee_member|
	        csv << member_attributes.map{ |attr| Member.find(committee_member.member_id).send(attr) } + attributes.map{ |attr| committee_member.send(attr) } + committee_attributes.map{ |attr| Committee.find(committee_member.committee_id).send(attr) } + organization_attributes.map{ |attr| Organization.find(committee_member.organization_id).send(attr) } 
	      end
	    end
	end

end
