class Organization < ApplicationRecord

	validates_presence_of :organization_name
    validates :organization_name, uniqueness: true

	has_many :memberships
	has_many :members, through: :memberships
    has_many :ecmembers
    has_many :staffs
    has_many :committees
    has_many :committee_members
    has_many :announcements
    has_many :requests

    belongs_to :state, optional: true

	# validates_presence_of :region, allow_nil: true
 #    validates_presence_of :country, allow_nil: true
    validate :country_xor_state

    private
	def country_xor_state
		unless country.blank? ^ state_id.blank?
			errors.add(:base, "Specify country or region, not both")
		end
        unless (country.present?) || (state_id.present? && State.find(state_id).present?)
            errors.add(:base, "Specify country or region, not both")
        end
	end
end
