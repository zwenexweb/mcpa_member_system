class MemberDatum < ApplicationRecord
	validates_presence_of :title, :description

	belongs_to :member
end
