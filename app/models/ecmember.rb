class Ecmember < ApplicationRecord

	validates_presence_of :organization_id, :member_id, :role, :year

    # One member can be only one role in each organization for specific years.
    validates :member_id, uniqueness: {scope: [:organization_id, :year]}
    # validates :role, uniqueness: {scope: [:organization_id, :year]}

	belongs_to :organization
	belongs_to :member

	def self.to_csv(options = {}, ecmembers)
		puts "ecmembers count to export is #{ecmembers.count}"
		puts "@ecmembers count to export is #{self.count}"
		ecmembers = ecmembers || Ecmember.limit(5)
		attributes = %w{role year}
		member_attributes = %w{name member_no phone email state}
		organization_attributes = %w{organization_name}

		CSV.generate(headers: true) do |csv|
	      csv << %w{name member_no phone email state organization_name role year}
	      ecmembers.each do |ecmember|
	        csv << member_attributes.map{ |attr| Member.find(ecmember.member_id).send(attr) } + organization_attributes.map{ |attr| Organization.find(ecmember.organization_id).send(attr) } + attributes.map{ |attr| ecmember.send(attr) }
	      end
	    end
	end

end
