class Event < ApplicationRecord
	validates_presence_of :from_date, :to_date, :from_time, :to_time, :place, :participant_type
end
