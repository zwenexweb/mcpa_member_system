class Comittee < ApplicationRecord
	validates_presence_of :name
	belongs_to :organization
end
