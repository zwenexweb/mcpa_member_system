class Membership < ApplicationRecord
	validates_presence_of :member_id, :organization_id, :valid_till, :grade

    # One member must be only in one organization with expiry date at certain grade.
    validates :member_id, uniqueness: {scope: [:organization_id, :grade, :valid_till]}

	belongs_to :member
	belongs_to :organization
end
