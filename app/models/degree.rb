class Degree < ApplicationRecord
	belongs_to :membership_application
	
	has_attached_file :degree_file, styles: { large: "600x800>", medium: "300x300>", small: "150x150>" }, default_url: "/images/:style/missing.png"

	validates_presence_of :degree_title
	validates_attachment :degree_file, presence: true,
	  content_type: { content_type: %w(image/jpeg image/jpg image/png application/pdf) },
	  size: { in: 0..2.megabytes }
end
