class Permission < ApplicationRecord
	validates_presence_of :access_control_id, :user_id
	
	belongs_to :user
	belongs_to :access_control
end
