class Photo < ApplicationRecord

	belongs_to :membership_application
	
	has_attached_file :avatar, styles: { large: "600x800>", medium: "300x300>", small: "150x150>" }, default_url: "/images/:style/missing.png"

	validates_attachment :avatar, presence: true,
	  content_type: { content_type: %w(image/jpeg image/jpg image/png application/pdf) },
	  size: { in: 0..2.megabytes }
end
