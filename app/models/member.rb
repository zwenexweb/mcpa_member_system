class Member < ApplicationRecord
	validates_uniqueness_of :member_no
	# validates_uniqueness_of :nrc, :member_no, optional: true#, :phone
	validates_presence_of :name, :state#, :township, :dob, :nrc, :father_name, :address,
	
	has_many :ecmembers
	has_many :member_data
	has_many :memberships
	has_many :organizations, through: :memberships

	has_one :user, dependent: :destroy
	# has_one :membership_application
	has_one :request
	has_one :committee_member
	has_one :committee, through: :committee_member

	belongs_to :membership_application, optional: true
	alias_attribute :name1, :name

	mmunicode_convert :burmese_name

	validate :phone_or_email?
	private
	def phone_or_email?
		unless phone.present? or email.present?
			errors.add(:base, "Specify phone or email")
		end
	end


	def self.to_csv(options = {}, members)
		members = members || Member.limit(5)
		attributes = %w{name burmese_name member_no email phone dob nrc father_name state highest_education current_employment employment_history areas_of_professional_working_experiences}

		CSV.generate(headers: true) do |csv|
	      csv << %w{name burmese_name member_no email phone dob nrc father_name state highest_education current_employment employment_history areas_of_professional_working_experiences }
	      members.each do |member|
	        csv << attributes.map{ |attr| member.send(attr) }
	      end
	    end

	  # CSV.generate(options) do |csv|
	  #   csv << column_names
	  #   guests.each do |guest|
	  #     csv << guest.attributes.values_at(*column_names)
	  #   end
	  # end
	end

end
