class Attachment < ApplicationRecord

	belongs_to :membership_application
	validates_presence_of :file_attachment,:membership_application_id
	
	has_attached_file :file_attachment, styles: { large: "600x800>", medium: "300x300>", small: "150x150>" }, default_url: "/images/:style/missing.png"

	validates_attachment :file_attachment, presence: true,
	  content_type: { content_type: %w(image/jpeg image/jpg image/png application/pdf) },
	  size: { in: 0..2.megabytes }
	# Validate filename
	# validates_attachment_file_name :file_attachment, matches: [/png\z/, /jpe?g\z/, /pdf\z/]
end

