class MembershipApplication < ApplicationRecord
	validates_presence_of :name, :phone, :email, :member_type, :state#, :dob, :father_name, :address, :township, :highest_education, :gender#, :nrc, :ref_no, :certificates, :areas_of_training, :efficiencies, :current_employment, :employment_history, :areas_of_professional_working_experiences, :application_status, :process_status, :certificates, :current_employment, :employment_history
	mmunicode_convert :burmese_name

	has_many :attachments
	has_many :photos
	has_many :degrees
	has_one :member
	# belongs_to :member, optional: true

	NRC_INITIAL_OPTIONS = [1,2,4,5,6,7,8,9,10,11,12,13,14]
	NRC_MIDDLE_OPTIONS = ["နိုင်", "ဧည့်", "ပြု"]
	NRC_MIDDLE_OPTIONS = ["Naing", "Ae", "Pyu"]


	validates_uniqueness_of :nrc_no, scope: [:nrc_initial, :nrc_tsp]
	validate :set_nrc

	def set_nrc
		if nrc.blank?
			if nrc_tsp.blank?
				errors.add(:nrc_tsp, "can't be blank")
			elsif nrc_tsp.present?
				errors.add(:nrc_tsp, "must be letter") unless nrc_tsp =~ /[a-z A-Z]/
			end
			if nrc_no.blank?
				errors.add(:nrc_no, "can't be blank")
			end
			if nrc_middle.blank?
				errors.add(:nrc_middle, "can't be blank")
			end
			if nrc_initial.blank?
				errors.add(:nrc_initial, "can't be blank")
			end
		else
			# NRC is not initial/township(middle)number
		end
	end

    has_attached_file :profile, styles: { large: "600x800>", medium: "300x300>", thumb: "25x25#" }, default_url: "/images/:style/missing.png"

    validates_attachment :profile, optional: true,
      content_type: { content_type: %w(image/jpeg image/jpg image/png) },
      size: { in: 0..2.megabytes }

end
