class AccessControl < ApplicationRecord

	validates_presence_of :key, :name
	
	has_many :permissions
    has_many :users, through: :permissions

end
