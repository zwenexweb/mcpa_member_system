json.extract! staff, :id, :name, :email, :role, :created_at, :updated_at
json.url staff_url(staff, format: :json)
