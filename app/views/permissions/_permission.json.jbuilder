json.extract! permission, :id, :assess_control_id, :user_id, :created_at, :updated_at
json.url permission_url(permission, format: :json)
