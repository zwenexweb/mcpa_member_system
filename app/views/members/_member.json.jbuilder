json.extract! member, :id, :name, :dob, :nrc, :father_name, :address, :township, :sate, :phone, :email, :highest_education, :areas_of_tranining, :efficiencies, :areas_of_professional_working_experiences, :created_at, :updated_at
json.url member_url(member, format: :json)
