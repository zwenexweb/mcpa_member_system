json.extract! member_datum, :id, :date, :content_catagory, :title, :description, :created_at, :updated_at
json.url member_datum_url(member_datum, format: :json)
