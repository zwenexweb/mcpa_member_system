json.extract! request, :id, :member_id, :request_type, :staff_id, :created_at, :updated_at
json.url request_url(request, format: :json)
