json.extract! event, :id, :form_date, :to_date, :form_time, :to_time, :place, :participant_type, :created_at, :updated_at
json.url event_url(event, format: :json)
