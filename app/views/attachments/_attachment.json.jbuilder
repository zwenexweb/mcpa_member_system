json.extract! attachment, :id, :membership_application_id, :created_at, :updated_at
json.url attachment_url(attachment, format: :json)
