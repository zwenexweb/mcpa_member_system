json.extract! access_control, :id, :key, :name, :description, :created_at, :updated_at
json.url access_control_url(access_control, format: :json)
