json.extract! membership, :id, :member_id, :organization_id, :valid_till, :grade, :created_at, :updated_at
json.url membership_url(membership, format: :json)
