json.extract! user, :id, :role, :staff_id, :member_id, :created_at, :updated_at
json.url user_url(user, format: :json)
