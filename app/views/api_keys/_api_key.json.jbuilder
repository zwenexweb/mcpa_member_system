json.extract! api_key, :id, :key, :email, :name, :phone, :created_at, :updated_at
json.url api_key_url(api_key, format: :json)
