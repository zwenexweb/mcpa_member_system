json.extract! degree, :id, :degree_title, :membership_application_id, :created_at, :updated_at
json.url degree_url(degree, format: :json)
