json.extract! committee_member, :id, :title, :organization_id, :committee_id, :member_id, :created_at, :updated_at
json.url committee_member_url(committee_member, format: :json)
