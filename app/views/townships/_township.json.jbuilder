json.extract! township, :id, :division, :state_id, :name, :name_mm, :created_at, :updated_at
json.url township_url(township, format: :json)
