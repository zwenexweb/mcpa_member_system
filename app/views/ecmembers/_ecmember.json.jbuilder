json.extract! ecmember, :id, :organization_id, :role, :year, :created_at, :updated_at
json.url ecmember_url(ecmember, format: :json)
