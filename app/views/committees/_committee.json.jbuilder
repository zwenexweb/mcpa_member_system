json.extract! committee, :id, :name, :organization_id, :created_at, :updated_at
json.url committee_url(committee, format: :json)
