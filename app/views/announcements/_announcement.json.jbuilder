json.extract! announcement, :id, :title, :date, :content, :created_at, :updated_at
json.url announcement_url(announcement, format: :json)
