json.extract! comittee, :id, :name, :organization_id, :created_at, :updated_at
json.url comittee_url(comittee, format: :json)
