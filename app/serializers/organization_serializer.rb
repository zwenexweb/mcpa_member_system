class OrganizationSerializer < ActiveModel::Serializer
  attributes :organization_name, :country

  has_many :announcements
  # has_many :committees
  has_many :members
  has_many :memberships
  has_many :staffs
  has_many :users

  belongs_to :state
  
end
