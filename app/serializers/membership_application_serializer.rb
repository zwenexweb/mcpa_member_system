class MembershipApplicationSerializer < ActiveModel::Serializer
  attributes :name, :burmese_name, :email, :phone, :gender, :dob, :nrc, :father_name, :address, :state, :township, :highest_education, :member_type

  has_many :attachments

  belongs_to :member
end
