class StaffSerializer < ActiveModel::Serializer
  attributes :name, :email, :role, :position

  has_one :user

  belongs_to :organization
end
