class EcmemberSerializer < ActiveModel::Serializer
  attributes :id, :role, :year

  belongs_to :member
end
