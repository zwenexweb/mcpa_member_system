class UserSerializer < ActiveModel::Serializer
  attributes :email, :role

  has_many :permissions
  has_many :access_controls, through: :permissions

  belongs_to :staff
  belongs_to :member
end
