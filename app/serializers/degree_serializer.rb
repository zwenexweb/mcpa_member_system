class DegreeSerializer < ActiveModel::Serializer
  attributes :id, :degree_title, :membership_application_id
end
