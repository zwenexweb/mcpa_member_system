class AttachmentSerializer < ActiveModel::Serializer
  attributes :attachment_type

  belongs_to :membership_application
end
