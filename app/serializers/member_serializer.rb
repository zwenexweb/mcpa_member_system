class MemberSerializer < ActiveModel::Serializer
  attributes :name, :burmese_name, :email, :phone, :gender, :dob, :nrc, :father_name, :address, :state, :township, :highest_education, :member_no, :application_date, :acceptance_date

	has_many :ecmembers
	has_many :member_data
	has_many :memberships
	has_many :organizations, through: :memberships
	
	has_one :user
	has_one :membership_application
	has_one :request
end
