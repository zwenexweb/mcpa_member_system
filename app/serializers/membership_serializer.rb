class MembershipSerializer < ActiveModel::Serializer
  attributes :valid_till, :grade

  belongs_to :organization
  belongs_to :member
end
