class MemberDatumSerializer < ActiveModel::Serializer
  attributes :date, :title, :description, :content_catagory

  belongs_to :member
end
