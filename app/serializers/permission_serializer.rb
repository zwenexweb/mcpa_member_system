class PermissionSerializer < ActiveModel::Serializer
  belongs_to :user
  belongs_to :access_control
end
