class StateSerializer < ActiveModel::Serializer
  attributes :name

  has_many :organizations
  has_many :townships
end
