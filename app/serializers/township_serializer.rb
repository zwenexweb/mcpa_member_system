class TownshipSerializer < ActiveModel::Serializer
  attributes :name

  belongs_to :state
end
