class CommitteeSerializer < ActiveModel::Serializer
  attributes :id, :title

  has_many :announcements

  belongs_to :organization

end
