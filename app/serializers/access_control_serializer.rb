class AccessControlSerializer < ActiveModel::Serializer
  attributes :key, :name
	
	has_many :permissions
    has_many :users, through: :permissions
end
