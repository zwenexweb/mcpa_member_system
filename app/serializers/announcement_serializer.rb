class AnnouncementSerializer < ActiveModel::Serializer
  attributes :id, :title, :date, :content, :member_type

  belongs_to :organization
  belongs_to :committee

end
