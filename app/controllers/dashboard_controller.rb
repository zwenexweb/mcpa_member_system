class DashboardController < ApplicationController

    before_action :is_login?
    before_action :is_member?, except: [:index]
    before_action :is_ec?, only: [:ecmembers]
    # before_action :is_committee_member?, only: [:committee_members, :committees]

	def index
    unless current_user.role == "Member"
    	date = DateTime.now - 30.days #1.month
      if current_user.role == 'Staff'
        @members = current_user.staff.organization.members.last(10)
      else
        @members = Member.last(10)
      end
    end
	end

  def announcements
    if current_user.role != "Member"
      @announcements = Announcement.all.order(date: :DESC)
    else
      member = current_user.member
      membership = member.memberships.last
      @announcements1 = Announcement.where("member_type LIKE ? AND organization_id IS NULL", membership.grade).order(date: :DESC)
      @announcements2 = Announcement.where("member_type LIKE ? AND organization_id = ?", '', member.organizations.last.id).order(date: :DESC)
      @announcements3 = Announcement.where("member_type LIKE ?  AND organization_id = ?", membership.grade, member.organizations.last.id).order(date: :DESC)
      @announcements4 = member.is_world_member ? Announcement.where("organization_id = ?", Organization.where.not(country: nil).first.id) : []
      @announcements = @announcements1 + @announcements2 + @announcements3 + @announcements4
      @announcements = @announcements
    end
  end
  def announcement
      @announcement = Announcement.find(params[:id])
  end

  def attachments
    if user_signed_in? && current_user.role != 'Member'
      @attachments = Attachment.all
      @attachments = @attachments.joins(:membership_application).where("lower(name) LIKE ?", "%#{params[:search_by].downcase}%") if params[:search_by].present?
        @attachments = @attachments.where("attachment_type = ? ", params[:search_attachment_type]) if params[:search_attachment_type].present?
    else
      @membership_application = current_user.member.membership_application
      @attachments = @membership_application.present? ? @membership_application.attachments : Attachment.where("id = 0")
    end
    @attachments = @attachments.paginate(page: params[:page], per_page: 20)
  end
  def attachment
      @attachment = Attachment.find(params[:id])
  end

  def members
    date_search = Date.parse(params[:date_search]) unless params[:date_search].nil? || params[:date_search].blank?
    if current_user.role =="Admin"
      @members = Member.all
      @members = @members.joins(:memberships)#.joins("LEFT JOIN organizations ON organizations.id == organization_id")#.where("name LIKE ?", "%one%")
    elsif current_user.role == "Staff"
      @members = current_user.staff.organization.members
    else
      @members = current_user.member.organizations.present? ? current_user.member.organizations.first.members : nil
    end

    if params[:search_by].present?
      @members = @members.where('lower(name) LIKE ? OR lower(phone) LIKE ? OR lower(email) LIKE ? OR nrc LIKE ?', "%#{params[:search_by]}%", "%#{params[:search_by]}%", "%#{params[:search_by]}%", "%#{params[:search_by]}%")
    end
    if params[:date_search].present? || params[:grade_search].present? || params[:organization_search].present?
        if params[:date_search].present?
          @members = @members.where('DATE(valid_till) <= ?', date_search)
        end
        if params[:grade_search].present?
          # params[:grade_search] is the value of the grade in the membership, not the id of the membership
          @members = @members.joins(memberships).where("memberships.grade LIKE ?", "#{params[:grade_search]}")
        end

        if params[:organization_search].present?
          # Need to improve code
          # First I was trying to left-join three tables. 
          # Currently only two talbes (members and memberships) joined
          # To filter by organizations' name, I use params[:organization_search] with id of organization, not with the name of the organization in member index
          # @members = Organization.find(params[:organization_search]).members
          @members = @members.where('organization_id = ?', "#{params[:organization_search]}")
        end
    end
    @members = @members.order(:id).paginate(:page => params[:page], :per_page => 15)
  end

  def member
    @price = 0
    # # Passing member_no instead of member id for security
    # @member = Member.find_by_member_no(params[:member_id])
    @member = current_user.member

    @membership = @member.memberships.first
    @price = 2000 if @membership.grade == "SM" || @membership.grade == "Student Member"
    @price = 4000 if @membership.grade == "AM" || @membership.grade == "Associate Member"
    @price = 6000 if @membership.grade == "PM" || @membership.grade == "Professional Member"
    @price = 0 if @membership.grade == "FM" || @membership.grade == "Fellow Member"

  end

  def organizations
    @organizations = Organization.order(:id).paginate(:page => params[:page], :per_page => 20)
  end

  def ecmembers
    if current_user.role == "Admin" 
      @ecmembers = Ecmember.all
    elsif current_user.role == 'Staff'
      @ecmembers = current_user.staff.organization.ecmembers
    else
      @ecmembers = current_user.member.organizations.last.ecmembers
    end
    if params[:search_role].present?
      @ecmembers = @ecmembers.where('role = ?', params[:search_role])
    end
    if params[:search_year].present?
      @ecmembers = @ecmembers.where("year = ?", params[:search_year])
    end
    if params[:search_by].present?
      @ecmembers = @ecmembers.joins(:member)
      if current_user.role == "Member"
        @ecmembers = @ecmembers.where('name LIKE ?', "%#{params[:search_by]}%")
      else
        @ecmembers = @ecmembers.joins(:organization)
        @ecmembers = @ecmembers.where('lower(name) LIKE ? OR lower(organization_name) LIKE ?', "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%")
      end
    end
    @ecmembers = @ecmembers.order(:year, :organization_id).paginate(:page => params[:page], :per_page => 20)
  end

  def committees
    if current_user.role == "Member"
      @committees = current_user.member.organizations.last.committees.paginate(:page => params[:page], :per_page => 20)
    else
      @committees = Committee.all
      @committees = @committees.where('lower(name) LIKE ?', "%#{params[:search_by].downcase}%") if params[:search_by].present?
      @committees = @committees.where('organization_id = ?', "#{params[:select_organization]}") if params[:select_organization].present?
      @committees = @committees.order(:id).paginate(:page => params[:page], :per_page => 20)
    end
  end

  def committee_members
    @committee_members = CommitteeMember.all
  end
    
    private
    def is_login?
      unless user_signed_in? 
          redirect_to new_user_session_path, notice: "Please sign-in."
      end
    end
    def is_member?
      unless current_user.role == 'Member'
          redirect_to root_path
      end
    end
    def is_ec?
      unless current_user.role == 'Member' && current_user.member.ecmembers.present?
          redirect_to root_path
      end
    end
    # def is_committee_member?
    #   unless current_user.role == 'Member'
    #       redirect_to root_path
    #   end
    # end
end
