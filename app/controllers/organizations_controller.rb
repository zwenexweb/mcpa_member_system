class OrganizationsController < ApplicationController
  before_action :is_login?
  before_action :is_staff?
  before_action :is_admin_or_head?, only: [:destroy, :edit, :update, :new, :create]
  before_action :is_admin?, only: [:destroy]
  before_action :set_organization, only: [:show, :edit, :update, :destroy]

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.order(:id).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
  end

  # GET /organizations/new
  def new
    @organization = Organization.new
  end

  # GET /organizations/1/edit
  def edit
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(organization_params)

    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: 'Organization was successfully created.' }
        format.json { render :show, status: :created, location: @organization }
      else
        format.html { render :new }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1
  # PATCH/PUT /organizations/1.json
  def update
    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to @organization, notice: 'Organization was successfully updated.' }
        format.json { render :show, status: :ok, location: @organization }
      else
        format.html { render :edit }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization.destroy
    respond_to do |format|
      format.html { redirect_to organizations_url, notice: 'Organization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_params
      params.require(:organization).permit(:organization_name, :country, :state_id, :short_name, :email)
    end

    def is_login?
      unless user_signed_in?
        redirect_to new_user_session_path, notice: 'Login first.'
      end
    end

    # Only head and admin can CRUD this model
    def is_staff?
      unless current_user.role != 'Member'
        redirect_to root_path
      end
    end
    def is_admin_or_head?
      unless current_user.role == "Admin" || current_user.role == 'Head'
        redirect_to organizations_path, notice: "Access denied."
      end
    end

    def is_admin?
      unless current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
