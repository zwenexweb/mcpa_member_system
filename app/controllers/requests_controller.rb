class RequestsController < ApplicationController
  before_action :is_staff?
  before_action :set_request, only: [:show, :edit, :update, :destroy, :accept, :reject]
  # before_action :is_admin?, except: [:index, :show]
  
  # GET /requests
  # GET /requests.json
  def index
    if current_user.role == "Admin"
      @requests = Request.all
    elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
      @requests = Request.all
    else
      @requests = current_user.staff.organization.requests
    end
    
  end

  # GET /requests/1
  # GET /requests/1.json
  def show
  end

  # GET /requests/new
  def new
    @request = Request.new
  end

  # GET /requests/1/edit
  def edit
  end

  # POST /requests
  # POST /requests.json
  def create
    puts params[:request][:request_type]
    @request = Request.new(request_params)
    @request.organization_id = Member.find(params[:request][:member_id]).organizations.last.id if params[:request][:member_id].present?

    respond_to do |format|
      if @request.save
        format.html { redirect_to @request, notice: 'Request was successfully created.' }
        format.json { render :show, status: :created, location: @request }
      else
        format.html { render :new }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to @request, notice: 'Request was successfully updated.' }
        format.json { render :show, status: :ok, location: @request }
      else
        format.html { render :edit }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_url, notice: 'Request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def accept
    # membership request to extend/upgrade accept
    @member = @request.member
    if @member.present? 
      @member.log = @member.log.present? ? @member.log : {}
      @membership = @member.memberships.last if @member.memberships.present?

      if @request.request_type == "Extend" && @request.extended_month.present?
          # Extend membership
          valid_till = @membership.valid_till + @request.extended_month.years
          @member.log["Membership extend @ #{DateTime.now}"] = "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Valid-till - #{@membership.valid_till} "
          respond_to do |format|
            if @member.save && @membership.update(valid_till: valid_till, extended_at: DateTime.now, upgraded_at: nil) && @request.destroy
              SystemMailer.extend_membership_accept(@member).deliver_now
              format.html { redirect_to @membership, notice: 'Membership was successfully updated.' }
              format.json { render :show, status: :ok, location: @membership }
            
            else
              format.html { redirect_to requests_path, notice: 'Accepting the request fails.'}
            end
          end

      elsif @request.request_type == "Upgrade" && @request.upgraded_type.present?
        # puts "*********\n\n #{@request.member.email}  \n**************\n\n"
        # Upgrade to
        # @membership.valid_till = DateTime.now + 1.years
        respond_to do |format|
            if @request.upgraded_type == "sm_to_am"
              @membership.grade = 'Associate Member'
              @member.log["Grade updated @ #{DateTime.now}"] = "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Grade - SM to AM , Valid-till - #{@membership.valid_till}, fees - #{ 3000 + 4000} "
            elsif @request.upgraded_type == "am_to_pm"
              @membership.grade = 'Professional Member'
              @member.log["Grade updated @ #{DateTime.now}"] = "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Grade - AM to PM , Valid-till - #{@membership.valid_till}, fees - #{ 4000 + 6000} "
            elsif @request.upgraded_type == "pm_to_fm"
              @membership.grade = 'Fellow Member'
              @member.log["Grade updated @ #{DateTime.now}"] = "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Grade - SM to PM , Valid-till - #{@membership.valid_till}, fees - #{ 4000 + 6000} "
            else
                format.html { redirect_to root_path, notice: 'Membership upgrade fails.' }
                format.json { render json: @member.errors, status: :unprocessable_entity }
            end
            @membership.upgraded_at = DateTime.now
            @membership.extended_at = nil
            
            if @member.save && @membership.save && @request.destroy
              SystemMailer.upgrade_membership_accept(@request).deliver_now
              format.html { redirect_to @membership, notice: 'Membership was successfully updated.' }
              format.json { render :show, status: :ok, location: @membership }
            else
                format.html { redirect_to requests_path, notice: 'Accepting the request fails.' }
                format.json { render json: @member.errors, status: :unprocessable_entity }
            end
        end
      else
        @request.destroy
        redirect_to requests_path, notice: 'The request is invlid and it have been deleted.'
      end
    else
      puts 'destroyed'
      @request.destroy
      redirect_to requests_path, notice: 'The request is invlid and it have been deleted.'
    end

  end

  def reject
    # membership request to extend/upgrade reject
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      if current_user.role == "Admin"
        @requests = Request.all
      elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
        @requests = Request.all
      else
        @requests = current_user.staff.organization.requests
      end
      if @requests.ids.include? (params[:id].to_i)
        @request = Request.find(params[:id])
      else
        redirect_to requests_path, notice: 'Not Found.'
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_params
      params.require(:request).permit(:member_id, :request_type, :staff_id, :upgraded_type, :extended_month)
    end

    def is_staff?
      unless user_signed_in? && current_user.role != 'Member'
        redirect_to root_path
      end
    end
end
