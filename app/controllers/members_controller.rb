class MembersController < ApplicationController
  require 'csv'

  before_action :authenticate_user!
  before_action :is_staff?, only: [:new, :create, :edit, :update, :destroy]
  before_action :is_member?, only: [:member_info_edit]
  before_action :set_member, only: [:show, :edit, :update, :destroy, :member_info_edit, :member_info_update]

  # GET /members
  # GET /members.json
  def index
    date_search = Date.parse(params[:date_search]) unless params[:date_search].nil? || params[:date_search].blank?
    if current_user.role =="Admin"
      @members = Member.all
      @members = @members.joins(:memberships)#.joins("LEFT JOIN organizations ON organizations.id == organization_id")#.where("name LIKE ?", "%one%")
    elsif current_user.role == "Staff" || current_user.role == "Head"
      if current_user.staff.organization.country.present?
        # @members = Member.where.not(is_world_member: nil)
        @members = Member.all
      else
        @members = current_user.staff.organization.present? ? current_user.staff.organization.members : nil
      end
    else
      @members = current_user.member.organizations.present? ? current_user.member.organizations.last.members : nil
    end

    if params[:search_by].present?
      @members = @members.where('lower(name) LIKE ? OR lower(phone) LIKE ? OR lower(email) LIKE ? OR lower(nrc) LIKE ?', "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%") if params[:search_by].to_i == 0
      @members = @members.where('member_no = ?', params[:search_by].to_i) unless params[:search_by].to_i == 0
    end
    if params[:date_search].present? || params[:grade_search].present? || params[:organization_search].present?
        if params[:date_search].present?
          @members = @members.where('DATE(valid_till) <= ?', date_search)
        end
        if params[:grade_search].present?
          # params[:grade_search] is the value of the grade in the membership, not the id of the membership
          @members = @members.joins(:membershps).where("memberships.grade LIKE ?", "#{params[:grade_search]}")
        end

        if params[:organization_search].present?
          # Need to improve code
          # First I was trying to left-join three tables. 
          # Currently only two talbes (members and memberships) joined
          # To filter by organizations' name, I use params[:organization_search] with id of organization, not with the name of the organization in member index
          # @members = Organization.find(params[:organization_search]).members
          @members = @members.where('organization_id = ?', "#{params[:organization_search]}")
        end
    end
    @members = @members.where("highest_education = ?", "#{params[:highest_education]}") unless params[:highest_education].blank? || params[:highest_education].nil?

    @members = @members.where("'PROGRAMMING' = Any(areas_of_training)") unless params[:training_programming].blank? || params[:training_programming].nil?
    @members = @members.where("'SOFTWARE ENGINEERING' = Any(areas_of_training)") unless params[:training_software_engineering].blank? || params[:training_software_engineering].nil?
    @members = @members.where("'DATABASE' = Any(areas_of_training)") unless params[:training_database].blank? || params[:training_database].nil?
    @members = @members.where("'NETWORKING' = Any(areas_of_training)") unless params[:training_networking].blank? || params[:training_networking].nil?
    @members = @members.where("'WEB DEVELOPMENT' = Any(areas_of_training)") unless params[:training_web_development].blank? || params[:training_web_development].nil?
    @members = @members.where("'OPERATION SYSTEM' = Any(areas_of_training)") unless params[:training_operation_system].blank? || params[:training_operation_system].nil?
    @members = @members.where("'GRAPHIC DESIGN' = Any(areas_of_training)") unless params[:training_graphic_design].blank? || params[:training_graphic_design].nil?
    @members = @members.where("'MULTIMEDIA' = Any(areas_of_training)") unless params[:training_multimedia].blank? || params[:training_multimedia].nil?
    @members = @members.where("'HARDWARE' = Any(areas_of_training)") unless params[:training_hardware].blank? || params[:training_hardware].nil?
    @members = @members.where("'MIS' = Any(areas_of_training)") unless params[:training_mis].blank? || params[:training_mis].nil?

    @members = @members.where("'PASCAL' = Any(efficiencies)") unless params[:efficiencies_pascal].blank? || params[:efficiencies_pascal].nil? 
    @members = @members.where("'COBOL' = Any(efficiencies)") unless params[:efficiencies_cobol].blank? || params[:efficiencies_cobol].nil? 
    @members = @members.where("'ASSEMBLY' = Any(efficiencies)") unless params[:efficiencies_assembly].blank? || params[:efficiencies_assembly].nil? 
    @members = @members.where("'C/C++' = Any(efficiencies)") unless params[:efficiencies_c_c_plus_plus].blank? || params[:efficiencies_c_c_plus_plus].nil? 
    @members = @members.where("'BASIC' = Any(efficiencies)") unless params[:efficiencies_basic].blank? || params[:efficiencies_basic].nil? 
    @members = @members.where("'VISUAL BASIC' = Any(efficiencies)") unless params[:efficiencies_visual_basic].blank? || params[:efficiencies_visual_basic].nil? 
    @members = @members.where("'VISUAL C' = Any(efficiencies)") unless params[:efficiencies_visual_c].blank? || params[:efficiencies_visual_c].nil? 
    @members = @members.where("'JAVA' = Any(efficiencies)") unless params[:efficiencies_java].blank? || params[:efficiencies_java].nil? 
    @members = @members.where("'NET' = Any(efficiencies)") unless params[:efficiencies_net].blank? || params[:efficiencies_net].nil? 
    @members = @members.where("'ASP' = Any(efficiencies)") unless params[:efficiencies_asp].blank? || params[:efficiencies_asp].nil? 
    @members = @members.where("'PERL' = Any(efficiencies)") unless params[:efficiencies_perl].blank? || params[:efficiencies_perl].nil? 
    @members = @members.where("'PHP' = Any(efficiencies)") unless params[:efficiencies_php].blank? || params[:efficiencies_php].nil? 
    @members = @members.where("'JSP' = Any(efficiencies)") unless params[:efficiencies_jsp].blank? || params[:efficiencies_jsp].nil? 
    @members = @members.where("'COLD FUSION' = Any(efficiencies)") unless params[:efficiencies_cold_fusion].blank? || params[:efficiencies_cold_fusion].nil? 
    @members = @members.where("'HTML' = Any(efficiencies)") unless params[:efficiencies_html].blank? || params[:efficiencies_html].nil? 
    @members = @members.where("'LINUX' = Any(efficiencies)") unless params[:efficiencies_linux].blank? || params[:efficiencies_linux].nil? 
    @members = @members.where("'WINDOWS' = Any(efficiencies)") unless params[:efficiencies_windows].blank? || params[:efficiencies_windows].nil? 
    @members = @members.where("'APPLE' = Any(efficiencies)") unless params[:efficiencies_apple].blank? || params[:efficiencies_apple].nil? 
    @members = @members.where("'UNIX' = Any(efficiencies)") unless params[:efficiencies_unix].blank? || params[:efficiencies_unix].nil? 
    @members = @members.where("'DATABASE' = Any(efficiencies)") unless params[:efficiencies_database].blank? || params[:efficiencies_database].nil? 
    @members = @members.where("'SQL' = Any(efficiencies)") unless params[:efficiencies_sql].blank? || params[:efficiencies_sql].nil? 
    @members = @members.where("'FLASH' = Any(efficiencies)") unless params[:efficiencies_flash].blank? || params[:efficiencies_flash].nil? 
    @members = @members.where("'MULTIMEDIA' = Any(efficiencies)") unless params[:efficiencies_multimedia].blank? || params[:efficiencies_multimedia].nil? 
    @members = @members.where("'ACCESS' = Any(efficiencies)") unless params[:efficiencies_access].blank? || params[:efficiencies_access].nil?
    @members = @members.where("'WEB DESIGN' = Any(efficiencies)") unless params[:efficiencies_web_design].blank? || params[:efficiencies_web_design].nil?
    @members = @members.where("'VIDEO/ AUDIO' = Any(efficiencies)") unless params[:efficiencies_video_audio].blank? || params[:efficiencies_video_audio].nil?
    @members = @members.where("'HARDWARE' = Any(efficiencies)") unless params[:efficiencies_hardware].blank? || params[:efficiencies_hardware].nil?
    @members = @members.where("'NETWORKING' = Any(efficiencies)") unless params[:efficiencies_networking].blank? || params[:efficiencies_networking].nil?

    # @members = @members.where("'PROGRAMMING' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_programming].blank? || params[:working_experience_programming].nil?
    # @members = @members.where("'SOFTWARE ENGINEERING' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_software_engineering].blank? || params[:working_experience_software_engineering].nil?
    # @members = @members.where("'ANALYSIS AND DESIGN' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_analysis_and_design].blank? || params[:working_experience_analysis_and_design].nil?
    # @members = @members.where("'QUALITY ASSURANCE' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_quality_assurance].blank? || params[:working_experience_quality_assurance].nil?
    # @members = @members.where("'MULTIMEDIA' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_multimedia].blank? || params[:working_experience_multimedia].nil?
    # @members = @members.where("'WEB DEVELOPMENT' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_web_development].blank? || params[:working_experience_web_development].nil?
    # @members = @members.where("'CUSTOMER SUPPORT' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_customer_support].blank? || params[:working_experience_customer_support].nil?
    # @members = @members.where("'MANAGER' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_is_it_manager].blank? || params[:working_experience_is_it_manager].nil?
    # @members = @members.where("'OTHER MANAGEMENT' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_orther_management].blank? || params[:working_experience_orther_management].nil?
    # @members = @members.where("'DATABASE ADMINISTRATION' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_database_administration].blank? || params[:working_experience_database_administration].nil?
    # @members = @members.where("'SYSTEM MAINTANANCE' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_system_maintanance].blank? || params[:working_experience_system_maintanance].nil?
    # @members = @members.where("'SYSTEM ADMINISTRATION' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_systems_administration].blank? || params[:working_experience_systems_administration].nil?
    # @members = @members.where("'NETWORKING ADMINISTRAION' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_network_administration].blank? || params[:working_experience_network_administration].nil?
    # @members = @members.where("'NETWORK ENGINEERING' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_network_engineering].blank? || params[:working_experience_network_engineering].nil?
    # @members = @members.where("'RESEARCH AND DEVELOPMENT' = Any(areas_of_professional_working_experiences)") unless params[:working_experience_research_and_development].blank? || params[:working_experience_research_and_development].nil?
    @members = @members.order(:member_no).paginate(:page => params[:page], :per_page => 20)
  end

  def export_excel
    @members = Member.all
    respond_to do |format|
        format.xlsx
    end
  end
  # GET /members/1
  # GET /members/1.json
  def show
    unless @member.membership_application.present?
      puts 'no membership_application'
      membership_application = MembershipApplication.new(name: @member.name, nrc: @member.nrc, email: @member.email, phone: @member.phone, address: @member.address, state: @member.state, township: @member.township)
      membership_application.save(validate: false)
      @member.update(membership_application_id: membership_application.id)
    end
    unless @member.memberships.present?
      puts 'no membership'
      organization = State.find_by_name(@member.state).organization || Organization.first
      Membership.create(member_id: @member.id, organization_id: organization.id, valid_till: DateTime.now + 1.years, grade: @membership_application.member_type)
    end
    @membership = @member.memberships.first
    @price = 2000 if @membership.grade == "SM" || @membership.grade == "Student Member"
    @price = 4000 if @membership.grade == "AM" || @membership.grade == "Associate Member"
    @price = 6000 if @membership.grade == "PM" || @membership.grade == "Professional Member"
    @price = 0 if @membership.grade == "FM" || @membership.grade == "Fellow Member"
    if @price == nil
      @membership.update(grade: 'Student Member')
      @price = 2000
    end
  end

  # GET /members/new
  def new
    @member = Member.new  
  end

  # GET /members/1/edit
  def edit
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)
    @member.is_world_member = params[:member][:is_world_member].present? ? true : false
    @member.member_no = SecureRandom.hex(8)

    respond_to do |format|
      if @member.save

        @membership = Membership.new
        @membership.organization_id = State.find_by_name(@member.state).organization.present? ? State.find_by_name(@member.state).organization.id : Organization.first.id
        @membership.grade = @member.member_type
        @membership.valid_till = DateTime.now + 1.years
        @membership.member_id= @member.id

        if @membership.save
          format.html { redirect_to @member, notice: 'Member was successfully created.' }
          format.json { render :show, status: :created, location: @member }
        else
          format.html { redirect_to @member, notice: 'Member was successfully created. Please create Membership manually.' }
          format.json { render :show, status: :created, location: @member }
        end
      else
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update

    @membership = @member.memberships.last
    @member.log = @member.log.present? ? @member.log : {}
    @member.is_world_member = params[:member][:is_world_member].present? ? true : false

    if State.find_by_name(params[:member][:state]).organization
      @membership.organization_id = State.find_by_name(params[:member][:state]).organization.id
      @member.log["State changed"] = {"#{DateTime.now}" => "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Grade - #{@membership.grade}, Valid-till - #{@membership.valid_till}"} if @member.state != params[:member][:state]
    end

    respond_to do |format|
      if @member.update(member_params) && @membership.save
        format.html { redirect_to @member, notice: 'Member was successfully updated.' }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { render :edit }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: 'Member was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def csv_import
    
  end

  def member_list_csv_import
    if params[:state].present? and params[:csv].present?
      successful_member_count = 0
      unsuccessful_member_count = 0
      organization = Organization.find_by_state_id(params[:state].to_i)
      state = State.find(params[:state].to_i).name

      csv = params[:csv].read
      lines = csv.split("\n")

      lines[1..-1].each do |line|
        line = line.gsub(",,", ",'',")
        member = line.split(',')
        
        new_member = Member.new( member_no: member[0], member_type: member[1], name: member[2], father_name: member[3], dob: member[4], gender: member[5], nrc: member[7], current_employment: {"position" => member[8]}, current_employment: {"company" => member[9]}, highest_education: member[10], areas_of_professional_working_experiences: [member[11]], phone: member[12], email: member[13], application_date: member[14], acceptance_date: member[15], log: {}, state: state, address: member[17])
        if new_member.save
          new_membership = new_member.memberships.build(organization_id: organization.id, valid_till: DateTime.now(), grade: member[1])
          new_membership.save
          successful_member_count = successful_member_count + 1
        else
          unsuccessful_member_count = unsuccessful_member_count + 1
        end
      end
    end
  end
    

    def member_info_edit
    end

    def member_info_update

        unless @member.memberships.any?
            @member.update(state: "Yangon") unless @member.state.present?
            grade = @member.member_type || "Associated Member"
            organization_id = State.find_by_name(@member.state).organization.id
            @member.memberships.build(organization_id: organization_id, grade: member_type, valid_till: DateTime.now + 3.months)
        end

        @membership = @member.memberships.last
        @member.log = @member.log.present? ? @member.log : {}

        @member.name = params[:name]
        @member.burmese_name = params[:burmese_name]
        @member.nrc = params[:nrc]
        @member.father_name = params[:father_name]
        @member.phone = params[:phone]
        @member.email = params[:email]
        @member.address = params[:address]
        @member.certificates = params[:certificates]
        @member.areas_of_training = params[:areas_of_training]
        @member.efficiencies = params[:efficiencies]
        @member.areas_of_professional_working_experiences = params[:areas_of_professional_working_experiences]

        @member.log["Profile Edit"] = {"#{DateTime.now}" => "member_info - #{@member.to_json}"}

        respond_to do |format|
          if @member.save
            format.html { redirect_to @member, notice: 'Member was successfully updated.' }
            format.json { render :show, status: :ok, location: @member }
          else
            format.html { render :edit }
            format.json { render json: @member.errors, status: :unprocessable_entity }
          end
        end
    end


  def csv_export
    if current_user.role =="Admin"
      @members = Member.all
      @members = @members.joins(:memberships)#.joins("LEFT JOIN organizations ON organizations.id == organization_id")#.where("name LIKE ?", "%one%")
    elsif current_user.role == "Staff" || current_user.role == "Head"
      if current_user.staff.organization.country.present?
        # @members = Member.where.not(is_world_member: nil)
        @members = Member.all
      else
        @members = current_user.staff.organization.present? ? current_user.staff.organization.members : nil
      end
    else
      @members = current_user.member.organizations.present? ? current_user.member.organizations.last.members : nil
    end

    if params[:search_by].present?
      @members = @members.where('lower(name) LIKE ? OR lower(phone) LIKE ? OR lower(email) LIKE ? OR lower(nrc) LIKE ?', "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%") if params[:search_by].to_i == 0
      @members = @members.where('member_no = ?', params[:search_by].to_i) unless params[:search_by].to_i == 0
    end
    if params[:date_search].present? || params[:grade_search].present? || params[:organization_search].present?
        if params[:date_search].present?
          @members = @members.where('DATE(valid_till) <= ?', date_search)
        end
        if params[:grade_search].present?
          # params[:grade_search] is the value of the grade in the membership, not the id of the membership
          @members = @members.where('member_type = ?', "#{params[:grade_search]}")
        end

        if params[:organization_search].present?
          # Need to improve code
          # First I was trying to left-join three tables. 
          # Currently only two talbes (members and memberships) joined
          # To filter by organizations' name, I use params[:organization_search] with id of organization, not with the name of the organization in member index
          # @members = Organization.find(params[:organization_search]).members
          @members = @members.where('organization_id = ?', "#{params[:organization_search]}")
        end
    end

    respond_to do |format|
      format.csv { send_data @members.to_csv(@members) } 
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      if current_user.role == "Member"
        @member = current_user.member
      else
          if current_user.role =="Admin"
            @members = Member.all
          elsif current_user.role == "Staff" || current_user.role == "Head"
            if current_user.staff.organization.country.present?
              @members = Member.all
            else
              @members = current_user.staff.organization.present? ? current_user.staff.organization.members : nil
            end
          end
          if @members.ids.include? (params[:id].to_i)
            @member = Member.find(params[:id])
          else
            redirect_to members_path, notice: 'Not Found.'
          end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(:name, :dob, :nrc, :father_name, :address, :township, :state, :phone, :email, :highest_education, :areas_of_training, :efficiencies, :areas_of_professional_working_experiences, :member_no, :certificates, :current_employment, :employment_history, :nrc_initial, :nrc_middle, :nrc_tsp, :nrc_no, :member_type, :gender, :nrc_initial, :nrc_tsp, :nrc_middle, :nrc_no, :code_no, :burmese_name, :is_world_member, :membership_application_id, :remark)
    end

    # Only staff can CRUD this model
    def is_staff?
      if user_signed_in? && current_user.role == "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_member?
      unless user_signed_in? && current_user.role == "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
