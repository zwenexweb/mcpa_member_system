class CommitteeMembersController < ApplicationController
  before_action :is_staff?
  before_action :is_admin?, only: [:destroy]
  before_action :set_committee_member, only: [:show, :edit, :update, :destroy]

  # GET /committee_members
  # GET /committee_members.json
  def index
    ### staff can see only the committee members belongs to its organization while admin can see and CRUD all committee members
    if current_user.role == "Staff" || current_user.role == "Head"
      @committee_members = CommitteeMember.where(organization_id: current_user.staff.organization.id)
    else
      @committee_members = CommitteeMember.all
    end

    if params[:search_role].present?
      @committee_members = @committee_members.where('role = ?', params[:search_role])
    end
    if params[:search_year].present?
      @committee_members = @committee_members.where("year = ?", params[:search_year])
    end
    if params[:search_by].present?
        @committee_members = @committee_members.joins(:member)
        @committee_members = @committee_members.where('lower(name) LIKE ? ', "%#{params[:search_by].downcase}%")
    end

    @committee_members = @committee_members.order(:organization_id, :committee_id, :role).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /committee_members/1
  # GET /committee_members/1.json
  def show
  end

  # GET /committee_members/new
  def new
    ### staff can choose only the committee belongs to its organization while admin can choose all committees 
    ### staff can make only the members belongs to its organization to the committee member while admin can make any members to the committee member
    @committee_member = CommitteeMember.new
    @organization = current_user.staff.organization
    @organizations = Organization.all if current_user.role == 'Admin'
    @organizations = Organization.where(id: @organization.id) if current_user.role == "Staff" || current_user.role == "Head"
    @committees = Committee.all.compact if current_user.role == "Admin"
    @committees = @organization.committees if current_user.role == "Staff" || current_user.role == "Head"
    @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
    @members = Member.where.not(is_world_member: nil) if current_user.role == "Staff" || current_user.role == "Head" && current_user.staff.organization.country.present?
    @members = Member.all if current_user.role == 'Admin'
  end

  # GET /committee_members/1/edit
  def edit
    ### staff can choose only the committee belongs to its organization while admin can choose all committees 
    ### staff can make only the members belongs to its organization to the committee member while admin can make any members to the committee member
    @organization = current_user.staff.organization
    @organizations = Organization.all if current_user.role == 'Admin'
    @organizations = Organization.where(id: @organization.id) if current_user.role == "Staff" || current_user.role == "Head"
    @committees = Committee.all.compact
    @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
    @members = Member.where.not(is_world_member: nil) if current_user.role == 'Staff' && current_user.staff.organization.country.present?
    @members = Member.all if current_user.role == 'Admin'
    
  end

  # POST /committee_members
  # POST /committee_members.json
  def create
    @committee_member = CommitteeMember.new(committee_member_params)
    respond_to do |format|
    if @committee_member.valid? && Organization.find(params[:committee_member][:organization_id]).present? && Committee.find(params[:committee_member][:committee_id]).present?
        organization = Organization.find(params[:committee_member][:organization_id])
        @committees = organization.committees
        @committee = @committees.find_by_name(Committee.find(params[:committee_member][:committee_id]).name)
        unless @committee.present?
          @committee = Committee.create(name: Committee.find(params[:committee_member][:committee_id]).name, organization_id: params[:committee_member][:organization_id])
        end
        @committee_member = CommitteeMember.new(committee_member_params)
        @committee_member.committee_id = @committee.id

        if @committee_member.save
          format.html { redirect_to @committee_member, notice: 'Committee member was successfully created.' }
          format.json { render :show, status: :created, location: @committee_member }
        else
          @organization = current_user.staff.organization
          @committee_member = CommitteeMember.new
          @organizations = Organization.all if current_user.role == 'Admin'
          @organizations = Organization.where(id: @organization.id) if current_user.role == "Staff" || current_user.role == "Head"
          @committees = @organizations.first.committees if current_user.role == "Staff" || current_user.role == "Head"
          @committees = Committee.all.compact if current_user.role == "Admin"
          @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
          @members = Member.where.not(is_world_member: nil) if current_user.role == "Staff" || current_user.role == "Head" && current_user.staff.organization.country.present?
          @members = Member.all if current_user.role == 'Admin'
          @notice=  'Invalid.' 
          format.html { render :new}
          format.json { render json: @committee_member.errors, status: :unprocessable_entity }
        end
    else
      unless @committee_member.save
          puts "***************\n"
          puts @committee_member.errors.full_messages
          puts "***************\n"
          @organization = current_user.staff.organization
          ### staff can choose only the committee belongs to its organization while admin can choose all committees 
          ### staff can make only the members belongs to its organization to the committee member while admin can make any members to the committee member
          @committee_member = CommitteeMember.new
          @organizations = Organization.all if current_user.role == 'Admin'
          @organizations = Organization.where(id: @organization.id) if current_user.role == "Staff" || current_user.role == "Head"
          @committees = @organizations.first.committees if current_user.role == "Staff" || current_user.role == "Head"
          @committees = Committee.all.compact if current_user.role == "Admin"
          @members = @organizations.first.members if current_user.role == "Staff" || current_user.role == "Head"
          @members = Member.all if current_user.role == 'Admin'
          @notice=  'Invalid.' 
          format.html { render :new}
          format.json { render json: @committee_member.errors, status: :unprocessable_entity }
      end
    end
    end
  end

  # PATCH/PUT /committee_members/1
  # PATCH/PUT /committee_members/1.json
  def update
    respond_to do |format|
      if @committee_member.update(committee_member_params)
        organization = Organization.find(params[:committee_member][:organization_id])
        @committees = organization.committees
        @committee = @committees.find_by_name(Committee.find(params[:committee_member][:committee_id]).name)
        unless @committee.present?
          puts 'no committee present'
          @committee = Committee.create(name: Committee.find(params[:committee_member][:committee_id]).name, organization_id: params[:committee_member][:organization_id])
        end
        @committee_member.committee_id = @committee.id
        @committee_member.save
        format.html { redirect_to @committee_member, notice: 'Committee member was successfully updated.' }
        format.json { render :show, status: :ok, location: @committee_member }
      else
        ### staff can choose only the committee belongs to its organization while admin can choose all committees 
        ### staff can make only the members belongs to its organization to the committee member while admin can make any members to the committee member
        @organizations = Organization.all if current_user.role == 'Admin'
        @organizations = Organization.where(id: @organization.id) if current_user.role == "Staff" || current_user.role == "Head"
        @committees = Committee.all.compact
        @members = @organizations.first.members if current_user.role == "Staff" || current_user.role == "Head"
        @members = Member.all if current_user.role == 'Admin'

        format.html { render :edit }
        format.json { render json: @committee_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /committee_members/1
  # DELETE /committee_members/1.json
  def destroy
    @committee_member.destroy
    respond_to do |format|
      format.html { redirect_to committee_members_url, notice: 'Committee member was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def csv_export
    if current_user.role == "Staff" || current_user.role == "Head"
      @committee_members = CommitteeMember.where(organization_id: current_user.staff.organization.id)
    else
      @committee_members = CommitteeMember.all
    end

    # if params[:search_role].present?
    #   @committee_members = @committee_members.where('role = ?', params[:search_role])
    # end
    # if params[:search_year].present?
    #   @committee_members = @committee_members.where("year = ?", params[:search_year])
    # end
    # if params[:search_by].present?
    #     @committee_members = @committee_members.joins(:member)
    #     @committee_members = @committee_members.where('lower(name) LIKE ? ', "%#{params[:search_by].downcase}%")
    # end
  
    respond_to do |format|
      format.csv { send_data @committee_members.to_csv(@committee_members) } 
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_committee_member

      ### staff can manage only the committee members belongs to its organization while admin can manage all committee members
      ### staff can not edit or delete the committee members who belongs to other organization
      @committee_members = current_user.role == "Staff" || current_user.role == "Head" ? CommitteeMember.where(organization_id: current_user.staff.organization.id) :  CommitteeMember.all
      if @committee_members.ids.include? (params[:id].to_i)
        @committee_member = @committee_members.find(params[:id])
      else
        redirect_to committee_members_path, notice: 'Not Found.'
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def committee_member_params
      params.require(:committee_member).permit(:role, :organization_id, :committee_id, :member_id, :year)
    end
    
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
