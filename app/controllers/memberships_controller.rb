class MembershipsController < ApplicationController
  layout :custom_layout #'apply', only: [:upgrade, :upgraded]
  before_action :is_login?
  before_action :is_member?, only:  [:upgrade, :upgraded, :extend, :extended]
  before_action :is_staff?, except: [:upgrade, :upgraded, :extend, :extended]
  before_action :is_admin?, only: [:destroy]
  before_action :set_membership, only: [:show, :edit, :update, :destroy, :upgrade, :upgraded, :extend, :extended]

  # GET /memberships
  # GET /memberships.json
  def index
    if current_user.role == "Admin"
      @memberships = Membership.all
    elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
        
      @memberships = Membership.all
    else
      @memberships = current_user.staff.organization.memberships
    end
    if params[:organization_search].present?
      # Need to improve code
      # First I was trying to left-join three tables. 
      # Currently only two talbes (members and memberships) joined
      # To filter by organizations' name, I use params[:organization_search] with id of organization, not with the name of the organization in member index
      # @memberships = Organization.find(params[:organization_search]).members
      @memberships = @memberships.where('organization_id = ?', "#{params[:organization_search]}")
    end
    @memberships = @memberships.joins(:organization, :member).where("lower(organization_name) LIKE ? OR lower(name) LIKE ?", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%") if params[:search_by].present?
    if params[:search_grade].present?
      # @memberships = @memberships.where("grade = ?", "#{params[:search_grade]}") if params[:search_grade] == 
      # @memberships = @memberships.where("grade = ?", "#{params[:search_grade]}") if params[:search_grade].present?
      @memberships = @memberships.where("grade = ?", "#{params[:search_grade]}") if params[:search_grade].present?
    end
    @memberships = @memberships.order(:organization_id, :grade).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /memberships/1
  # GET /memberships/1.json
  def show
    @member = @membership.member
  end

  # GET /memberships/new
  def new
    @membership = Membership.new
    @members = []
    Member.all.each do |m|
      @members << m unless m.memberships.present?
    end
  end

  # GET /memberships/1/edit
  def edit
    @members  = Member.all
  end

  # POST /memberships
  # POST /memberships.json
  def create
    @membership = Membership.new(membership_params)

    respond_to do |format|
      if @membership.save
        format.html { redirect_to @membership, notice: 'Membership was successfully created.' }
        format.json { render :show, status: :created, location: @membership }
      else
        @members = []
        Member.all.each do |m|
          @members << m unless m.memberships.present?
        end
        format.html { render :new }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    @member = Member.find(@membership.member_id)
    @member.log = @member.log.present? ? @member.log : {}
    
    respond_to do |format|
        @member.log["Grade changed "] = {"#{DateTime.now}" => "State - #{@member.state}, Organization - #{@membership.organization.organization_name}, Grade - #{@membership.grade}, Valid-till - #{@membership.valid_till}"}
        @member.member_type = @membership.grade

          if @membership.update(membership_params) && @member.save
            format.html { redirect_to @membership, notice: 'Membership was successfully updated.' }
            format.json { render :show, status: :ok, location: @membership }
          else

            @members  = Member.all
            format.html { render :edit }
            format.json { render json: @membership.errors, status: :unprocessable_entity }
          end
    end
  end

  # DELETE /memberships/1
  # DELETE /memberships/1.json
  def destroy
    @membership.destroy
    respond_to do |format|
      format.html { redirect_to memberships_url, notice: 'Membership was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upgrade
    @price = 0
    @member = @membership.member
    # puts "************\n #{@member.inspect} *********\n"
    member_type = @membership.grade
    date = @membership.valid_till
    if date < DateTime.now 
      price = 2000 if member_type == 'SM' || member_type == 'Student Member'
      price = 4000 if member_type == 'AM' || member_type == 'Associate Member'
      price = 6000 if member_type == 'FM' || member_type == 'Fellow Member'
    elsif date > DateTime.now
      counter = (date.month - DateTime.now.month) > 0 ? 1 : 0 
      price = 2000 * (date.year - DateTime.now.year + counter) 
    else
      price = 0
    end

    # unless date.present?
    #   date = DateTime.now
    #   # redirect_to "/d/member_information?member_no=#{@member.member_no}", notice: "Membership date is invalid. Contact the Admin."
    # end
    if date.strftime("%Y").present? && date.strftime("%Y").to_i.present?
      year = date.strftime("%Y").to_i
      if member_type == 'SM' || member_type == 'Student Member'
        # puts 'sm'
          if year < 2004
            price = price + (2004 - year)*500
            year = 2004
          end
          if year > 2003 && year < 2008
            price = price + (2008 - year)* 1000
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            price = price + (DateTime.now.strftime("%Y").to_i - year)*2000
          end
          @debt = price
          @fee = 4000
          @price = price + 1000 + 4000
          @upgraded_from_to = "from Student Member to Associate Member "
      elsif member_type == 'AM' || member_type == 'Associate Member'
        # puts 'am'
          if year < 2004
            price = price + (2004 - year)*1000
            year = 2004
          end
          if year > 2003 && year < 2008
            price = price + (2008 - year)* 1500
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            price = price + (DateTime.now.strftime("%Y").to_i - year)*4000
            puts price
          end
          @debt = price
          @fee = 6000
          @price = price + 1000 + 6000
          @upgraded_from_to = "from Associate Member to Professional Member "
      else
        # puts 'other'
          if year < 2004
            price = price + (2004 - year)*1500
            year = 2004
          end
          if year > 2003 && year < 2008
            price = price + (2008 - year)* 2000
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            price = price + (DateTime.now.strftime("%Y").to_i - year)*6000
          end
          @debt = price
          @fee = 0
          @price = price + 1000
          @upgraded_from_to = "from Professional Member to Fellow Member "
      end
    end
  end

  def upgraded
    @member = @membership.member
    @request = Request.new
    @request.request_type = "Upgrade"
    @request.member_id = @member.id

    if @membership.grade == "SM" || @membership.grade == "Student Member"
      @request.upgraded_type = 'sm_to_am'
    elsif @membership.grade == "AM" || @membership.grade == "Associate Member"
      @request.upgraded_type = 'am_to_pm'
    elsif @membership.grade == "PM" || @membership.grade == "Professional Member"
      @request.upgraded_type = 'pm_to_fm'
    end
    # puts "**************\n #{@member.inspect} **********\n"
    @request.organization_id = @member.organizations.last.id
    if @request.upgraded_type.present? && @request.save
      SystemMailer.upgrade_membership_received(@member, @request).deliver_now
      redirect_to "/attachments/new", notice: "Membership upgrade request is successfully sent. Please upload the attachments."
      # redirect_to "/d/member_information?member_no=#{@member.member_no}", notice: "Membership upgrade request is successfully sent."
    else
      redirect_to "/d/member_information?member_no=#{@member.member_no}", notice: "Membership upgrade fails."
    end
  end

  def extend
    @member = @membership.member
    # puts "************\n #{@member.inspect} *********\n"
    member_type = @membership.grade
    date = @membership.valid_till
    if date < DateTime.now 
      price = 2000 if member_type == 'SM' || member_type == 'Student Member'
      price = 4000 if member_type == 'AM' || member_type == 'Associate Member'
      price = 6000 if member_type == 'FM' || member_type == 'Fellow Member'
    else
      price = 0
    end
    @price = 0
    @fee = 0
    unless date.present?
      date = DateTime.now
      # redirect_to "/d/member_information?member_no=#{@member.member_no}", notice: "Membership date is invalid. Contact the Admin."
    end
    if date.strftime("%Y").present? && date.strftime("%Y").to_i.present?
      year = date.strftime("%Y").to_i
      if member_type == 'SM' || member_type == 'Student Member'
        # puts 'SM'
          if year < 2004
            puts 'before 2004'
            price = price + (2004 - year)*500
            year = 2004
          end
          if year > 2003 && year < 2008
            puts 'between 2004 and 2007'
            price = price + (2008 - year)* 1000
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            puts 'after 2007'
            price = price + (DateTime.now.strftime("%Y").to_i - year)*2000
          end
          @fee = 2000
      elsif member_type == 'AM' || member_type == 'Associate Member'
        # puts 'AM'
          if year < 2004
            price = price + (2004 - year)*1000
            year = 2004
          end
          if year > 2003 && year < 2008
            price = price + (2008 - year)* 1500
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            price = price + (DateTime.now.strftime("%Y").to_i - year)*4000
          end
          @fee = 4000
      else
        # puts 'PM'
          if year < 2004
            price = price + (2004 - year)*1500
            year = 2004
          end
          if year > 2003 && year < 2008
            price = price + (2008 - year)* 2000
            year = 2008
          end
          if year > 2007 && year < DateTime.now.strftime("%Y").to_i
            price = price + (DateTime.now.strftime("%Y").to_i - year)*6000
          end
          @fee = 6000
      end
          @debt = price
          @price = price
    end
  end

  def extended
    @member = Member.find_by_member_no(params["member_no"])
    # puts "************\n #{@member.inspect} *********\n"
    @request = Request.new

    @request.member_id = @member.id
    @request.extended_month = params[:year_to_extend]
    @request.request_type = "Extend"
    @request.organization_id = @member.organizations.last.id

    if @request.save
      SystemMailer.extend_membership_received(@member, @request).deliver_now
        redirect_to "/attachments/new", notice: "Membership extend request is successfully sent. Please upload the attachments."
      # redirect_to "/d/member_information?member_no=#{@member.member_no}", notice: "Membership extend request is successfully sent."
    else
      render 'extend'
    end
  end

  protected
  def custom_layout
    if action_name == 'extend' || action_name == 'upgrade'
      'apply'
    else
      'staff_layout'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_membership
      if current_user.role == "Member"
        @membership = current_user.member.memberships.first
      else
          if current_user.role == "Admin"
            @memberships = Membership.all
          elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
              
            @memberships = Membership.all
          else
            @memberships = current_user.staff.organization.memberships
          end
          if @memberships.ids.include? (params[:id].to_i)
              @membership = Membership.find(params[:id])
          else
              redirect_to memberships_path, notice: 'Not Found.'
          end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membership_params
      params.require(:membership).permit(:member_id, :organization_id, :valid_till, :grade)
    end

    def is_login?
      unless user_signed_in?
        redirect_to new_user_session_path
      end
    end
    # Only staff can access this model
    def is_member?
      unless current_user.role == "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end

    # Only staff can access this model
    def is_staff?
      unless current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end

end
