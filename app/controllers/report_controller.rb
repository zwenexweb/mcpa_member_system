class ReportController < ApplicationController
	before_action :is_login?
	before_action :is_staff_or_admin?
	def index
		if current_user.role == 'Admin' || current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"

			@member_count = {}
			@member_count['Student Member'] = Membership.where(grade:'Student Member').count
			@member_count['Associate Member'] = Membership.where(grade:'Associate Member').count
			@member_count['Professional Member'] = Membership.where(grade:'Professional Member').count
			@member_count['Fellow Member'] = Membership.where(grade:'Fellow Member').count

			# @member_organization = {}
			@member_list = {}
			@new_members = {}
			mem = {}

			State.all.each do |state|
				# state  = State.first
				if state.organization.present?
					members = state.organization.members

					@member_list["#{state.name}"] = {}
					mem = {}
					mem["SM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Student Member", "SM"]).count || 0
					mem["AM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Associate Member", "AM"]).count || 0
					mem["PM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Professional Member", "PM"]).count || 0
					mem["FM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Fellow Member", "FM"]).count || 0
					@member_list["#{state.name}"] = mem


					@new_members["#{state.name}"] = {}
					new_mem = {}

				new_mem["SM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", DateTime.now, DateTime.now - 9.months, 'Student Member').count || 0
				new_mem["AM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", DateTime.now, DateTime.now - 9.months, 'Associate Member').count || 0
				new_mem["PM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", DateTime.now, DateTime.now - 9.months, 'Professional Member').count || 0
				new_mem["FM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", DateTime.now, DateTime.now - 9.months, 'Fellow Member').count || 0

					# @member_organization["#{state.name}"] = members.count + state.organization.ecmembers.count
					@new_members["#{state.name}"] = new_mem
				end
			end

			@membership_applications_count = MembershipApplication.where("member_id IS NOT null").count
			@extend_membership_request_count = Request.where("request_type = 'Extend'").count
			@upgrade_membership_request_count = Request.where("request_type = 'Upgrade'").count		

		else
			org = current_user.staff.organization
			@member_count = {}
			@member_count['Student Member'] = Membership.where(organization_id: org.id, grade:'Student Member').count
			@member_count['Associate Member'] = Membership.where(organization_id: org.id, grade:'Associate Member').count
			@member_count['Professional Member'] = Membership.where(organization_id: org.id, grade:'Professional Member').count
			@member_count['Fellow Member'] = Membership.where(organization_id: org.id, grade:'Fellow Member').count

			@member_organization = {}
			@member_list = {}
			@new_members = {}
			@upgraded_members = {}
			@extended_members = {}

			count = 11
			members = org.members
			memberships = org.memberships

			12.times do |m|
				month = DateTime.now - m.months
				check_date = DateTime.now - m.months

				month_name = Date::MONTHNAMES[month.month]
				@new_members["#{month_name}"] = {}
				new_mem = {}

				@upgraded_members["#{month_name}"] = {}
				upgraded_mem = {}

				@extended_members["#{month_name}"] = {}
				extended_mem = {}

				# puts "counter = #{m}"
				# puts "from month - #{check_date}"
				# puts "to month - #{DateTime.now - (m+1).months}"
				# puts DateTime.now.month
				# puts month_name

				####### need to fix
					# for new members 
					new_mem["SM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Student Member').count || 0
					new_mem["AM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
					new_mem["PM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
					new_mem["FM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0


					# for upgrade memberships
					upgraded_mem["sm_to_am"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
					upgraded_mem["am_to_pm"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
					upgraded_mem["pm_to_fm"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0


					# for extend memberships
					extended_mem["SM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Student Member').count || 0
					extended_mem["AM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
					extended_mem["PM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
					extended_mem["FM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0
				####### need to fix

				@member_organization["#{month_name}"] = members.count + org.ecmembers.count
				@new_members["#{month_name}"] = new_mem
				@upgraded_members["#{month_name}"] = upgraded_mem
				@extended_members["#{month_name}"] = extended_mem
				count -= 1

			end

			@member_list["#{org.organization_name}"] = {}
			mem = {}
			mem["SM"] = memberships.where(grade: ["Student Member", "SM"]).count || 0
			mem["AM"] = memberships.where(grade: ["Associate Member", "AM"]).count || 0
			mem["PM"] = memberships.where(grade: ["Professional Member", "PM"]).count || 0
			mem["FM"] = memberships.where(grade: ["Fellow Member", "FM"]).count || 0
			@member_list["#{org.organization_name}"] = mem
	    end
	    respond_to do |format|
		    format.html
		    format.xlsx{
			    response.headers['Content-Disposition'] = 'attachment; filename="reports_for_all_organizations.xlsx"'
			}
		end
	end

	def organization_lvl_report
		if Organization.find_by_id(params[:organization_id].to_i).present?
			organization = Organization.find(params[:organization_id].to_i)
		else
			organization = current_user.staff.organization
		end

		@organization = organization
		sm = Membership.where(organization_id: organization.id, grade:'Student Member').count
		am = Membership.where(organization_id: organization.id, grade:'Associate Member').count
		pm = Membership.where(organization_id: organization.id, grade:'Professional Member').count
		fm = Membership.where(organization_id: organization.id, grade:'Fellow Member').count

		@member_count = {}
		@member_count['Student Member'] = sm
		@member_count['Associate Member'] = am
		@member_count['Professional Member'] = pm
		@member_count['Fellow Member'] = fm

		@member_organization = {}
		@member_list = {}
		@new_members = {}
		@upgraded_members = {}
		@extended_members = {}

		count = 11
		members = organization.members
		memberships = organization.memberships

		12.times do |m|
			month = DateTime.now - m.months
			check_date = DateTime.now - m.months

			month_name = Date::MONTHNAMES[month.month]
			@new_members["#{month_name}"] = {}
			new_mem = {}

			@upgraded_members["#{month_name}"] = {}
			upgraded_mem = {}

			@extended_members["#{month_name}"] = {}
			extended_mem = {}

			# puts "counter = #{m}"
			# puts "from month - #{check_date}"
			# puts "to month - #{DateTime.now - (m+1).months}"
			# puts DateTime.now.month
			# puts month_name

			####### need to fix
				# for new members 
				new_mem["SM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Student Member').count || 0
				new_mem["AM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
				new_mem["PM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
				new_mem["FM"] = members.where("members.created_at <= ? AND members.created_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0

				# for upgrade memberships
				upgraded_mem["sm_to_am"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
				upgraded_mem["am_to_pm"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
				upgraded_mem["pm_to_fm"] = memberships.where("upgraded_at <= ? AND upgraded_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0


				# for extend memberships
				extended_mem["SM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Student Member').count || 0
				extended_mem["AM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Associate Member').count || 0
				extended_mem["PM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Professional Member').count || 0
				extended_mem["FM"] = memberships.where("memberships.extended_at <= ? AND memberships.extended_at >= ? AND grade = ?", month, DateTime.now - (m + 1).months, 'Fellow Member').count || 0
			####### need to fix

			@member_organization["#{month_name}"] = members.count + organization.ecmembers.count
			@new_members["#{month_name}"] = new_mem
			@upgraded_members["#{month_name}"] = upgraded_mem
			@extended_members["#{month_name}"] = extended_mem
			count -= 1


			@member_list["#{organization.organization_name}"] = {}
			mem = {}
			members = organization.members
			mem["SM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Student Member", "SM"]).count || 0
			mem["AM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Associate Member", "AM"]).count || 0
			mem["PM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Professional Member", "PM"]).count || 0
			mem["FM"] = members.joins(:memberships).where("memberships.grade IN (?)", ["Fellow Member", "FM"]).count || 0
			@member_list["#{organization.organization_name}"] = mem
		end

	    respond_to do |format|
		    format.html
		    format.xlsx{
			    response.headers['Content-Disposition'] = "attachment; filename='reports_for_#{@organization.short_name}.xlsx'"
			}
		end
	end

	private
	def is_login?
		unless user_signed_in?
			redirect_to new_user_session_path
		end
	end
	def is_staff_or_admin?
		if current_user.role == "Member"
			redirect_to root_path
		end
	end
end
