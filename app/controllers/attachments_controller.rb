class AttachmentsController < ApplicationController
  layout :custom_layout#"apply", only: [:new, :create]
  before_action :is_login?, except: [:show, :new, :create]
  before_action :is_admin?, only: [:destroy]
  before_action :set_attachment, only: [:show, :edit, :update, :destroy]

  # GET /attachments
  # GET /attachments.json
  def index
    if user_signed_in? && current_user.role != 'Member'

      @attachments = Attachment.all
      @attachments = @attachments.joins(:membership_application).where("lower(name) LIKE ?", "%#{params[:search_by].downcase}%") if params[:search_by].present?
        @attachments = @attachments.where("attachment_type = ? ", params[:search_attachment_type]) if params[:search_attachment_type].present?
    else
      @membership_application = current_user.member.membership_application
      @attachments = @membership_application.present? ? @membership_application.attachments : Attachment.where("id = 0")
    end

    @attachments = @attachments.paginate(page: params[:page], per_page: 20)
  end

  # GET /attachments/1
  # GET /attachments/1.json
  def show
    unless user_signed_in?
      @ref_no = @attachment.membership_application.ref_no
    end
  end

  # GET /attachments/new
  def new
    @attachment = Attachment.new
    if user_signed_in? && current_user.role == "Member"
      @member = current_user.member
      @membership = @member.memberships.last
      if @member.membership_application.present?
        @attachment.membership_application_id = @member.membership_application.id 
      else
        membership_application = MembershipApplication.new(name: @member.name, nrc: @member.nrc, email: @member.email, phone: @member.phone, address: @member.address, state: @member.state, township: @member.township, member_type: @membership.grade)
        membership_application.save(validate: false)
        @member.update(membership_application_id: membership_application.id)
        @attachment.membership_application_id = membership_application.id
      end
    else
      @attachment.membership_application_id = MembershipApplication.find_by_ref_no(params[:membership_application_ref_no]).id if params[:membership_application_ref_no].present? 
      # @member = Member.find_by_member_no(params[:membership_application_ref_no]) if params[:membership_application_ref_no].present?
    end
  end

  # GET /attachments/1/edit
  def edit
  end

  # POST /attachments
  # POST /attachments.json
  def create
    @attachment = Attachment.new(attachment_params)
    @attachment.membership_application_id = MembershipApplication.find_by_name(params[:attachment][:membership_application_id]).id unless user_signed_in? && current_user.role != "Member"

    respond_to do |format|
      if @attachment.save
        format.html { redirect_to "/attachments/#{@attachment.id}?membership_application_ref_no=#{@attachment.membership_application.ref_no}", notice: 'Attachment was successfully created.' }
        format.json { render :show, status: :created, location: @attachment }
      else
        format.html { render :new }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attachments/1
  # PATCH/PUT /attachments/1.json
  def update
    @attachment.membership_application_id = MembershipApplication.find_by_name(params[:attachment][:membership_application_id]).id if current_user.role == 'Member'
    respond_to do |format|
      if @attachment.update(attachment_params)
        format.html { redirect_to @attachment, notice: 'Attachment was successfully updated.' }
        format.json { render :show, status: :ok, location: @attachment }
      else
        format.html { render :edit }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attachments/1
  # DELETE /attachments/1.json
  def destroy
    @attachment.destroy
    respond_to do |format|
      format.html { redirect_to attachments_url, notice: 'Attachment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected
  def custom_layout
    if !user_signed_in? || (user_signed_in? && current_user.role == 'Member' && action_name == 'new')
      "apply"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attachment
      if user_signed_in?
        if current_user.role == 'Member'
            ### Member can see his own attachments
            attachments = current_user.member.membership_application.attachments
            if attachments.ids.include? (params[:id].to_i)
              @attachment = Attachment.find(params[:id])
            else
              redirect_to '/d/attachments', notice: "Not Found"
            end
        elsif current_user.role == "Staff"
            ### Staff can see the attachments of the members who belongs to corresponding organizations
            @attachment = Attachment.find(params[:id])
        else
            ### Admin can see all the attachments
            @attachment = Attachment.find(params[:id])
        end
      else
        @attachment = Attachment.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attachment_params
      params.require(:attachment).permit(:membership_application_id, :file_attachment, :attachment_type)
    end

    # Check login
    def is_login?
      unless user_signed_in?
        redirect_to new_user_session_path
      else
        if current_user.role == 'Member'
          redirect_to '/d/attachments'
        end
      end
    end

    # Only staff can access this model
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
