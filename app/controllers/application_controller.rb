class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout_by_resource
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:role, :member_id, :staff_id])
    devise_parameter_sanitizer.permit(:account_update, keys: [:role, :member_id, :staff_id, :profile])
  end


  private

  def layout_by_resource
    if user_signed_in? && current_user.role == "Member"
      "member_layout"
    else
      "staff_layout"
    end
  end
end
