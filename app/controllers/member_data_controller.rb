class MemberDataController < ApplicationController
  before_action :is_staff?
  before_action :is_admin?, only: [:destroy]
  before_action :set_member_datum, only: [:show, :edit, :update, :destroy]

  # GET /member_data
  # GET /member_data.json
  def index
    @member_data = MemberDatum.order(:id).paginate(page: params[:page], per_page: 20)
  end

  # GET /member_data/1
  # GET /member_data/1.json
  def show
  end

  # GET /member_data/new
  def new
    @member_datum = MemberDatum.new
  end

  # GET /member_data/1/edit
  def edit
  end

  # POST /member_data
  # POST /member_data.json
  def create
    @member_datum = MemberDatum.new(member_datum_params)
    @member_datum.content_catagory = params[:content_catagory]

    respond_to do |format|
      if @member_datum.save
        format.html { redirect_to @member_datum, notice: 'Member datum was successfully created.' }
        format.json { render :show, status: :created, location: @member_datum }
      else
        format.html { render :new }
        format.json { render json: @member_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /member_data/1
  # PATCH/PUT /member_data/1.json
  def update
    @member_datum.content_catagory = params[:content_catagory]
    respond_to do |format|
      if @member_datum.update(member_datum_params)
        format.html { redirect_to @member_datum, notice: 'Member datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @member_datum }
      else
        format.html { render :edit }
        format.json { render json: @member_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /member_data/1
  # DELETE /member_data/1.json
  def destroy
    @member_datum.destroy
    respond_to do |format|
      format.html { redirect_to member_data_url, notice: 'Member datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member_datum
      @member_datum = MemberDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_datum_params
      params.require(:member_datum).permit(:date, :content_catagory, :title, :description, :member_id)
    end

    # Only staff can access this model
    def is_staff?
        unless user_signed_in? && current_user.role != "Member"
            redirect_to root_path, notice: 'Access Denied'
        end
    end
    def is_admin?
        unless user_signed_in? && current_user.role == "Admin"
            redirect_to root_path, notice: 'Access Denied'
        end
    end
end
