class AnnouncementsController < ApplicationController
  # before_action :authenticate_user!
  before_action :is_staff?#, except: [:index, :show]
  before_action :is_admin?, only: [:destroy]
  before_action :set_announcement, only: [:show, :edit, :update, :destroy]

  # GET /announcements
  # GET /announcements.json
  def index
    if current_user.role == "Admin"
      @announcements = Announcement.all.order(date: :DESC)
    else
      @announcements = Announcement.where(organization_id: [nil, current_user.staff.organization.id])
    end
  end

  # GET /announcements/1
  # GET /announcements/1.json
  def show
    @text = "show"
  end

  # GET /announcements/new
  def new
    @announcement = Announcement.new
    if current_user.role == 'Admin' || current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
      @organizations = Organization.all.collect {|o| [o.organization_name, o.id]}
    else
      @organizations = Organization.where(id: current_user.staff.organization.id).collect {|o| [o.organization_name, o.id]}
    end

  end

  # GET /announcements/1/edit
  def edit
    if current_user.role == 'Admin' || current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
      @organizations = Organization.all.collect {|o| [o.organization_name, o.id]}
    else
      @organizations = Organization.where(id: current_user.staff.organization.id).collect {|o| [o.organization_name, o.id]}
    end
  end

  # POST /announcements
  # POST /announcements.json
  def create
    @announcement = Announcement.new(announcement_params)

    respond_to do |format|
      if @announcement.save
        # @notice = "Announcement was successfully created."
        format.html { redirect_to @announcement, notice: 'Announcement was successfully created.' }
        format.json { render :show, status: :created, location: @announcement }
      else
        format.html { render :new }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /announcements/1
  # PATCH/PUT /announcements/1.json
  def update
    respond_to do |format|
      if @announcement.update(announcement_params)
        format.html { redirect_to @announcement, notice: 'Announcement was successfully updated.' }
        format.json { render :show, status: :ok, location: @announcement }
      else
        if current_user.role == 'Admin' || current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
          @organizations = Organization.all.collect {|o| [o.organization_name, o.id]}
        else
          @organizations = Organization.where(id: current_user.staff.organization.id).collect {|o| [o.organization_name, o.id]}
        end
        format.html { render :edit }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /announcements/1
  # DELETE /announcements/1.json
  def destroy
    @announcement.destroy
    respond_to do |format|
      format.html { redirect_to announcements_url, notice: 'Announcement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_announcement
      @announcements = current_user.role == 'Staff' ?  Announcement.where(organization_id: [nil, current_user.staff.organization.id]) : Announcement.all
      if @announcements.ids.include? (params[:id].to_i)
        @announcement = Announcement.find(params[:id])
      else
        redirect_to announcements_path, notice: 'Not Found.'
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def announcement_params
      params.require(:announcement).permit(:title, :date, :content, :organization_id, :member_type, :committee_id)
    end

    def is_staff?
      unless current_user.role != 'Member'
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
