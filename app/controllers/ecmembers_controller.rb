class EcmembersController < ApplicationController
  # before_action :authenticate_user!
  before_action :is_staff?#, only: [:new, :create, :edit, :update, :destroy]
  before_action :is_admin?, only: [:destroy]
  before_action :set_ecmember, only: [:show, :edit, :update, :destroy]

  # GET /ecmembers
  # GET /ecmembers.json
  def index
    if current_user.role == "Admin" 
      @ecmembers = Ecmember.all
    elsif current_user.role == "Staff" || current_user.role == "Head"
      @ecmembers = current_user.staff.organization.ecmembers
    else
      @ecmembers = current_user.member.organizations.first.ecmembers
    end
    if params[:search_role].present?
      @ecmembers = @ecmembers.where('role = ?', params[:search_role])
    end
    if params[:search_year].present?
      @ecmembers = @ecmembers.where("year = ?", params[:search_year])
    end
    if params[:search_by].present?
      @ecmembers = @ecmembers.joins(:member)


        @ecmembers = @ecmembers.joins(:organization)
        @ecmembers = @ecmembers.where('lower(name) LIKE ? OR lower(organization_name) LIKE ? OR lower(nrc) LIKE ? OR member_no = ?', "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", params[:search_by].to_i)
      
    end
    @ecmembers = @ecmembers.order(:year, :organization_id).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /ecmembers/1
  # GET /ecmembers/1.json
  def show
  end

  # GET /ecmembers/new
  def new
    @ecmember = Ecmember.new
    @organization = current_user.staff.organization
    @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff" || current_user.role == "Head"
    @organizations = Organization.all if current_user.role == "Admin"
    @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
    @members = Member.where.not(is_world_member: nil) if current_user.role == 'Staff' && current_user.staff.organization.country.present?
    @members = Member.all if current_user.role == 'Admin'
  end

  # GET /ecmembers/1/edit
  def edit
    @organization = current_user.staff.organization
    @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff" || current_user.role == "Head"
    @organizations = Organization.all if current_user.role == "Admin"
    @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
    @members = Member.where.not(is_world_member: nil) if current_user.role == "Staff" || current_user.role == "Head" && current_user.staff.organization.country.present?
    @members = Member.all if current_user.role == 'Admin'
  end

  # POST /ecmembers
  # POST /ecmembers.json
  def create
    @ecmember = Ecmember.new(ecmember_params)
    puts params[:ecmember][:year]
    puts @ecmember.year
    # @ecmember.year = params[:date][:year]
    respond_to do |format|
      if @ecmember.save
        format.html { redirect_to @ecmember, notice: 'Ecmember was successfully created.' }
        format.json { render :show, status: :created, location: @ecmember }
      else
        @organization = current_user.staff.organization
        @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff"
        @organizations = Organization.all if current_user.role == "Admin"
        @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
        @members = Member.where.not(is_world_member: nil) if current_user.role == "Staff" || current_user.role == "Head" && current_user.staff.organization.country.present?
        @members = Member.all if current_user.role == 'Admin'
        format.html { render :new }
        format.json { render json: @ecmember.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ecmembers/1
  # PATCH/PUT /ecmembers/1.json
  def update
    # @ecmember.year = params[:date][:year]
    respond_to do |format|
      if @ecmember.update(ecmember_params)
        format.html { redirect_to @ecmember, notice: 'Ecmember was successfully updated.' }
        format.json { render :show, status: :ok, location: @ecmember }
      else
        @organization = current_user.staff.organization
        @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff"
        @organizations = Organization.all if current_user.role == "Admin"
        @members = @organization.members if current_user.role == "Staff" || current_user.role == "Head"
        @members = Member.where.not(is_world_member: nil) if current_user.role == "Staff" || current_user.role == "Head" && current_user.staff.organization.country.present?
        @members = Member.all if current_user.role == 'Admin'
        format.html { render :edit }
        format.json { render json: @ecmember.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ecmembers/1
  # DELETE /ecmembers/1.json
  def destroy
    @ecmember.destroy
    respond_to do |format|
      format.html { redirect_to ecmembers_url, notice: 'Ecmember was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def member_search
    organization = Organization.find(params[:organization_id])
    if organization.country.present?
      @members = Member.where.not(is_world_member: nil)
    else
      @members = organization.members
    end
    respond_to do |f|
      f.js
    end
  end

  def csv_export
    if current_user.role == "Admin" 
      @ecmembers = Ecmember.all
    elsif current_user.role == "Staff" || current_user.role == "Head"
      @ecmembers = current_user.staff.organization.ecmembers
    else
      @ecmembers = current_user.member.organizations.first.ecmembers
    end
    
    respond_to do |format|
      format.csv { send_data @ecmembers.to_csv(@ecmembers) } 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ecmember
      @ecmembers = current_user.role == "Staff" || current_user.role == "Head" ? Ecmember.where(organization_id: current_user.staff.organization.id) :  Ecmember.all
      if @ecmembers.ids.include? (params[:id].to_i)
        @ecmember = @ecmembers.find(params[:id])
      else
        redirect_to ecmembers_path, notice: 'Not Found.'
      end

      @ecmember = Ecmember.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ecmember_params
      params.require(:ecmember).permit(:organization_id, :member_id, :role, :year)
    end

    # Only staff can CRU this model
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end

     # Only Admin can delete
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
