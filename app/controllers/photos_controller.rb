class PhotosController < ApplicationController
  layout :custom_layout#"apply", only: [:new, :create]
  before_action :is_login?, except: [:show, :new, :create]
  before_action :is_admin?, only: [:destroy]
  before_action :set_photo, only: [:show, :edit, :update, :destroy]

  # GET /photos
  # GET /photos.json
  def index
    @photos = Photo.all
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
  end

  # GET /photos/new
  def new
    unless params[:membership_application_ref_no].present?
      redirect_to root_path
    end
    @photo = Photo.new
    @photo.membership_application_id = MembershipApplication.find_by_ref_no(params[:membership_application_ref_no]).id if params[:membership_application_ref_no].present? && MembershipApplication.find_by_ref_no(params[:membership_application_ref_no]).present?
  end

  # GET /photos/1/edit
  def edit
  end

  # POST /photos
  # POST /photos.json
  def create
    @photo = Photo.new(photo_params)
    puts "-------------------"
    puts @photo.inspect
    @photo.membership_application_id = MembershipApplication.find_by_name(params[:photo][:membership_application_id]).id

    respond_to do |format|
      if @photo.save
        format.html { redirect_to "/degrees/new?membership_application_ref_no=#{@photo.membership_application.ref_no}", notice: 'Photo was successfully created.' }
        # format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
        format.json { render :show, status: :created, location: @photo }
      else
        format.html { render :new }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photos_url, notice: 'Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected
  def custom_layout
    if !user_signed_in? || (user_signed_in? && current_user.role == 'Member' && action_name == 'new')
      "apply"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:avatar, :membership_application_id)
    end

    # Check login
    def is_login?
      unless user_signed_in?
        redirect_to new_user_session_path
      else
        if current_user.role == 'Member'
          redirect_to '/d/attachments'
        end
      end
    end

    # Only staff can access this model
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end

end
