class DegreesController < ApplicationController
  layout :custom_layout#"apply", only: [:new, :create]
  before_action :is_login?, except: [:show, :new, :create]
  before_action :is_admin?, only: [:destroy]
  before_action :set_degree, only: [:show, :edit, :update, :destroy]

  # GET /degrees
  # GET /degrees.json
  def index
    @degrees = Degree.all
  end

  # GET /degrees/1
  # GET /degrees/1.json
  def show
  end

  # GET /degrees/new
  def new
    unless params[:membership_application_ref_no].present?
      redirect_to root_path
    end
    @degree = Degree.new
    @degree.membership_application_id = MembershipApplication.find_by_ref_no(params[:membership_application_ref_no]).id if params[:membership_application_ref_no].present? && MembershipApplication.find_by_ref_no(params[:membership_application_ref_no]).present?
  end

  # GET /degrees/1/edit
  def edit
  end

  # POST /degrees
  # POST /degrees.json
  def create
    @degree = Degree.new(degree_params)
    @degree.membership_application_id = MembershipApplication.find_by_name(params[:degree][:membership_application_id]).id

    respond_to do |format|
      if @degree.save
        format.html { redirect_to "/attachments/new?membership_application_ref_no=#{@degree.membership_application.ref_no}", notice: 'Photo was successfully created.' }
        format.html { redirect_to @degree, notice: 'Degree was successfully created.' }
        format.json { render :show, status: :created, location: @degree }
      else
        format.html { render :new }
        format.json { render json: @degree.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /degrees/1
  # PATCH/PUT /degrees/1.json
  def update
    respond_to do |format|
      if @degree.update(degree_params)
        format.html { redirect_to @degree, notice: 'Degree was successfully updated.' }
        format.json { render :show, status: :ok, location: @degree }
      else
        format.html { render :edit }
        format.json { render json: @degree.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /degrees/1
  # DELETE /degrees/1.json
  def destroy
    @degree.destroy
    respond_to do |format|
      format.html { redirect_to degrees_url, notice: 'Degree was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected
  def custom_layout
    if !user_signed_in? || (user_signed_in? && current_user.role == 'Member' && action_name == 'new')
      "apply"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_degree
      @degree = Degree.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def degree_params
      params.require(:degree).permit(:degree_title, :membership_application_id, :degree_file)
    end

    # Check login
    def is_login?
      unless user_signed_in?
        redirect_to new_user_session_path
      else
        if current_user.role == 'Member'
          redirect_to '/d/attachments'
        end
      end
    end

    # Only staff can access this model
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
