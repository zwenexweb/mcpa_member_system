class MembershipApplicationsController < ApplicationController
    layout :custom_layout #"apply", only: [:new, :create, :successful]
    before_action :is_member_login?
    before_action :is_staff?, except: [:new, :create, :successful, :state_search, :done]
    before_action :set_membership_application, only: [:show, :edit, :update, :destroy, :accept, :reject]

    # GET /membership_applications
    # GET /membership_applications.json
    def index
        if current_user.role == "Admin"
            @membership_applications = MembershipApplication.all
        elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
            @membership_applications = MembershipApplication.all
        else
            @membership_applications = MembershipApplication.where("state LIKE ?", current_user.staff.organization.state.name)
        end

        date_search = Date.parse(params[:date_search]) unless params[:date_search].nil? || params[:date_search].blank?
        
        if params[:state_search].present?
            state_name = State.find(params[:state_search]).name
          @membership_applications = MembershipApplication.where(state: state_name)
        end

        if params[:search_by].present?
          @membership_applications = @membership_applications.where('lower(name) LIKE ? OR lower(phone) LIKE ? OR lower(email) LIKE ? OR lower(nrc) LIKE ?', "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%") if params[:search_by].to_i == 0
        end
        if params[:date_search].present? || params[:grade_search].present? || params[:organization_search].present?
            if params[:date_search].present?
              @membership_applications = @membership_applications.where('DATE(created_at) <= ?', date_search)
            end
            if params[:grade_search].present?
              # params[:grade_search] is the value of the grade in the membership, not the id of the membership
              @membership_applications = @membership_applications.where('member_type = ?', "#{params[:grade_search]}")
            end
        end
        @membership_applications = @membership_applications.order("created_at DESC").paginate(:page => params[:page], :per_page => 20)
    end

    # GET /membership_applications/1
    # GET /membership_applications/1.json
    def show
    end

    # GET /membership_applications/new
    def new
        @membership_application = MembershipApplication.new
        @townships = Township.all
    end

    # GET /membership_applications/1/edit
    def edit
        @townships = Township.all
    end

    # POST /membership_applications
    # POST /membership_applications.json
    def create
        respond_to do |format|
            @membership_application = MembershipApplication.new(membership_application_params)
            if  User.find_by_email(@membership_application.email).present?
                @error = "Email already occupied."
                format.html { render :new}
                format.json { render json: @membership_application.errors, status: :unprocessable_entity }
            else
                highest_education = ""
                certificates = {}
                areas_of_training = []
                efficiencies = []
                current_employment = {}
                employment_history = {}
                areas_of_professional_working_experiences = []

                # For highest_education
                highest_education = params[:highest_education] if params[:highest_education].present?
                # For highest_education

                # For Certificates
                    if params[:education1].present? && params[:date1].present? && params[:institution1].present?
                        certificates[:certificate1] = { :education => "#{params[:education1]}", :date =>  "#{params[:date1]}", :institution => "#{params[:institution1]}"}

                        if params[:education2].present? && params[:date2].present? && params[:institution2].present?
                            certificates[:certificate2] = { :education => "#{params[:education2]}", :date =>  "#{params[:date2]}", :institution => "#{params[:institution2]}"}
                        end
                        if params[:education3].present? && params[:date3].present? && params[:institution3].present?
                            certificates[:certificate3] = { :education => "#{params[:education3]}", :date =>  "#{params[:date3]}", :institution => "#{params[:institution3]}"}
                        end
                        if params[:education4].present? && params[:date4].present? && params[:institution4].present?
                            certificates[:certificate4] = { :education => "#{params[:education4]}", :date =>  "#{params[:date4]}", :institution => "#{params[:institution4]}"}
                        end
                        if params[:education5].present? && params[:date5].present? && params[:institution5].present?
                            certificates[:certificate5] = { :education => "#{params[:education5]}", :date =>  "#{params[:date5]}", :institution => "#{params[:institution5]}"}
                        end
                    end
                # For Certificates

                # For areas_of_training
                    areas_of_training[0] = params[:training_programming] unless params[:training_programming].blank? || params[:training_programming].nil?
                    areas_of_training[1] = params[:training_software_engineering] unless params[:training_software_engineering].blank? || params[:training_software_engineering].nil?
                    areas_of_training[2] = params[:training_database] unless params[:training_database].blank? || params[:training_database].nil?
                    areas_of_training[3] = params[:training_networking] unless params[:training_networking].blank? || params[:training_networking].nil?
                    areas_of_training[4] = params[:training_web_development] unless params[:training_web_development].blank? || params[:training_web_development].nil?
                    areas_of_training[5] = params[:training_operation_system] unless params[:training_operation_system].blank? || params[:training_operation_system].nil?
                    areas_of_training[6] = params[:training_graphic_design] unless params[:training_graphic_design].blank? || params[:training_graphic_design].nil?
                    areas_of_training[7] = params[:training_multimedia] unless params[:training_multimedia].blank? || params[:training_multimedia].nil?
                    areas_of_training[8] = params[:training_hardware] unless params[:training_hardware].blank? || params[:training_hardware].nil?
                    areas_of_training[9] = params[:training_mis] unless params[:training_mis].blank? || params[:training_mis].nil?
                # For areas_of_training

                # For efficiencies
                    efficiencies[0] = params[:efficiencies_pascal] unless params[:efficiencies_pascal].blank? || params[:efficiencies_pascal].nil? 
                    efficiencies[1] = params[:efficiencies_cobol] unless params[:efficiencies_cobol].blank? || params[:efficiencies_cobol].nil? 
                    efficiencies[2] = params[:efficiencies_assembly] unless params[:efficiencies_assembly].blank? || params[:efficiencies_assembly].nil? 
                    efficiencies[3] = params[:efficiencies_c_c_plus_plus] unless params[:efficiencies_c_c_plus_plus].blank? || params[:efficiencies_c_c_plus_plus].nil? 
                    efficiencies[4] = params[:efficiencies_basic] unless params[:efficiencies_basic].blank? || params[:efficiencies_basic].nil? 
                    efficiencies[5] = params[:efficiencies_visual_basic] unless params[:efficiencies_visual_basic].blank? || params[:efficiencies_visual_basic].nil? 
                    efficiencies[6] = params[:efficiencies_visual_c] unless params[:efficiencies_visual_c].blank? || params[:efficiencies_visual_c].nil? 
                    efficiencies[7] = params[:efficiencies_java] unless params[:efficiencies_java].blank? || params[:efficiencies_java].nil? 
                    efficiencies[8] = params[:efficiencies_net] unless params[:efficiencies_net].blank? || params[:efficiencies_net].nil? 
                    efficiencies[9] = params[:efficiencies_asp] unless params[:efficiencies_asp].blank? || params[:efficiencies_asp].nil? 
                    efficiencies[10] = params[:efficiencies_perl] unless params[:efficiencies_perl].blank? || params[:efficiencies_perl].nil? 
                    efficiencies[11] = params[:efficiencies_php] unless params[:efficiencies_php].blank? || params[:efficiencies_php].nil? 
                    efficiencies[12] = params[:efficiencies_jsp] unless params[:efficiencies_jsp].blank? || params[:efficiencies_jsp].nil? 
                    efficiencies[13] = params[:efficiencies_cold_fusion] unless params[:efficiencies_cold_fusion].blank? || params[:efficiencies_cold_fusion].nil? 
                    efficiencies[14] = params[:efficiencies_html] unless params[:efficiencies_html].blank? || params[:efficiencies_html].nil? 
                    efficiencies[15] = params[:efficiencies_linux] unless params[:efficiencies_linux].blank? || params[:efficiencies_linux].nil? 
                    efficiencies[16] = params[:efficiencies_windows] unless params[:efficiencies_windows].blank? || params[:efficiencies_windows].nil? 
                    efficiencies[17] = params[:efficiencies_apple] unless params[:efficiencies_apple].blank? || params[:efficiencies_apple].nil? 
                    efficiencies[18] = params[:efficiencies_unix] unless params[:efficiencies_unix].blank? || params[:efficiencies_unix].nil? 
                    efficiencies[19] = params[:efficiencies_database] unless params[:efficiencies_database].blank? || params[:efficiencies_database].nil? 
                    efficiencies[20] = params[:efficiencies_sql] unless params[:efficiencies_sql].blank? || params[:efficiencies_sql].nil? 
                    efficiencies[21] = params[:efficiencies_flash] unless params[:efficiencies_flash].blank? || params[:efficiencies_flash].nil? 
                    efficiencies[22] = params[:efficiencies_multimedia] unless params[:efficiencies_multimedia].blank? || params[:efficiencies_multimedia].nil? 
                    efficiencies[23] = params[:efficiencies_access] unless params[:efficiencies_access].blank? || params[:efficiencies_access].nil?
                    efficiencies[24] = params[:efficiencies_web_design] unless params[:efficiencies_web_design].blank? || params[:efficiencies_web_design].nil?
                    efficiencies[25] = params[:efficiencies_video_audio] unless params[:efficiencies_video_audio].blank? || params[:efficiencies_video_audio].nil?
                    efficiencies[26] = params[:efficiencies_hardware] unless params[:efficiencies_hardware].blank? || params[:efficiencies_hardware].nil?
                    efficiencies[27] = params[:efficiencies_networking] unless params[:efficiencies_networking].blank? || params[:efficiencies_networking].nil?
                # For efficiencies

                # For current_employment
                    current_employment[:position] = params[:position] unless params[:position].blank? || params[:position].nil?
                    current_employment[:department] = params[:department] unless params[:department].blank? || params[:department].nil?
                    current_employment[:company_organization_government] = params[:company_organization_government] unless params[:company_organization_government].blank? || params[:company_organization_government].nil?
                    current_employment[:line_of_business] = params[:line_of_business] unless params[:line_of_business].blank? || params[:line_of_business].nil?
                    current_employment[:number_of_employees] = params[:number_of_employees] unless params[:number_of_employees].blank? || params[:number_of_employees].nil?
                # For current_employment

                # For employment_history
                    if params[:period1].present? && params[:employer1].present? && params[:position1].present?
                        employment_history[:employment1] = { "period" => "#{params[:period1]}", "employer" => "#{params[:employer1]}", "position" => "#{params[:position1]}"}
                    end
                    if params[:period2].present? && params[:employer2].present? && params[:position2].present?
                        employment_history[:employment2] = { "period" => "#{params[:period2]}", "employer" => "#{params[:employer2]}", "position" => "#{params[:position2]}"}
                    end
                    if params[:period3].present? && params[:employer3].present? && params[:position3].present?
                        employment_history[:employment3] = { "period" => "#{params[:period3]}", "employer" => "#{params[:employer3]}", "position" => "#{params[:position3]}"}
                    end
                # For employment_history

                # For areas_of_professional_working_experiences
                    areas_of_professional_working_experiences[0] = params[:working_experience_programming] unless params[:working_experience_programming].blank? || params[:working_experience_programming].nil?
                    areas_of_professional_working_experiences[1] = params[:working_experience_software_engineering] unless params[:working_experience_software_engineering].blank? || params[:working_experience_software_engineering].nil?
                    areas_of_professional_working_experiences[2] = params[:working_experience_analysis_and_design] unless params[:working_experience_analysis_and_design].blank? || params[:working_experience_analysis_and_design].nil?
                    areas_of_professional_working_experiences[3] = params[:working_experience_quality_assurance] unless params[:working_experience_quality_assurance].blank? || params[:working_experience_quality_assurance].nil?
                    areas_of_professional_working_experiences[4] = params[:working_experience_multimedia] unless params[:working_experience_multimedia].blank? || params[:working_experience_multimedia].nil?
                    areas_of_professional_working_experiences[5] = params[:working_experience_web_development] unless params[:working_experience_web_development].blank? || params[:working_experience_web_development].nil?
                    areas_of_professional_working_experiences[6] = params[:working_experience_customer_support] unless params[:working_experience_customer_support].blank? || params[:working_experience_customer_support].nil?
                    areas_of_professional_working_experiences[7] = params[:working_experience_is_it_manager] unless params[:working_experience_is_it_manager].blank? || params[:working_experience_is_it_manager].nil?
                    areas_of_professional_working_experiences[8] = params[:working_experience_orther_management] unless params[:working_experience_orther_management].blank? || params[:working_experience_orther_management].nil?
                    areas_of_professional_working_experiences[9] = params[:working_experience_database_administration] unless params[:working_experience_database_administration].blank? || params[:working_experience_database_administration].nil?
                    areas_of_professional_working_experiences[10] = params[:working_experience_system_maintanance] unless params[:working_experience_system_maintanance].blank? || params[:working_experience_system_maintanance].nil?
                    areas_of_professional_working_experiences[11] = params[:working_experience_systems_administration] unless params[:working_experience_systems_administration].blank? || params[:working_experience_systems_administration].nil?
                    areas_of_professional_working_experiences[12] = params[:working_experience_network_administration] unless params[:working_experience_network_administration].blank? || params[:working_experience_network_administration].nil?
                    areas_of_professional_working_experiences[13] = params[:working_experience_network_engineering] unless params[:working_experience_network_engineering].blank? || params[:working_experience_network_engineering].nil?
                    areas_of_professional_working_experiences[14] = params[:working_experience_research_and_development] unless params[:working_experience_research_and_development].blank? || params[:working_experience_research_and_development].nil?
                # For areas_of_professional_working_experiences

                @membership_application.highest_education = highest_education unless highest_education.nil? || highest_education.blank?
                @membership_application.certificates = certificates unless certificates.nil? || certificates.blank?
                @membership_application.areas_of_training = areas_of_training unless areas_of_training.nil? || areas_of_training.blank?
                @membership_application.efficiencies = efficiencies unless efficiencies.nil? || efficiencies.blank?
                @membership_application.current_employment = current_employment unless current_employment.nil? || current_employment.blank?
                @membership_application.employment_history = employment_history unless employment_history.nil? || employment_history.blank?
                @membership_application.areas_of_professional_working_experiences = areas_of_professional_working_experiences unless areas_of_professional_working_experiences.nil? || areas_of_professional_working_experiences.blank?
                @membership_application.gender = params[:gender] unless params[:gender].blank? || params[:gender].nil?
                @membership_application.member_type = params[:member_type] unless params[:member_type].blank? || params[:member_type].nil?

                # @membership_application.application_date = DateTime.now
                @membership_application.ref_no = SecureRandom.hex(8)
                @membership_application.code_no = SecureRandom.hex(8)
            end

            if @membership_application.save
                format.html { redirect_to "/photos/new?membership_application_ref_no=#{@membership_application.ref_no}", notice: 'Membership application was successfully created.' }
                # format.html { redirect_to "/attachments/new?membership_application_ref_no=#{@membership_application.ref_no}", notice: 'Membership application was successfully created.' }
                format.json { render :show, status: :created, location: @membership_application }
            else
                format.html { render :new }
                format.json { render json: @membership_application.errors, status: :unprocessable_entity }
            end
        end
    end

    # PATCH/PUT /membership_applications/1
    # PATCH/PUT /membership_applications/1.json
    def update
        highest_education = ""
        gender = ""
        certificates = {}
        areas_of_training = []
        efficiencies = []
        current_employment = {}
        employment_history = {}
        areas_of_professional_working_experiences = []

        # For highest_education
        highest_education = params[:highest_education] if params[:highest_education].present?
        # For highest_education

        # For Certificates
        if params[:education1].present? && params[:date1].present? && params[:institution1].present?
            certificates[:certificate1] = { :education => "#{params[:education1]}", :date =>  "#{params[:date1]}", :institution => "#{params[:institution1]}"}

            if params[:education2].present? && params[:date2].present? && params[:institution2].present?
                certificates[:certificate2] = { :education => "#{params[:education2]}", :date =>  "#{params[:date2]}", :institution => "#{params[:institution2]}"}
            end
            if params[:education3].present? && params[:date3].present? && params[:institution3].present?
                certificates[:certificate3] = { :education => "#{params[:education3]}", :date =>  "#{params[:date3]}", :institution => "#{params[:institution3]}"}
            end
            if params[:education4].present? && params[:date4].present? && params[:institution4].present?
                certificates[:certificate4] = { :education => "#{params[:education4]}", :date =>  "#{params[:date4]}", :institution => "#{params[:institution4]}"}
            end
            if params[:education5].present? && params[:date5].present? && params[:institution5].present?
                certificates[:certificate5] = { :education => "#{params[:education5]}", :date =>  "#{params[:date5]}", :institution => "#{params[:institution5]}"}
            end
        end
        # For Certificates

        # For areas_of_training
            areas_of_training[0] = params[:training_programming] unless params[:training_programming].blank? || params[:training_programming].nil?
            areas_of_training[1] = params[:training_software_engineering] unless params[:training_software_engineering].blank? || params[:training_software_engineering].nil?
            areas_of_training[2] = params[:training_database] unless params[:training_database].blank? || params[:training_database].nil?
            areas_of_training[3] = params[:training_networking] unless params[:training_networking].blank? || params[:training_networking].nil?
            areas_of_training[4] = params[:training_web_development] unless params[:training_web_development].blank? || params[:training_web_development].nil?
            areas_of_training[5] = params[:training_operation_system] unless params[:training_operation_system].blank? || params[:training_operation_system].nil?
            areas_of_training[6] = params[:training_graphic_design] unless params[:training_graphic_design].blank? || params[:training_graphic_design].nil?
            areas_of_training[7] = params[:training_multimedia] unless params[:training_multimedia].blank? || params[:training_multimedia].nil?
            areas_of_training[8] = params[:training_hardware] unless params[:training_hardware].blank? || params[:training_hardware].nil?
            areas_of_training[9] = params[:training_mis] unless params[:training_mis].blank? || params[:training_mis].nil?
        # For areas_of_training

        # For efficiencies
            efficiencies[0] = params[:efficiencies_pascal] unless params[:efficiencies_pascal].blank? || params[:efficiencies_pascal].nil? 
            efficiencies[1] = params[:efficiencies_cobol] unless params[:efficiencies_cobol].blank? || params[:efficiencies_cobol].nil? 
            efficiencies[2] = params[:efficiencies_assembly] unless params[:efficiencies_assembly].blank? || params[:efficiencies_assembly].nil? 
            efficiencies[3] = params[:efficiencies_c_c_plus_plus] unless params[:efficiencies_c_c_plus_plus].blank? || params[:efficiencies_c_c_plus_plus].nil? 
            efficiencies[4] = params[:efficiencies_basic] unless params[:efficiencies_basic].blank? || params[:efficiencies_basic].nil? 
            efficiencies[5] = params[:efficiencies_visual_basic] unless params[:efficiencies_visual_basic].blank? || params[:efficiencies_visual_basic].nil? 
            efficiencies[6] = params[:efficiencies_visual_c] unless params[:efficiencies_visual_c].blank? || params[:efficiencies_visual_c].nil? 
            efficiencies[7] = params[:efficiencies_java] unless params[:efficiencies_java].blank? || params[:efficiencies_java].nil? 
            efficiencies[8] = params[:efficiencies_net] unless params[:efficiencies_net].blank? || params[:efficiencies_net].nil? 
            efficiencies[9] = params[:efficiencies_asp] unless params[:efficiencies_asp].blank? || params[:efficiencies_asp].nil? 
            efficiencies[10] = params[:efficiencies_perl] unless params[:efficiencies_perl].blank? || params[:efficiencies_perl].nil? 
            efficiencies[11] = params[:efficiencies_php] unless params[:efficiencies_php].blank? || params[:efficiencies_php].nil? 
            efficiencies[12] = params[:efficiencies_jsp] unless params[:efficiencies_jsp].blank? || params[:efficiencies_jsp].nil? 
            efficiencies[13] = params[:efficiencies_cold_fusion] unless params[:efficiencies_cold_fusion].blank? || params[:efficiencies_cold_fusion].nil? 
            efficiencies[14] = params[:efficiencies_html] unless params[:efficiencies_html].blank? || params[:efficiencies_html].nil? 
            efficiencies[15] = params[:efficiencies_linux] unless params[:efficiencies_linux].blank? || params[:efficiencies_linux].nil? 
            efficiencies[16] = params[:efficiencies_windows] unless params[:efficiencies_windows].blank? || params[:efficiencies_windows].nil? 
            efficiencies[17] = params[:efficiencies_apple] unless params[:efficiencies_apple].blank? || params[:efficiencies_apple].nil? 
            efficiencies[18] = params[:efficiencies_unix] unless params[:efficiencies_unix].blank? || params[:efficiencies_unix].nil? 
            efficiencies[19] = params[:efficiencies_database] unless params[:efficiencies_database].blank? || params[:efficiencies_database].nil? 
            efficiencies[20] = params[:efficiencies_sql] unless params[:efficiencies_sql].blank? || params[:efficiencies_sql].nil? 
            efficiencies[21] = params[:efficiencies_flash] unless params[:efficiencies_flash].blank? || params[:efficiencies_flash].nil? 
            efficiencies[22] = params[:efficiencies_multimedia] unless params[:efficiencies_multimedia].blank? || params[:efficiencies_multimedia].nil? 
            efficiencies[23] = params[:efficiencies_access] unless params[:efficiencies_access].blank? || params[:efficiencies_access].nil?
            efficiencies[24] = params[:efficiencies_web_design] unless params[:efficiencies_web_design].blank? || params[:efficiencies_web_design].nil?
            efficiencies[25] = params[:efficiencies_video_audio] unless params[:efficiencies_video_audio].blank? || params[:efficiencies_video_audio].nil?
            efficiencies[26] = params[:efficiencies_hardware] unless params[:efficiencies_hardware].blank? || params[:efficiencies_hardware].nil?
            efficiencies[27] = params[:efficiencies_networking] unless params[:efficiencies_networking].blank? || params[:efficiencies_networking].nil?
        # For efficiencies

        # For current_employment
        current_employment[:position] = params[:position] unless params[:position].blank? || params[:position].nil?
        current_employment[:department] = params[:department] unless params[:department].blank? || params[:department].nil?
        current_employment[:company_organization_government] = params[:company_organization_government] unless params[:company_organization_government].blank? || params[:company_organization_government].nil?
        current_employment[:line_of_business] = params[:line_of_business] unless params[:line_of_business].blank? || params[:line_of_business].nil?
        current_employment[:number_of_employees] = params[:number_of_employees] unless params[:number_of_employees].blank? || params[:number_of_employees].nil?
        # For current_employment

        # For employment_history
            if params[:period1].present? && params[:employer1].present? && params[:position1].present?
                employment_history[:employment1] = { "period" => "#{params[:period1]}", "employer" => "#{params[:employer1]}", "position" => "#{params[:position1]}"}
            end
            if params[:period2].present? && params[:employer2].present? && params[:position2].present?
                employment_history[:employment2] = { "period" => "#{params[:period2]}", "employer" => "#{params[:employer2]}", "position" => "#{params[:position2]}"}
            end
            if params[:period3].present? && params[:employer3].present? && params[:position3].present?
                employment_history[:employment3] = { "period" => "#{params[:period3]}", "employer" => "#{params[:employer3]}", "position" => "#{params[:position3]}"}
            end
        # For employment_history

        # For areas_of_professional_working_experiences
            areas_of_professional_working_experiences[0] = params[:working_experience_programming] unless params[:working_experience_programming].blank? || params[:working_experience_programming].nil?
            areas_of_professional_working_experiences[1] = params[:working_experience_software_engineering] unless params[:working_experience_software_engineering].blank? || params[:working_experience_software_engineering].nil?
            areas_of_professional_working_experiences[2] = params[:working_experience_analysis_and_design] unless params[:working_experience_analysis_and_design].blank? || params[:working_experience_analysis_and_design].nil?
            areas_of_professional_working_experiences[3] = params[:working_experience_quality_assurance] unless params[:working_experience_quality_assurance].blank? || params[:working_experience_quality_assurance].nil?
            areas_of_professional_working_experiences[4] = params[:working_experience_multimedia] unless params[:working_experience_multimedia].blank? || params[:working_experience_multimedia].nil?
            areas_of_professional_working_experiences[5] = params[:working_experience_web_development] unless params[:working_experience_web_development].blank? || params[:working_experience_web_development].nil?
            areas_of_professional_working_experiences[6] = params[:working_experience_customer_support] unless params[:working_experience_customer_support].blank? || params[:working_experience_customer_support].nil?
            areas_of_professional_working_experiences[6] = params[:working_experience_graphic_design] unless params[:working_experience_graphic_design].blank? || params[:working_experience_graphic_design].nil?
            areas_of_professional_working_experiences[7] = params[:working_experience_is_it_manager] unless params[:working_experience_is_it_manager].blank? || params[:working_experience_is_it_manager].nil?
            areas_of_professional_working_experiences[8] = params[:working_experience_orther_management] unless params[:working_experience_orther_management].blank? || params[:working_experience_orther_management].nil?
            areas_of_professional_working_experiences[8] = params[:working_experience_insstructor] unless params[:working_experience_insstructor].blank? || params[:working_experience_insstructor].nil?
            areas_of_professional_working_experiences[9] = params[:working_experience_database_administration] unless params[:working_experience_database_administration].blank? || params[:working_experience_database_administration].nil?
            areas_of_professional_working_experiences[10] = params[:working_experience_system_maintanance] unless params[:working_experience_system_maintanance].blank? || params[:working_experience_system_maintanance].nil?
            areas_of_professional_working_experiences[11] = params[:working_experience_systems_administration] unless params[:working_experience_systems_administration].blank? || params[:working_experience_systems_administration].nil?
            areas_of_professional_working_experiences[12] = params[:working_experience_network_administration] unless params[:working_experience_network_administration].blank? || params[:working_experience_network_administration].nil?
            areas_of_professional_working_experiences[13] = params[:working_experience_network_engineering] unless params[:working_experience_network_engineering].blank? || params[:working_experience_network_engineering].nil?
            areas_of_professional_working_experiences[14] = params[:working_experience_research_and_development] unless params[:working_experience_research_and_development].blank? || params[:working_experience_research_and_development].nil?
        # For areas_of_professional_working_experiences

        @membership_application.highest_education = highest_education unless highest_education.nil? || highest_education.blank?
        @membership_application.certificates = certificates unless certificates.nil? || certificates.blank?
        @membership_application.areas_of_training = areas_of_training unless areas_of_training.nil? || areas_of_training.blank?
        @membership_application.efficiencies = efficiencies unless efficiencies.nil? || efficiencies.blank?
        @membership_application.current_employment = current_employment unless current_employment.nil? || current_employment.blank?
        @membership_application.employment_history = employment_history unless employment_history.nil? || employment_history.blank?
        @membership_application.areas_of_professional_working_experiences = areas_of_professional_working_experiences unless areas_of_professional_working_experiences.nil? || areas_of_professional_working_experiences.blank?
        @membership_application.gender = params[:gender] unless params[:gender].blank? || params[:gender].nil?
        @membership_application.member_type = params[:member_type] unless params[:member_type].blank? || params[:member_type].nil?

        @member = Member.find_by_member_no(@membership_application.ref_no)
        @member.name = @membership_application.name
        @member.father_name = @membership_application.father_name
        @member.dob = @membership_application.dob
        @member.nrc = @membership_application.nrc
        @member.address = @membership_application.address
        @member.phone = @membership_application.phone
        @member.email = @membership_application.email
        @member.state = @membership_application.state
        @member.township = @membership_application.township
        @member.highest_education = @membership_application.highest_education
        @member.areas_of_training = @membership_application.areas_of_training
        @member.efficiencies = @membership_application.efficiencies
        @member.areas_of_professional_working_experiences = @membership_application.areas_of_professional_working_experiences
        @member.certificates = @membership_application.certificates
        @member.current_employment = @membership_application.current_employment
        @member.employment_history = @membership_application.employment_history

        respond_to do |format|
            if @membership_application.update(membership_application_params) && @member.save
                format.html { redirect_to @membership_application, notice: 'Membership application was successfully updated.' }
                format.json { render :show, status: :ok, location: @membership_application }
            else
                format.html { render :edit }
                format.json { render json: @membership_application.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /membership_applications/1
    # DELETE /membership_applications/1.json
    def destroy
        @membership_application.destroy
        respond_to do |format|
            format.html { redirect_to membership_applications_url, notice: 'Membership application was successfully destroyed.' }
            format.json { head :no_content }
        end
    end


    def state_search
        @townships = State.find_by_name(params[:state_name]).townships
        respond_to do |f|
            f.js
        end
    end

    def successful
        # @membership_application = MembershipApplication.find(params[:member_no])
        puts params[:membership_application_ref_no]
        puts "---------------------"
        @membership_application = MembershipApplication.find_by_ref_no(params[:membership_application_ref_no])
        puts @membership_application.id
        # render file: "/system_mailer/application_is_ready_for_review.html.erb"
        redirect_to "/done"

        SystemMailer.application_is_ready_for_review(@membership_application).deliver_now
        SystemMailer.new_membership_welcome_mail(@membership_application).deliver_now
    end

    def accept
        @member = Member.new
        unless User.find_by_email(@membership_application.email).present? || Member.find_by_email(@membership_application.email).present?
        # # puts "accept method"
            @member.name = @membership_application.name
            @member.burmese_name = @membership_application.burmese_name
            @member.father_name = @membership_application.father_name
            @member.dob = @membership_application.dob
            @member.nrc = @membership_application.nrc.present? ? @membership_application.nrc : "#{@membership_application.nrc_initial}" + "/" + "#{@membership_application.nrc_tsp}" + "(" + "#{@membership_application.nrc_middle}" + ")" + "#{@membership_application.nrc_no}"
            @member.address = @membership_application.address
            @member.phone = @membership_application.phone
            @member.email = @membership_application.email
            @member.state = @membership_application.state
            @member.township = @membership_application.township
            @member.highest_education = @membership_application.highest_education
            @member.areas_of_training = @membership_application.areas_of_training
            @member.efficiencies = @membership_application.efficiencies
            @member.areas_of_professional_working_experiences = @membership_application.areas_of_professional_working_experiences
            @member.certificates = @membership_application.certificates
            @member.current_employment = @membership_application.current_employment
            @member.employment_history = @membership_application.employment_history
            if State.find_by_name(@membership_application.state).present? && State.find_by_name(@membership_application.state).organization.present?
                # puts @membership_application.state
                # puts "found state"
                if State.find_by_name(@membership_application.state).organization.members.count == 0
                    # puts 'zero member'
                    initial = "1200000" 
                    if @membership_application.state == 'Yangon'
                        initial = "12000000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 12000000, 13000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Mandalay'
                        initial = "9000000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 9000000, 10000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Ayeyarwaddy' || "Ayeyawaddy"
                        initial = "14000000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 14000000, 15000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Sagaing'
                        initial = "5000000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 5000000, 6000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Mon'
                        initial = "10000000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 10000000, 11000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Bago'
                        initial = "700000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 700000, 8000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Magway'
                        initial = "800000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 800000, 9000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Kayin'
                        initial = "300000" 
                        # member_no = Member.where("member_no > ? AND member_no < ?", 300000, 4000000).order(:member_no).last.member_no
                    end
                    @member.member_no = initial.to_i + 1
                else
                    # puts 'member exists'
                    if @membership_application.state == 'Yangon'
                        initial = "12000000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 12000000, 13000000).order(:member_no).last.member_no
                        puts Member.all.inspect
                    elsif @membership_application.state == 'Mandalay'
                        initial = "9000000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 9000000, 10000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Ayeyarwaddy' || "Ayeyawaddy"
                        initial = "14000000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 14000000, 15000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Sagaing'
                        initial = "5000000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 5000000, 6000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Mon'
                        initial = "10000000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 10000000, 11000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Bago'
                        initial = "700000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 700000, 8000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Magway'
                        initial = "800000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 800000, 9000000).order(:member_no).last.member_no
                    elsif @membership_application.state == 'Kayin'
                        initial = "300000" 
                        member_no = Member.where("member_no > ? AND member_no < ?", 300000, 4000000).order(:member_no).last.member_no
                    end
                    @member.member_no = member_no + 1
                end
            else
                if Organization.first.members.count == 0
                    # puts 'zero member'
                    initial = "1200000" 
                    # member_no = Member.where("member_no > ? AND member_no < ?", 12000000, 13000000).order(:member_no).last.member_no
                    @member.member_no = initial.to_i + 1
                else
                    member_no = Member.where("member_no > ? AND member_no < ?", 12000000, 13000000).order(:member_no).last.member_no
                    @member.member_no = Organization.first.members.last.member_no + 1
                end
            end
            @member.code_no = @membership_application.code_no
            @member.membership_application_id = @membership_application.id

            @member.log = @member.log.present? ? @member.log : {}
            @member.log["Created member"] = {"#{DateTime.now}" => " State - #{@member.state}, Grade - #{@membership_application.member_type}, Valid-till -  #{DateTime.now + 1.years}"}
        
        end
        respond_to do |format|
            if @member.save

                @membership = Membership.new
                @membership.organization_id = State.find_by_name(@membership_application.state).organization.present? ? State.find_by_name(@membership_application.state).organization.id : Organization.first.id
                @membership.grade = @membership_application.member_type
                @membership.valid_till = DateTime.now + 1.years
                @membership.member_id = @member.id

                @user = User.new
                @user.email = @member.email
                @password = SecureRandom.hex(4)
                @user.password = @password
                @user.role = "Member"
                @user.member_id = @member.id
                @user.profile = @membership_application.profile
                # puts @password

                if @membership.save && @user.save
                    # puts "email to #{@user.email}"
                    # Need to send email to new applicant
                    SystemMailer.membership_application_accepted(@user, @password).deliver_now
                    format.html { redirect_to @membership_application, notice: 'Membership application was successfully accepted.' }
                    # format.json { render :show, status: :created, location: @membership_application }
                else
                    # puts @membership.errors.full_messages
                    # puts @user.errors.full_messages
                    format.html { redirect_to @membership_application, notice: 'Membership application was successfully accepted, yet Membership was not able to created. Please create manually.' }
                    format.json { render :show, status: :created, location: @membership_application }
                end
            else
                # puts @member.errors.full_messages
                # puts "other errors"
                format.html { redirect_to @membership_application, notice: "Some error encounters. Accepting the membership application process is failed. Email might already accquired." }
                # format.html { render :new }
                # format.json { render json: @membership_application.errors, status: :unprocessable_entity }
            end
        end
    end
    def reject
        redirect_to membership_applications_path
    end

    def done

    end

    protected
    def custom_layout
        if user_signed_in?
            if current_user.role == "Member"
                'member_layout'
            else
                'staff_layout'
            end
        else
            'apply'
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_membership_application
        if user_signed_in? && current_user.role == "Admin"
            @membership_applications = MembershipApplication.all
        elsif current_user.staff.organization.organization_name == "Myanmar Computer Professionals Association"
            @membership_applications = MembershipApplication.all
        else
            @membership_applications = MembershipApplication.where("state LIKE ?", current_user.staff.organization.state.name)
        end
        if @membership_applications.ids.include? (params[:id].to_i)
            @membership_application = MembershipApplication.find(params[:id])
        else
            redirect_to membership_applications_path, notice: "Not Found."
        end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membership_application_params
      params.require(:membership_application).permit(:name, :dob, :nrc, :father_name, :address, :township, :state, :phone, :email, :highest_education, :certificates, :areas_of_training, :efficiencies, :current_employment, :employment_history, :areas_of_professional_working_experiences, :application_status, :process_status, :ref_no, :certificates, :current_employment, :employment_history, :gender, :nrc_initial, :nrc_middle, :nrc_tsp, :nrc_no, :profile, :member_type, :code_no, :burmese_name)

    end

    # Only staff can access this model
    def is_staff?
        unless user_signed_in? && current_user.role != "Member"
            redirect_to root_path, notice: 'Access Denied'
        end
    end

    def is_member_login?
        if user_signed_in? && action_name == 'new'
            if current_user.role == "Member" 
                redirect_to "/d/member_information#member_id=#{current_user.member.member_no}", notice: 'You are already the member of this system. Please log out first.'            
            end
        end
    end
end
