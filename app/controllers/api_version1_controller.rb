class ApiVersion1Controller < ApplicationController
	
  def display
    @state = StateSerializer.new(State.first)
    render json: @state.to_json
  end

  def access_control
  	@access_control = AccessControlSerializer.new(AccessControl.find(params[:id]))
    render json: @access_control.to_json
  end
  def announcement
  	@announcement = AnnouncementSerializer.new(Announcement.find(params[:id]))
    render json: @announcement.to_json
  end
  def attachment
  	@attachment = AttachmentSerializer.new(Attachment.find(params[:id]))
    render json: @attachment.to_json
  end
  def ecmember
  	@ecmember = EcmemberSerializer.new(Ecmember.find(params[:id]))
    render json: @ecmember.to_json
  end
  def event
  	@event = EventSerializer.new(Event.find(params[:id]))
    render json: @event.to_json
  end
  def member_datum
  	@member_datum = MemberDatumSerializer.new(MemberDatum.find(params[:id]))
    render json: @member_datum.to_json
  end
  def member
  	@member = MemberSerializer.new(Member.find(params[:id]))
    render json: @member.to_json
  end
  def membership_application
  	@membership_application = MembershipApplicationSerializer.new(MembershipApplication.find(params[:id]))
    render json: @membership_application.to_json
  end
  def membership
  	@membership = MembershipSerializer.new(Membership.find(params[:id]))
    render json: @membership.to_json
  end
  def organization
  	@organization = OrganizationSerializer.new(Organization.find(params[:id]))
    render json: @organization.to_json
  end
  def permission
  	@permission = PermissionSerializer.new(Permission.find(params[:id]))
    render json: @permission.to_json
  end
  def staff
  	@staff = StaffSerializer.new(Staff.find(params[:id]))
    render json: @staff.to_json
  end
  def township
  	@township = TownshipSerializer.new(Township.find(params[:id]))
    render json: @township.to_json
  end
  def user
  	@user = UserSerializer.new(User.find(params[:id]))
    render json: @user.to_json
  end
  
end
