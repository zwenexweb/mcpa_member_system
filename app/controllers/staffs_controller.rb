class StaffsController < ApplicationController
  before_action :is_login?
  before_action :is_admin_or_head?, only: [:destroy, :edit, :update, :new, :create]
  before_action :set_staff, only: [:show, :edit, :update, :destroy]

  # GET /staffs
  # GET /staffs.json
  def index
    if current_user.role == "Admin"
      @staffs = Staff.all 
    else
      @staffs = current_user.staff.organization.staffs.where('role != ? ', 'Admin')
    end

    @staffs = @staffs.where("lower(email) LIKE ? OR lower(name) LIKE ?", "%#{params[:search_by].downcase}%", "%#{params[:search_by].downcase}%") if params[:search_by].present?
    @staffs = @staffs.where("role = ?", "#{params[:search_role]}") if params[:search_role].present?
    @staffs = @staffs.order(:id).paginate(:page => params[:page], :per_page => 15)
  end

  # GET /staffs/1
  # GET /staffs/1.json
  def show
  end

  # GET /staffs/new
  def new
    @staff = Staff.new
  end

  # GET /staffs/1/edit
  def edit
  end

  # POST /staffs
  # POST /staffs.json
  def create
    @staff = Staff.new(staff_params)
    respond_to do |format|
      if @staff.save && User.create(email: @staff.email, staff_id: @staff.id, role: @staff.role, password: 12345678)
        format.html { redirect_to @staff, notice: 'Staff was successfully created.' }
        format.json { render :show, status: :created, location: @staff }
      else
        format.html { render :new }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /staffs/1
  # PATCH/PUT /staffs/1.json
  def update
    respond_to do |format|
      if @staff.update(staff_params)
        format.html { redirect_to @staff, notice: 'Staff was successfully updated.' }
        format.json { render :show, status: :ok, location: @staff }
      else
        format.html { render :edit }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /staffs/1
  # DELETE /staffs/1.json
  def destroy
    if Staff.where(role: 'Admin').count > 1
      @staff.destroy
      notice = 'Staff was successfully destroyed.'
    else
      notice = "The last admin can't destroyed."
    end
    respond_to do |format|
      format.html { redirect_to staffs_url, notice: notice  }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_staff
      @staff = Staff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def staff_params
      params.require(:staff).permit(:name, :email, :role, :organization_id, :position)
    end
    

    def is_login?
      unless user_signed_in? && current_user.staff.present?
        redirect_to root_path, notice: 'Access denied'
      end
    end

    #only admin or head can has access to this model
    def is_admin_or_head?
      unless current_user.role == "Admin" || current_user.role == 'Head'
        redirect_to staffs_path, notice: "Access denied."
      end
    end

end
