class CommitteesController < ApplicationController
  # before_action :authenticate_user!
  before_action :is_staff?#, except: [:index]
  before_action :is_admin?, only: [:destroy]
  before_action :set_committee, only: [:show, :edit, :update, :destroy]

  # GET /committees
  # GET /committees.json
  def index

    if current_user.role == "Staff" || current_user.role == "Head"
      @committees = current_user.staff.organization.committees
    else
      @committees = Committee.all
    end
      @committees = @committees.where('lower(name) LIKE ?', "%#{params[:search_by].downcase}%") if params[:search_by].present?
      @committees = @committees.where('organization_id = ?', "#{params[:select_organization]}") if params[:select_organization].present?
      @committees = @committees.order(:name).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /committees/1
  # GET /committees/1.json
  def show
  end

  # GET /committees/new
  def new
    @committee = Committee.new
    @organizations = Organization.all if current_user.role == "Admin"
    @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff" || current_user.role == "Head"
  end

  # GET /committees/1/edit
  def edit
    @organizations = Organization.all if current_user.role == "Admin"
    @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff" || current_user.role == "Head"
  end

  # POST /committees
  # POST /committees.json
  def create
    @committee = Committee.new(committee_params)

    respond_to do |format|
      if @committee.save
        format.html { redirect_to @committee, notice: 'committee was successfully created.' }
        format.json { render :show, status: :created, location: @committee }
      else
        @organizations = Organization.all if current_user.role == "Admin"
        @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff"
        format.html { render :new }
        format.json { render json: @committee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /committees/1
  # PATCH/PUT /committees/1.json
  def update
    respond_to do |format|
      if @committee.update(committee_params)
        format.html { redirect_to @committee, notice: 'committee was successfully updated.' }
        format.json { render :show, status: :ok, location: @committee }
      else
        @organizations = Organization.all if current_user.role == "Admin"
        @organizations = Organization.where(id: current_user.staff.organization.id) if current_user.role == "Staff"
        format.html { render :edit }
        format.json { render json: @committee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /committees/1
  # DELETE /committees/1.json
  def destroy
    @committee.destroy
    respond_to do |format|
      format.html { redirect_to committees_url, notice: 'committee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_committee
      ### staff can manage only the committee belongs to its organization while admin can manage all committees
      ### staff can not edit or delete the committees who belongs to other organization
      @committees = current_user.role == 'Staff' ? Committee.where(organization_id: current_user.staff.organization.id) :  Committee.all
      if @committees.ids.include? (params[:id].to_i)
        @committee = @committees.find(params[:id])
      else
        redirect_to committees_path, notice: 'Not Found.'
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def committee_params
      params.require(:committee).permit(:name, :organization_id)
    end
    
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end
