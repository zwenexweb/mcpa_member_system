class ComitteesController < ApplicationController
  before_action :authenticate_user!
  before_action :is_staff?, except: [:index]
  before_action :is_admin?, only: [:destroy]
  before_action :set_comittee, only: [:show, :edit, :update, :destroy]

  # GET /comittees
  # GET /comittees.json
  def index

    if current_user.role == "Member"
      @comittees = current_user.member.organizations.last.comittees.paginate(:page => params[:page], :per_page => 20)
    else
      @comittees = Comittee.all
      @comittees = @comittees.where('lower(name) LIKE ?', "%#{params[:search_by].downcase}%") if params[:search_by].present?
      @comittees = @comittees.where('organization_id = ?', "#{params[:select_organization]}") if params[:select_organization].present?
      @comittees = @comittees.order(:id).paginate(:page => params[:page], :per_page => 20)
    end
        
  end

  # GET /comittees/1
  # GET /comittees/1.json
  def show
  end

  # GET /comittees/new
  def new
    @comittee = Comittee.new
  end

  # GET /comittees/1/edit
  def edit
  end

  # POST /comittees
  # POST /comittees.json
  def create
    @comittee = Comittee.new(comittee_params)

    respond_to do |format|
      if @comittee.save
        format.html { redirect_to @comittee, notice: 'Comittee was successfully created.' }
        format.json { render :show, status: :created, location: @comittee }
      else
        format.html { render :new }
        format.json { render json: @comittee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comittees/1
  # PATCH/PUT /comittees/1.json
  def update
    respond_to do |format|
      if @comittee.update(comittee_params)
        format.html { redirect_to @comittee, notice: 'Comittee was successfully updated.' }
        format.json { render :show, status: :ok, location: @comittee }
      else
        format.html { render :edit }
        format.json { render json: @comittee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comittees/1
  # DELETE /comittees/1.json
  def destroy
    @comittee.destroy
    respond_to do |format|
      format.html { redirect_to comittees_url, notice: 'Comittee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comittee
      ### staff can manage only the committee members belongs to its organization while admin can manage all committee members
      ### staff can not edit or delete the committee members who belongs to other organization
      @committees = current_user.role == 'Staff' ? Committee.where(organization_id: current_user.staff.organization.id) :  Committee.all
      if @committees.ids.include? (params[:id].to_i)
        @committee_member = @committees.find(params[:id])
      else
        redirect_to committees_path, notice: 'Not Found.'
      end
      @comittee = Comittee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comittee_params
      params.require(:comittee).permit(:name, :organization_id)
    end
    def is_staff?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
    def is_admin?
      unless user_signed_in? && current_user.role == "Admin"
        redirect_to root_path, notice: 'Access Denied'
      end
    end
end


