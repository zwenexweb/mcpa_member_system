class UsersController < ApplicationController
  before_action :is_login?
  before_action :is_admin?, only: [:destroy]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :edit_information, :update_information]

  # GET /users
  # GET /users.json
  def index
      members = Member.all
      if current_user.role == "Head"
          if params[:search_role] == "Staff"
            @users = User.joins("INNER JOIN staffs ON staffs.id = users.staff_id AND staffs.organization_id = '#{current_user.staff.organization.id}' AND staffs.role != 'Admin'")
          else
            members = current_user.staff.organization.members
            @users = User.joins("INNER JOIN members ON members.id = users.member_id AND members.state = '#{current_user.staff.organization.state.name}'")
          end
          puts "#{@users.count}"
          @users = @users.where("lower(users.email) LIKE ? ", "%#{params[:search_by].downcase}%") if params[:search_by].present?
      else
          if current_user.role == "Staff"
            members = current_user.staff.organization.members
            @users = User.joins("INNER JOIN members ON members.id = users.member_id AND members.state = '#{current_user.staff.organization.state.name}'")
          else
            @users = User.all
          end
            @users = @users.where("lower(users.email) LIKE ?", "%#{params[:search_by].downcase}%") if params[:search_by].present?
            @users = @users.where(role: [params[:search_role]]) if params[:search_role].present?
      end
      @users = @users.order(:id).paginate(:page => params[:page], :per_page => 20)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if params[:user][:member_id].present? 
        puts "member id present"
        if Member.where(member_no: params[:user][:member_id].to_i).present?
          puts 'member with member no found'
          @user.member_id = Member.where(member_no: params[:user][:member_id].to_i).first.id
          if @user.save
            Member.find(@user.member_id).update(email: @user.email) if @user.member_id.present?
            format.html { redirect_to @user, notice: 'User was successfully created.' }
            format.json { render :show, status: :created, location: @user }
          else
            puts 'member not save'
            format.html { render :new }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
       else
          puts 'no member with member id'
          @user.member_id = nil
          @notice = 'No member with such member ID found.'
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        puts 'staff'
        if @user.save
          format.html { redirect_to @user, notice: 'User account for staff was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def populate_by_staff_id
    @staff = Staff.find(params[:staff_id])
    respond_to do |f|
      f.js
    end
  end
  def populate_by_member_id
    @member = Member.find(params[:member_id])
    respond_to do |f|
      f.js
    end
  end

  def edit_information
    # @user = current_user
  end
  def update_information
    @user = current_user
    respond_to do |format|
      if @user.update_with_password(user_params)
        @member = @user.member
        format.html {redirect_to @member, notice: 'Password was successfully updated.'}
      else
        format.html {render :edit_information, notice: "Enter new password" }
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      # unless current_user.role != 'Member'
      #   @users =current_user.role == "Staff" ? User.joins("INNER JOIN members ON members.id = users.member_id AND members.state = '#{current_user.staff.organization.state.name}'") : User.all
      #   puts @users.count
      #   if @users.ids.include? (params[:id].to_id)
      #   else
      #     redirect_to users_path, notice: 'Not Found.'
      #   end
      # else
      #   @user = current_user
      # end 
          @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:role, :staff_id, :member_id, :email, :password, :password_confirmation, :profile, :current_password)
    end

    def is_login?
      unless user_signed_in? && current_user.role != "Member"
        redirect_to root_path, notice: "Access denied."
      end
    end

    #only admin can has access to this model
    def is_admin?
      unless current_user.role == "Admin"
        redirect_to users_path, notice: "Access denied."
      end
    end
end
