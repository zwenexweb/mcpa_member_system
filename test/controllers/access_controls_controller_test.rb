require 'test_helper'

class AccessControlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @access_control = access_controls(:one)
  end

  test "should get index" do
    get access_controls_url
    assert_response :success
  end

  test "should get new" do
    get new_access_control_url
    assert_response :success
  end

  test "should create access_control" do
    assert_difference('AccessControl.count') do
      post access_controls_url, params: { access_control: { description: @access_control.description, key: @access_control.key, name: @access_control.name } }
    end

    assert_redirected_to access_control_url(AccessControl.last)
  end

  test "should show access_control" do
    get access_control_url(@access_control)
    assert_response :success
  end

  test "should get edit" do
    get edit_access_control_url(@access_control)
    assert_response :success
  end

  test "should update access_control" do
    patch access_control_url(@access_control), params: { access_control: { description: @access_control.description, key: @access_control.key, name: @access_control.name } }
    assert_redirected_to access_control_url(@access_control)
  end

  test "should destroy access_control" do
    assert_difference('AccessControl.count', -1) do
      delete access_control_url(@access_control)
    end

    assert_redirected_to access_controls_url
  end
end
