require 'test_helper'

class ComitteesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comittee = comittees(:one)
  end

  test "should get index" do
    get comittees_url
    assert_response :success
  end

  test "should get new" do
    get new_comittee_url
    assert_response :success
  end

  test "should create comittee" do
    assert_difference('Comittee.count') do
      post comittees_url, params: { comittee: { name: @comittee.name, organization_id: @comittee.organization_id } }
    end

    assert_redirected_to comittee_url(Comittee.last)
  end

  test "should show comittee" do
    get comittee_url(@comittee)
    assert_response :success
  end

  test "should get edit" do
    get edit_comittee_url(@comittee)
    assert_response :success
  end

  test "should update comittee" do
    patch comittee_url(@comittee), params: { comittee: { name: @comittee.name, organization_id: @comittee.organization_id } }
    assert_redirected_to comittee_url(@comittee)
  end

  test "should destroy comittee" do
    assert_difference('Comittee.count', -1) do
      delete comittee_url(@comittee)
    end

    assert_redirected_to comittees_url
  end
end
