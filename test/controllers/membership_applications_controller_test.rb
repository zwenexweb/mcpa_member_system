require 'test_helper'

class MembershipApplicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @membership_application = membership_applications(:one)
  end

  test "should get index" do
    get membership_applications_url
    assert_response :success
  end

  test "should get new" do
    get new_membership_application_url
    assert_response :success
  end

  test "should create membership_application" do
    assert_difference('MembershipApplication.count') do
      post membership_applications_url, params: { membership_application: { address: @membership_application.address, application_status: @membership_application.application_status, areas_of_professional_working_experiences: @membership_application.areas_of_professional_working_experiences, areas_of_training: @membership_application.areas_of_training, certificates: @membership_application.certificates, current_employment: @membership_application.current_employment, dob: @membership_application.dob, efficiencies: @membership_application.efficiencies, email: @membership_application.email, employment_history: @membership_application.employment_history, father_name: @membership_application.father_name, highest_education: @membership_application.highest_education, name: @membership_application.name, nrc: @membership_application.nrc, phone: @membership_application.phone, process_status: @membership_application.process_status, ref_no: @membership_application.ref_no, state: @membership_application.state, township: @membership_application.township } }
    end

    assert_redirected_to membership_application_url(MembershipApplication.last)
  end

  test "should show membership_application" do
    get membership_application_url(@membership_application)
    assert_response :success
  end

  test "should get edit" do
    get edit_membership_application_url(@membership_application)
    assert_response :success
  end

  test "should update membership_application" do
    patch membership_application_url(@membership_application), params: { membership_application: { address: @membership_application.address, application_status: @membership_application.application_status, areas_of_professional_working_experiences: @membership_application.areas_of_professional_working_experiences, areas_of_training: @membership_application.areas_of_training, certificates: @membership_application.certificates, current_employment: @membership_application.current_employment, dob: @membership_application.dob, efficiencies: @membership_application.efficiencies, email: @membership_application.email, employment_history: @membership_application.employment_history, father_name: @membership_application.father_name, highest_education: @membership_application.highest_education, name: @membership_application.name, nrc: @membership_application.nrc, phone: @membership_application.phone, process_status: @membership_application.process_status, ref_no: @membership_application.ref_no, state: @membership_application.state, township: @membership_application.township } }
    assert_redirected_to membership_application_url(@membership_application)
  end

  test "should destroy membership_application" do
    assert_difference('MembershipApplication.count', -1) do
      delete membership_application_url(@membership_application)
    end

    assert_redirected_to membership_applications_url
  end
end
