require 'test_helper'

class EcmembersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ecmember = ecmembers(:one)
  end

  test "should get index" do
    get ecmembers_url
    assert_response :success
  end

  test "should get new" do
    get new_ecmember_url
    assert_response :success
  end

  test "should create ecmember" do
    assert_difference('Ecmember.count') do
      post ecmembers_url, params: { ecmember: { organization_id: @ecmember.organization_id, role: @ecmember.role, year: @ecmember.year } }
    end

    assert_redirected_to ecmember_url(Ecmember.last)
  end

  test "should show ecmember" do
    get ecmember_url(@ecmember)
    assert_response :success
  end

  test "should get edit" do
    get edit_ecmember_url(@ecmember)
    assert_response :success
  end

  test "should update ecmember" do
    patch ecmember_url(@ecmember), params: { ecmember: { organization_id: @ecmember.organization_id, role: @ecmember.role, year: @ecmember.year } }
    assert_redirected_to ecmember_url(@ecmember)
  end

  test "should destroy ecmember" do
    assert_difference('Ecmember.count', -1) do
      delete ecmember_url(@ecmember)
    end

    assert_redirected_to ecmembers_url
  end
end
