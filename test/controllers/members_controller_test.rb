require 'test_helper'

class MembersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @member = members(:one)
  end

  test "should get index" do
    get members_url
    assert_response :success
  end

  test "should get new" do
    get new_member_url
    assert_response :success
  end

  test "should create member" do
    assert_difference('Member.count') do
      post members_url, params: { member: { address: @member.address, areas_of_professional_working_experiences: @member.areas_of_professional_working_experiences, areas_of_tranining: @member.areas_of_tranining, dob: @member.dob, efficiencies: @member.efficiencies, email: @member.email, father_name: @member.father_name, highest_education: @member.highest_education, name: @member.name, nrc: @member.nrc, phone: @member.phone, sate: @member.sate, township: @member.township } }
    end

    assert_redirected_to member_url(Member.last)
  end

  test "should show member" do
    get member_url(@member)
    assert_response :success
  end

  test "should get edit" do
    get edit_member_url(@member)
    assert_response :success
  end

  test "should update member" do
    patch member_url(@member), params: { member: { address: @member.address, areas_of_professional_working_experiences: @member.areas_of_professional_working_experiences, areas_of_tranining: @member.areas_of_tranining, dob: @member.dob, efficiencies: @member.efficiencies, email: @member.email, father_name: @member.father_name, highest_education: @member.highest_education, name: @member.name, nrc: @member.nrc, phone: @member.phone, sate: @member.sate, township: @member.township } }
    assert_redirected_to member_url(@member)
  end

  test "should destroy member" do
    assert_difference('Member.count', -1) do
      delete member_url(@member)
    end

    assert_redirected_to members_url
  end
end
