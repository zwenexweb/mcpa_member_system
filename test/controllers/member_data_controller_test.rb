require 'test_helper'

class MemberDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @member_datum = member_data(:one)
  end

  test "should get index" do
    get member_data_url
    assert_response :success
  end

  test "should get new" do
    get new_member_datum_url
    assert_response :success
  end

  test "should create member_datum" do
    assert_difference('MemberDatum.count') do
      post member_data_url, params: { member_datum: { content_catagory: @member_datum.content_catagory, date: @member_datum.date, description: @member_datum.description, title: @member_datum.title } }
    end

    assert_redirected_to member_datum_url(MemberDatum.last)
  end

  test "should show member_datum" do
    get member_datum_url(@member_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_member_datum_url(@member_datum)
    assert_response :success
  end

  test "should update member_datum" do
    patch member_datum_url(@member_datum), params: { member_datum: { content_catagory: @member_datum.content_catagory, date: @member_datum.date, description: @member_datum.description, title: @member_datum.title } }
    assert_redirected_to member_datum_url(@member_datum)
  end

  test "should destroy member_datum" do
    assert_difference('MemberDatum.count', -1) do
      delete member_datum_url(@member_datum)
    end

    assert_redirected_to member_data_url
  end
end
